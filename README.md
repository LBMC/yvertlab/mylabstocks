# MyLabStocks: A web-application to manage molecular biology materials.

## Table of Contents
1. [What is MyLabStocks?](#About)
2. [Documentation](#Documentation)
	- [Installation](doc/INSTALL.md)
	- [Upgrading](doc/admin/upgrading/README.md)
	- [Administrators manual](doc/admin/README.md)
	- [Guidelines for developers](doc/dev/README.md)
3. [Features](#Features)
4. [Simple, free and open-source](#Licence)

## What is MyLabStocks? <a name="About"></a>

[MyLabStocks](https://gitbio.ens-lyon.fr/LBMC/yvertlab/mylabstocks) is a web application dedicated to managing stocks of molecular biology laboratories. It was developed by members of the [laboratory headed by Gael Yvert](http://www.ens-lyon.fr/LBMC/gisv/index.php/en/) at CNRS and ENS de Lyon, France. The application was first described in the following open-access scientific article: [Chuffart and Yvert, Yeast 2014](https://doi.org/10.1002/yea.3008). Here is a quote of the article's abstract:

>Laboratory stocks are the hardware of research. They must be stored and managed with mimimum loss of material and information. Plasmids, oligonucleotides and strains are regularly exchanged between collaborators within and between laboratories. Managing and sharing information about every item is crucial for retrieval of reagents, for planning experiments and for reproducing past experimental results. We have developed a web‐based application to manage stocks commonly used in a molecular biology laboratory. Its functionalities include user‐defined privileges, visualization of plasmid maps directly from their sequence and the capacity to search items from fields of annotation or directly from a query sequence using BLAST. It is designed to handle records of plasmids, oligonucleotides, yeast strains, antibodies, pipettes and notebooks. Based on PHP/MySQL, it can easily be extended to handle other types of stocks and it can be installed on any server architecture.

It was later (in 2019-2021) completely recoded into the Laravel PHP framework in order to i) meet the evolving requirements of operating systems, ii) provide a complete administrative backend and iii) offer the possibility to simultaneously evolve the code and the database structure (via Laravel migrations classes).

## Documentation <a name="Documentation"></a>

- [Installation](doc/INSTALL.md) : detailed instructions on installation procedure.
- [Upgrading](doc/admin/upgrading/README.md) : detailed instructions on how to upgrade. 
- [Administrators Manual](doc/admin/README.md) : documentation on database structure and administrative tasks such as creating users and accounts, performing backups etc.
- [Guidelines for developers](doc/dev/README.md) : how to contribute to the source code of MyLabStocks.

## Features  <a name="Features"></a>

#### Authentication

![loginpage](/doc/img/screenshot-login-page.png)

#### Administrative backend

MyLabStocks possesses an interface to perform all common administrative tasks: create user accounts, assign them roles and permissions, edit any table, manage media files etc. For a complete description of these functionalities, please see the [documentation for administrators](/doc/admin/README.md).

![manager users](/doc/img/screenshot-users.png) 

#### Dynamic display of plasmid maps

When visiting a plasmid's page, a map is generated dynamically that shows you the DNA features matching your plasmid's sequence. You can build your own library of DNA features that will be displayed on your maps.

![plasmidmap](/doc/img/screenshot-plasmidmap.png) 

#### Search your stocks with BLAST

MyLabStocks provides a [BLAST](https://blast.ncbi.nlm.nih.gov/Blast.cgi) search interface that allows you to retrieve your oligos, plasmids and DNA features using any query DNA or peptidic sequence. The search is performed locally on your server, not remotely on the internet.

![blastform](/doc/img/screenshot-blast-form.png)

## Simple, free and open-source <a name="Licence"></a>

Enjoy MyLabStocks as a free and open-source software. With its licence terms, MyLabStocks will never become dependent on a third party and you are sure to keep your freedom to access and manage your stocks. The application is rather simple because it is based on packages from the well-documented [Laravel framework](https://laravel.com). You can even contribute to MyLabStocks and adapt it to the needs of your lab or institution.

