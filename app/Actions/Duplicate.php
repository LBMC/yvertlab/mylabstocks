<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class Duplicate extends AbstractAction
{
    public function getTitle()
    {
        return 'Duplicate';
    }

    public function getIcon()
    {
        return 'voyager-trees';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.'.$this->dataType->slug.'.duplicate', $this->data->{$this->data->getKeyName()});
    }
}
