<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antibody extends Model
{
     /**
     * Relationship to get the supplier that provided the antibody.
     */
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    /**
     * Relationship to get the host that produced the antibody.
     */
    public function host()
    {
        return $this->belongsTo('App\Host');
    }
     
     /**
     * Relationship to select the availability of the antibody.
     */
    public function instock()
    {
        return $this->belongsTo('App\Availability');
    }
     /**
     * Relationship to select the lab member who ordered the antibody.
     */
    public function orderedby()
    {
        return $this->belongsTo('App\User');
    }

}
