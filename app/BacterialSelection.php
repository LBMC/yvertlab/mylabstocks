<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BacterialSelection extends Model
{
    /**
    * Relationship to get the plasmids having this selection.
    */
    public function plasmids()
    {
        return $this->hasMany('App\Plasmid');
    }
   
}
