<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellLine extends Model
{
     /**
     * Relationship to get the cell type.
     */
    public function celltype()
    {
        return $this->belongsTo('App\CellType');
    }
   
    /**
     * Relationship to get the passages of this cell line.
     */
    public function passage()
    {
        return $this->hasMany('App\Passage');
    }
     /**
     * Relationship to select who obtained the cell line.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

 
}
