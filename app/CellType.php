<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellType extends Model
{
    /**
    * Relationship to get the cell lines of this cell type.
    */
    public function celllines()
    {
        return $this->hasMany('App\CellLines');
    }
}
