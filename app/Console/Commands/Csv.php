<?php

namespace App\Console\Commands;

use App\Plasmid;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class Csv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate CSV files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (Storage::disk('csv')->exists('plasmids.csv')) {
            Storage::disk('csv')->delete('plasmids.csv');
        }

        $plasmids = Plasmid::factory()->csv()->count(50)->make();

        //we create the CSV into memory
        //$csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv = Writer::createFromPath(Storage::disk('csv')->path('plasmids.csv'), 'w+');
        $csv->setDelimiter(',');
        $csv->setEnclosure('"');
        $csv->setEscape('\\');

        // Generate CSV Headers
        $headers = collect($plasmids->first()->toArray())->keys()->toArray();

        //we insert the CSV header
        $csv->insertOne($headers);

        foreach($plasmids as $plasmid) {
            $array = $plasmid->toArray();
            isset($array['created_at']) ? $array['created_at'] = now()->toDateTimeString() : null;
            isset($array['updated_at']) ? $array['updated_at'] = now()->toDateTimeString() : null;
            $csv->insertOne($array);
        }

        return 0;
    }
}
