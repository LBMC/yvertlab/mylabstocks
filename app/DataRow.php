<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataRow extends Model
{
    protected $casts = [
        'details' => 'array'
    ];

    public function DataType()
    {
        return $this->belongsTo(DataType::class);
    }
}
