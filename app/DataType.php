<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataType extends Model
{
    public function dataRows()
    {
        return $this->hasMany(DataRow::class)->orderBy('order', 'asc');
    }
}
