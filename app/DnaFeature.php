<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DnaFeature extends Model
{
     /**
     * Relationship to select the author of the feature.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }
    
}
