<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     /**
     * Relationship to get the datatype of this event.
     */
    public function datatype()
    {
        return $this->belongsTo('App\DataType');
    }
     /**
     * Relationship to get the type of this event.
     */
    public function eventtype()
    {
        return $this->belongsTo('App\EventType');
    }
    
}
