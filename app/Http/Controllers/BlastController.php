<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oligo;
use App\Plasmid;
use App\DnaFeature;

require_once("lib/Seq.php");

class BlastController extends Voyager\VoyagerBaseController
{
        /**
         * Build updated Blast databases
         * And return to query form
         */
      public function welcome()
      {
          $storagedir = 'storage/blast/';
                   
          /*
          ** Create/update BLAST Databases
          */
          update_blastdb_plasmids($storagedir);
          update_blastdb_oligos($storagedir);
          update_blastdb_dnafeatures($storagedir);

          /*
          ** Return the query form
          */
          return view('blast.welcome');
      }

      public function update_dna_features(Request $request)
      {
          $storagedir = 'storage/blast/';
          /* run the update */
          update_blastdb_dnafeatures($storagedir);
          /* redirect back to browse page of dna_features */
         $msg="Plasmid maps features were updated";
         return redirect()->back()->with(['message' => $msg, 'alert-type' => 'success']);
      }

      public function search(Request $request)
      {
          // Where inputs/outputs are written
          $storagedir = 'storage/blast/';

          // read the request
          $qryseq = $request->queryseq;
          $blastdb     = $request->blastdb;
          $prog   = $request->blastprog;
          $task   = $request->blasttask;

          // create a FASTA file containing the query sequence
          $qryfasta = $storagedir . "query.fasta";
          $FastaFile = fopen($qryfasta, 'w');
          fwrite($FastaFile, ">your query\n");
          fwrite($FastaFile, "$qryseq\n");
          fclose($FastaFile);

          // build command line
          $outfile = $storagedir . "results.html";
          $db      = $storagedir . $blastdb;
          $cmd_blast = $prog . " -task ". $task . " -query ". $qryfasta . " -db ". $db . " -html -out ". $outfile;
          system($cmd_blast, $retval1);
          if ($retval1 != 0){
               return "Error when running " . $cmd_blast;
               exit;
          }

          return view('blast.results')->with([
                 'prog' => $prog,
                 'task' => $task,
                 'blastdb' => $blastdb,
                 'outfile' => $outfile
          ]);

      }

     
}
