<?php

namespace App\Http\Controllers\Csv;

use App\DataType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use League\Csv\Writer;
use SplFileObject;
use TCG\Voyager\Facades\Voyager;

class ExportController extends Controller
{
    public function download(Request $request, DataType $dataType)
    {
        $model = new $dataType->model_name;
        $this->authorize('read', $model);

        if (!in_array($dataType->slug, ['antibodies', 'oligos', 'strains', 'plasmids'])) {
            abort(503);
        }

        // Redirect if no data
        if ($model->count() == 0) {
            return redirect()->back();
        }

        // Lazy collection
        $items = $model->query()->cursor();

        // Set filename
        $filename = sprintf('%s.csv', $dataType->name);

        return response()->streamDownload(function() use($items) {
            $writer = Writer::createFromFileObject(
                new SplFileObject('php://output', 'w+')
            );
            // Column headers
            $columnHeaders = array_keys($items->first()->toArray());
            $writer->insertOne($columnHeaders);
            // Data chunks
            $chunks = $items->chunk(1000);
            foreach($chunks as $chunk) {
                $writer->insertAll($chunk->toArray());
            }
        }, $filename);

    }
}
