<?php

namespace App\Http\Controllers\Csv;

use App\DataType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use League\Csv\Statement;

class MapController extends Controller
{
    public function create(Request $request, DataType $dataType)
    {
        $model = app($dataType->model_name);
        $this->authorize('add', $model);

        $filename = request()->filename;

        // Check if filename is provided
        if (!$filename) {
            return redirect()->route('csv.upload.create', ['dataType' => $dataType]);
        }

        // Check if file exists
        $csvHeaders = [];
        if (Storage::disk('csv')->exists(sprintf('upload/%s', $filename))) {
            $csvHeaders = $this->getCsvHeader(
                Storage::disk('csv')->path(sprintf('upload/%s', $filename))
            );
        } else {
            abort(404);
        }

        $dataType->load(['dataRows' => function ($query) {
            $query->where('type', '!=', 'relationship');
        }]);

        return view('csv.map.create', compact(['filename', 'csvHeaders', 'model', 'dataType']));
    }

    public function store(Request $request, DataType $dataType)
    {
        // Validation rules ?
        //return $request->all();

        $filename = request()->filename;

        // Check if file exists
        if (Storage::disk('csv')->exists(sprintf('upload/%s', $filename))) {
            $csv = Reader::createFromPath(Storage::disk('csv')->path(sprintf('upload/%s', $filename)), 'r');
            $csv->setHeaderOffset(0); //set the CSV header offset

            //get all records starting skipping the first row
            $stmt = Statement::create();

            $records = $stmt->process($csv);

            DB::beginTransaction();
            try {
                foreach ($records as $record) {
                    DB::table($dataType->name)->insert($record);
                }

                DB::commit();

                // Delete source file
                Storage::disk('csv')->delete(sprintf('upload/%s', $filename));

                return redirect()
                    ->route(sprintf('voyager.%s.index', $dataType->slug))
                    ->with('message', sprintf('%d rows imported successfully', count($records)));

            } catch (\Exception $e) {
                // echo $e->getMessage();
                $error = $e->getMessage();

                DB::rollBack();
                return redirect()
                    ->back()
                    ->with('message', sprintf('Error when importing'))
                    ->with('error', $error);
            }

        } else {
            abort(404, sprintf('Source file %s not found', $filename));
        }
    }

    private function getCsvHeader($filepath): array
    {
        //load the CSV document from a file path
        $csv = Reader::createFromPath($filepath, 'r');
        $csv->setHeaderOffset(0);

        return $csv->getHeader(); //returns the CSV header record
    }
}
