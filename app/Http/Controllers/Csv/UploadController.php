<?php

namespace App\Http\Controllers\Csv;

use App\DataType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    public $uploadMaxFilesize;

    public function __construct()
    {
        $this->uploadMaxFilesize = ini_get('upload_max_filesize');
    }

    public function create(DataType $dataType)
    {
        $model = app($dataType->model_name);
        $this->authorize('add', $model);

        return view('csv.upload.create', [
            'dataType' => $dataType,
            'uploadMaxFilesize' => $this->uploadMaxFilesize]);
    }

    public function store(Request $request, DataType $dataType)
    {
        $model = app($dataType->model_name);
        $this->authorize('add', $model);

        $request->validate([
            'source' => [
                'required',
                'mimes:csv,txt',
                'max:'. $this->toBytes($this->uploadMaxFilesize)
            ],
        ]);

        $filename = sprintf('%s_%s', time(), $request->file('source')->getClientOriginalName());
        $request->file('source')->storeAs('upload', $filename, 'csv');
        return redirect()
            ->route('csv.map.create', ['filename' => $filename, 'dataType' => $dataType])
            ->with('success', 'File has uploaded successfully.');
    }

    private function toBytes($val)
    {
        $val  = trim($val);

        if (is_numeric($val))
            return $val;

        $last = strtolower($val[strlen($val)-1]);
        $val  = substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

        switch($last) {
            // The 'G' modifier is available since PHP 5.1.0
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }
}
