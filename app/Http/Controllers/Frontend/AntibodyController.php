<?php

namespace App\Http\Controllers\Frontend;

use App\Antibody;
use App\DataType;
use App\Http\Controllers\Controller;
use App\Traits\Frontend\SearchBuilder;
use Carbon\Exceptions\ParseErrorException;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class AntibodyController extends Controller
{
    use SearchBuilder;

    public function index(Builder $builder)
    {
        $this->authorize('browse_antibodies');

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                $query->where('browse', '=', true)
                    ->where('type', '!=', 'relationship');
            }])
            ->where('model_name', '=', 'App\Antibody')
            ->firstOrFail();

        if (request()->ajax()) {
            $query = Antibody::query()->when(request()->searchBuilder, function ($query, $searchBuilderAjaxRequest) {
                $criteria = $searchBuilderAjaxRequest['criteria'];
                $logic = $searchBuilderAjaxRequest['logic'];
                $query = $this->buildRecursiveQuery($query, $criteria, $logic);
            });

            // Init datatables
            $dataTables = DataTables::of($query)
                ->addColumn('created_at', function (Antibody $antibody) {
                    if ($antibody->created_at) {
                        try {
                            $createdAt = Carbon::parse($antibody->created_at);
                        } catch (ParseErrorException $e) {
                            return '';
                        }
                        return sprintf('<time datetime="%s">%s</time>', $createdAt->toDateString(), $createdAt->isoFormat('ll'));
                    }
                })
                ->addColumn('obtention_date', function (Antibody $antibody) {
                    if ($antibody->obtention_date) {
                        try {
                            $obtentionDate = Carbon::parse($antibody->obtention_date);
                        } catch (ParseErrorException $e) {
                            return '';
                        }
                        return sprintf('<time datetime="%s">%s</time>', $obtentionDate->toDateString(), $obtentionDate->isoFormat('ll'));
                    }
                })
                ->addColumn('actions', function (Antibody $antibody) {
                    $actions = [];
                    if (auth()->user()->can('read', $antibody)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">View</a>', route('voyager.antibodies.show', ['id' => $antibody]));
                    }
                    if (auth()->user()->can('edit', $antibody)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Edit</a>', route('voyager.antibodies.edit', ['id' => $antibody]));
                    }
                    if (auth()->user()->can('add', $antibody)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Duplicate</a>', route('voyager.antibodies.duplicate', ['id' => $antibody]));
                    }
                    return implode(' ', $actions);
                })
                ->rawColumns(['actions', 'created_at', 'obtention_date']);

            // Enable column search
            foreach ($dataType->dataRows as $dataRow) {
                $dataTables->filterColumn($dataRow->field, function ($query, $keyword) use ($dataRow) {
                    $query->where($dataRow->field, 'like', '%' . $keyword . '%');
                });
            }

            // Execute
            return $dataTables->toJson();
        }

        // HTML Builder parameters
        // Action column is placed first
        $builder->addColumn([
            'data' => 'actions',
            'name' => 'actions',
            'title' => 'Actions',
            'sortable' => false,
            'searchable' => false
        ]);

        // Remaining columns
        foreach($dataType->dataRows as $dataRow) {
            $builder->addColumn([
                'data' => $dataRow->field,
                'name' => $dataRow->field,
                'title' => $dataRow->display_name
            ]);
        }

        // Remove "actions" from DataTable SearchBuilder column dropdown
        $searchBuilderColumns = $builder->getColumns()->reject(function($item, $key) {
            return $item['name'] == 'actions';
        })->keys()->toArray();

        // Datatables JS parameters
        $builder->parameters([
            'paging' => true,
            'searching' => true,
            'info' => false,
            'searchDelay' => 350,
            'scrollY' => '428px',
            'scrollX' => true,
            'scrollCollapse' => true,
            'initComplete' => "function () {
                i = 0;
                this.api().columns().every(function () {
                    var column = this;
                    if (i > 0) {
                        var input = document.createElement(\"input\");
                        $(input)
                            .appendTo($(column.footer()).empty())
                            .addClass('form-control')
                            .on('keyup', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    } else {
                        var label = document.createElement(\"label\");
                        $(label)
                            .appendTo($(column.footer()).empty())
                            .text('Search');
                    }
                    i++;
                });
            }",
            'dom' => 'Qlfrtip',
            'searchBuilder' => [
                'depthLimit' => false,
                'columns' => $searchBuilderColumns,
            ],
            'deferRender' => true,
        ]);

        return view('frontend.antibodies.index', compact('builder', 'dataType'));
    }
}
