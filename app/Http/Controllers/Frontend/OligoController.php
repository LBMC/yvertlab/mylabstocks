<?php

namespace App\Http\Controllers\Frontend;

use App\DataType;
use App\Http\Controllers\Controller;
use App\Traits\Frontend\SearchBuilder;
use App\Traits\Frontend\VoyagerRelationships;
use App\Oligo;
use Carbon\Exceptions\ParseErrorException;
use Illuminate\Support\Carbon;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class OligoController extends Controller
{
    use VoyagerRelationships;
    use SearchBuilder;

    public function index(Builder $builder)
    {
        $this->authorize('browse_oligos');

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                $query->where('browse', '=', true)
                    ->where('type', '!=', 'relationship');
            }])
            ->where('model_name', '=', 'App\Oligo')
            ->firstOrFail();

        if (request()->ajax()) {
            $query = Oligo::query()->when(request()->searchBuilder, function ($query, $searchBuilderAjaxRequest) {
                $criteria = $searchBuilderAjaxRequest['criteria'];
                $logic = $searchBuilderAjaxRequest['logic'];
                $query = $this->buildRecursiveQuery($query, $criteria, $logic);
            });

            // Init datatables
            $dataTables = DataTables::of($query)
                ->addColumn('created_at', function (Oligo $oligo) {
                    if ($oligo->created_at) {
                        try {
                            $createdAt = Carbon::parse($oligo->created_at);
                        } catch (ParseErrorException $e) {
                            return '';
                        }
                        return sprintf('<time datetime="%s">%s</time>', $createdAt->toDateString(), $createdAt->isoFormat('ll'));
                    }
                })
                ->addColumn('obtention_date', function (Oligo $oligo) {
                    if ($oligo->obtention_date) {
                        try {
                            $obtentionDate = Carbon::parse($oligo->obtention_date);
                        } catch (ParseErrorException $e) {
                            return '';
                        }
                        return sprintf('<time datetime="%s">%s</time>', $obtentionDate->toDateString(), $obtentionDate->isoFormat('ll'));
                    }
                })
                ->addColumn('actions', function (Oligo $oligo) {
                    $actions = [];
                    if (auth()->user()->can('read', $oligo)) {
                        $actions[] = sprintf('<input id="oligo-%d" name="oligo-%d" value="%s" type="text" class="w-25" readonly/>', $oligo->id, $oligo->id, $oligo->sequence);
                        $actions[] = sprintf('<a href="#" onclick="document.getElementById(\'oligo-%d\').select(); document.execCommand(\'copy\'); toastr.info(\'Sequence copied to clipboard\')">CopySeq</a>', $oligo->id);
                        $actions[] = sprintf('<a href="%s" target="_self">View</a>', route('frontend.oligos.show', ['oligo' => $oligo]));
                    }
                    if (auth()->user()->can('edit', $oligo)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Edit</a>', route('voyager.oligos.edit', ['id' => $oligo]));
                    }
                    if (auth()->user()->can('add', $oligo)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Duplicate</a>', route('voyager.oligos.duplicate', ['id' => $oligo]));
                    }
                    return implode(' ', $actions);
                })
                ->rawColumns(['actions', 'created_at', 'obtention_date', 'description']);

            // Enable column search
            foreach ($dataType->dataRows as $dataRow) {
                $dataTables->filterColumn($dataRow->field, function ($query, $keyword) use ($dataRow) {
                    $query->where($dataRow->field, 'like', '%' . $keyword . '%');
                });
            }

            // Execute
            return $dataTables->toJson();
        }

        // HTML Builder parameters
        // Action column is placed first
        $builder->addColumn([
            'data' => 'actions',
            'name' => 'actions',
            'title' => 'Actions',
            'sortable' => false,
            'searchable' => false
        ]);

        // Remaining columns
        foreach($dataType->dataRows as $dataRow) {
            $builder->addColumn([
                'data' => $dataRow->field,
                'name' => $dataRow->field,
                'title' => $dataRow->display_name
            ]);
        }

        // Remove "actions" from DataTable SearchBuilder column dropdown
        $searchBuilderColumns = $builder->getColumns()->reject(function($item, $key) {
            return $item['name'] == 'actions';
        })->keys()->toArray();

        // Datatables JS parameters
        $builder->parameters([
            'paging' => true,
            'searching' => true,
            'info' => false,
            'searchDelay' => 350,
            'scrollY' => '428px',
            'scrollX' => true,
            'scrollCollapse' => true,
            'initComplete' => "function () {
                i = 0;
                this.api().columns().every(function () {
                    var column = this;
                    if (i > 0) {
                        var input = document.createElement(\"input\");
                        $(input)
                            .appendTo($(column.footer()).empty())
                            .addClass('form-control')
                            .on('keyup', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    } else {
                        var label = document.createElement(\"label\");
                        $(label)
                            .appendTo($(column.footer()).empty())
                            .text('Search');
                    }
                    i++;
                });
            }",
            'dom' => 'Qlfrtip',
            'searchBuilder' => [
                'depthLimit' => false,
                'columns' => $searchBuilderColumns,
            ],
            'deferRender' => true,
        ]);

        return view('frontend.oligos.index', compact('builder', 'dataType'));
    }

    public function show(Oligo $oligo)
    {
        $this->authorize('read', $oligo);

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                $query->where('read', '=', true);
            }])
            ->where('model_name', '=', 'App\Oligo')
            ->firstOrFail();

        $this->resolveRelations($dataType);

        return view('frontend.oligos.show', [
            'dataType' => $dataType,
            'item' => $oligo
        ]);
    }
}
