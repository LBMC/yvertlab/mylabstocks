<?php

namespace App\Http\Controllers\Frontend;

use App\DataType;
use App\Http\Controllers\Controller;
use App\Traits\Frontend\SearchBuilder;
use App\Traits\Frontend\VoyagerRelationships;
use App\Plasmid;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class PlasmidController extends Controller
{
    use VoyagerRelationships;
    use SearchBuilder;

    public function index(Builder $builder)
    {
        $this->authorize('browse_plasmids');

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                $query->where('browse', '=', true)
                    ->where('type', '!=', 'relationship');
            }])
            ->where('model_name', '=', 'App\Plasmid')
            ->firstOrFail();

        if (request()->ajax()) {
            $query = Plasmid::query()->when(request()->searchBuilder, function ($query, $searchBuilderAjaxRequest) {
                    $criteria = $searchBuilderAjaxRequest['criteria'];
                    \Log::debug($criteria);
                    $logic = $searchBuilderAjaxRequest['logic'];
                    \Log::debug($logic);
                    $query = $this->buildRecursiveQuery($query, $criteria, $logic);
                });

            // Init datatables
            $dataTables = DataTables::of($query)
                ->addColumn('created_at', function (Plasmid $plasmid) {
                    return $plasmid->created_at->isoFormat('lll');
                })
                ->addColumn('description', function (Plasmid $plasmid) {
                    return sprintf('<div class="overflow-auto" style="max-width: 250px; max-height: 150px;">%s</div>', $plasmid->description);
                })
                ->addColumn('seqfile', function (Plasmid $plasmid) {
                    $seqfile = view('components.frontend.data.file', ['files' => (array) json_decode($plasmid->seqfile, true)]);
                    return sprintf('<div class="overflow-auto" style="max-width: 250px; max-height: 150px;">%s</div>', $seqfile->toHtml());
                })
                ->addColumn('actions', function (Plasmid $plasmid) {
                    $actions = [];
                    if (auth()->user()->can('read', $plasmid)) {
                        $actions[] = sprintf('<a href="%s" target="_self">View</a>', route('frontend.plasmids.show', ['plasmid' => $plasmid]));
                    }
                    if (auth()->user()->can('edit', $plasmid)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Edit</a>', route('voyager.plasmids.edit', ['id' => $plasmid]));
                    }
                    if (auth()->user()->can('add', $plasmid)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Duplicate</a>', route('voyager.plasmids.duplicate', ['id' => $plasmid]));
                    }
                    return implode(' ', $actions);
                })
                ->rawColumns(['description', 'actions', 'seqfile']);

            // Enable column search
            foreach ($dataType->dataRows as $dataRow) {
                $dataTables->filterColumn($dataRow->field, function ($query, $keyword) use ($dataRow) {
                    $query->where($dataRow->field, 'like', '%' . $keyword . '%');
                });
            }

            // Execute
            return $dataTables->toJson();
        }

        // HTML Builder parameters
        // Action column is placed first
        $builder->addColumn([
            'data' => 'actions',
            'name' => 'actions',
            'title' => 'Actions',
            'sortable' => false,
            'searchable' => false
        ]);

        // Remaining columns
        foreach ($dataType->dataRows as $dataRow) {
            $builder->addColumn([
                'data' => $dataRow->field,
                'name' => $dataRow->field,
                'title' => $dataRow->display_name
            ]);
        }

        // Remove "actions" from DataTable SearchBuilder column dropdown
        $searchBuilderColumns = $builder->getColumns()->reject(function($item, $key) {
            return $item['name'] == 'actions';
        })->keys()->toArray();

        // Datatables JS parameters
        $builder->parameters([
            'paging' => true,
            'searching' => true,
            'info' => false,
            'searchDelay' => 350,
            'scrollY' => '428px',
            'scrollX' => true,
            'scrollCollapse' => true,
            'initComplete' => "function () {
                i = 0;
                this.api().columns().every(function () {
                    var column = this;
                    if (i > 0) {
                        var input = document.createElement(\"input\");
                        $(input)
                            .appendTo($(column.footer()).empty())
                            .addClass('form-control')
                            .on('keyup', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    } else {
                        var label = document.createElement(\"label\");
                        $(label)
                            .appendTo($(column.footer()).empty())
                            .text('Search');
                    }
                    i++;
                });
            }",
            'dom' => 'Qlfrtip',
            'searchBuilder' => [
                'depthLimit' => false,
                'columns' => $searchBuilderColumns,
            ],
            'deferRender' => true,
        ]);

        return view('frontend.plasmids.index', compact('builder', 'dataType'));
    }

    public function show(Plasmid $plasmid)
    {
        $this->authorize('read', $plasmid);

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                //->where('type', '!=', 'relationship')
                $query->where('read', '=', true);
            }])
            ->where('model_name', '=', 'App\Plasmid')
            ->firstOrFail();

        $this->resolveRelations($dataType);

        // check that sequence exists (later we must also check that it is in correct format)
        if (!$plasmid->sequence) {
            $map = NULL; //todo: put a default image telling that there is no seq
        } else {
            // draw the map and store its address
            $map = drawplasmidmap($plasmid->sequence, $plasmid->id);
        }

        return view('frontend.plasmids.show', [
            'dataType' => $dataType,
            'item' => $plasmid,
            'map' => $map
        ]);
    }
}
