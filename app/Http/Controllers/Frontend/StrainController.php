<?php

namespace App\Http\Controllers\Frontend;

use App\DataType;
use App\Http\Controllers\Controller;
use App\Traits\Frontend\SearchBuilder;
use App\Traits\Frontend\VoyagerRelationships;
use App\Strain;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class StrainController extends Controller
{
    use VoyagerRelationships;
    use SearchBuilder;

    public function index(Builder $builder)
    {
        $this->authorize('browse_strains');

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                $query->where('browse', '=', true)
                    ->where('type', '!=', 'relationship');
            }])
            ->where('model_name', '=', 'App\Strain')
            ->firstOrFail();

        if (request()->ajax()) {
            $query = Strain::query()->when(request()->searchBuilder, function ($query, $searchBuilderAjaxRequest) {
                $criteria = $searchBuilderAjaxRequest['criteria'];
                $logic = $searchBuilderAjaxRequest['logic'];
                $query = $this->buildRecursiveQuery($query, $criteria, $logic);
            });

            // Init datatables
            $dataTables = DataTables::of($query)
                ->addColumn('created_at', function (Strain $strain) {
                    return $strain->created_at->isoFormat('lll');
                })
                ->addColumn('comments', function (Strain $strain) {
                    return sprintf('<div class="overflow-auto" style="max-height: 150px; max-width: 250px;">%s</div>', $strain->comments);
                })
                ->addColumn('actions', function (Strain $strain) {
                    $actions = [];
                    if (auth()->user()->can('read', $strain)) {
                        $actions[] = sprintf('<a href="%s" target="_self">View</a>', route('frontend.strains.show', ['strain' => $strain]));
                    }
                    if (auth()->user()->can('edit', $strain)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Edit</a>', route('voyager.strains.edit', ['id' => $strain]));
                    }
                    if (auth()->user()->can('add', $strain)) {
                        $actions[] = sprintf('<a href="%s" target="_blank" class="text-secondary">Duplicate</a>', route('voyager.strains.duplicate', ['id' => $strain]));
                    }
                    return implode(' ', $actions);
                })
                ->rawColumns(['comments', 'actions']);

            // Enable column search
            foreach ($dataType->dataRows as $dataRow) {
                $dataTables->filterColumn($dataRow->field, function ($query, $keyword) use ($dataRow) {
                    $query->where($dataRow->field, 'like', '%' . $keyword . '%');
                });
            }

            // Execute
            return $dataTables->toJson();
        }

        // HTML Builder parameters
        // Action column is placed first
        $builder->addColumn([
            'data' => 'actions',
            'name' => 'actions',
            'title' => 'Actions',
            'sortable' => false,
            'searchable' => false
        ]);

        // Remaining columns
        foreach ($dataType->dataRows as $dataRow) {
            $builder->addColumn([
                'data' => $dataRow->field,
                'name' => $dataRow->field,
                'title' => $dataRow->display_name
            ]);
        }

        // Remove "actions" from DataTable SearchBuilder column dropdown
        $searchBuilderColumns = $builder->getColumns()->reject(function($item, $key) {
            return $item['name'] == 'actions';
        })->keys()->toArray();

        // Datatables JS parameters
        $builder->parameters([
            'paging' => true,
            'searching' => true,
            'info' => false,
            'searchDelay' => 350,
            'scrollY' => '428px',
            'scrollX' => true,
            'scrollCollapse' => true,
            'initComplete' => "function () {
                i = 0;
                this.api().columns().every(function () {
                    var column = this;
                    if (i > 0) {
                        var input = document.createElement(\"input\");
                        $(input)
                            .appendTo($(column.footer()).empty())
                            .addClass('form-control')
                            .on('keyup', function () {
                                column.search($(this).val(), false, false, true).draw();
                            });
                    } else {
                        var label = document.createElement(\"label\");
                        $(label)
                            .appendTo($(column.footer()).empty())
                            .text('Search');
                    }
                    i++;
                });
            }",
            'dom' => 'Qlfrtip',
            'searchBuilder' => [
                'depthLimit' => false,
                'columns' => $searchBuilderColumns,
            ],
            'deferRender' => true,
        ]);

        return view('frontend.strains.index', compact('builder', 'dataType'));
    }

    public function show(Strain $strain)
    {
        $this->authorize('read', $strain);

        $dataType = DataType::query()
            ->with(['dataRows' => function ($query) {
                //->where('type', '!=', 'relationship')
                $query->where('read', '=', true);
            }])
            ->where('model_name', '=', 'App\Strain')
            ->firstOrFail();

        $this->resolveRelations($dataType);

        return view('frontend.strains.show', [
            'dataType' => $dataType,
            'item' => $strain
        ]);
    }
}
