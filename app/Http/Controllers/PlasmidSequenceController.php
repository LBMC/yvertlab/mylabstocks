<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

Use App\Plasmid;

require_once("lib/Seq.php");

class PlasmidSequenceController extends Controller
{

    public function update_plasmid_seq_from_file($id, Request $request) {
    
        /**
        * Retrieve seq filename from database
        **/
        $plasmid = Plasmid::find($id);
        $storagedir = 'storage/';

        /* Test if a sequence file is entered for this plasmid record */
        if ($plasmid->seqfile == "[]" || is_null($plasmid->seqfile)) {
              $msg="NOT Updated. This plasmid has no seqfile entry";
              return redirect()->back()->with(['message' => $msg, 'alert-type' => 'warning']);
        }
        /* extract path to seq file */
        $s  = explode(",", $plasmid->seqfile);
        $ss = explode(":", $s[0]);
        $sss = stripslashes(substr($ss[1], 1, -1));
        $file = $storagedir.$sss;
        /* verify that file exists */
        if (!file_exists($file)){
             $msg="FAILURE: File ".$file." was not found. Verify the seqfile entry.";
             return redirect()->back()->with(['message'=> $msg, 'alert-type' => 'error']);
        }
        /**
        * read file and extract the sequence 
        **/
        $section = file_get_contents($file);
        /* extract text after ORIGIN word */
        $pos = strpos($section, 'ORIGIN');
        $seq = substr($section, $pos + 7);
        /* remove all tabs, new lines, double spaces */
        $seq = preg_replace("/[^a-zA-Z]/", "", $seq);
        /* Convert all characters to lowercase*/
        $seq = strtolower($seq);    
        /* verify that it is a DNA sequence */
        $isdna=is_dna($seq);
        if (!$isdna['valid']){
           return("error when verifying DNA sequence of file " . $file . "\n". $isdna['msg']. ". The tested sequence was: \n". $seq);
        }

        /**
        * Update plasmid sequence in database 
        **/
        $plasmid->update(['sequence' => $seq]);

        /* redirect to read page of plasmid */
        return redirect()->back()->with(['message' => "Sequence was successfully updated from .gb file", 'alert-type' => 'success']);
     }

}
