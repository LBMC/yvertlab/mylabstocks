<?php

namespace App\Http\Controllers\Voyager;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\DB;

//use Exception;
//use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Support\Facades\Auth;
//use TCG\Voyager\Database\Schema\SchemaManager;
//use TCG\Voyager\Events\BreadDataAdded;
//use TCG\Voyager\Events\BreadDataDeleted;
//use TCG\Voyager\Events\BreadDataRestored;
//use TCG\Voyager\Events\BreadDataUpdated;
//use TCG\Voyager\Events\BreadImagesDeleted;
//use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;



class VoyagerBaseController extends BaseVoyagerBaseController
{

    //****************************************
    //
    // Add a new item of our Data Type BRE(A)D
    // and offer the possibility to edit it.
    // This provides the possibility 
    // to copy/paste an item instead of 
    // re-entering all entries each time we add
    // a new item.
    //
    //****************************************

    public function duplicate(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = isset($row->details->width) ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check permission
        $this->authorize('add', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // duplicate the item
        $newContent = $dataTypeContent->replicate();

        // Force the key to be NULL (this is how the edit-add view then knows that we are in 'add' mode)
        $keyName = $newContent->getKeyName();
        $newContent->update([$keyName => NULL]);

        // return the edit-add view for the duplicated item
        $dataTypeContent = $newContent;
        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));

    }
  
}
