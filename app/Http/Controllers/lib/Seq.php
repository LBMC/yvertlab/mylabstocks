<?php

Use App\DnaFeature;
Use App\Oligo;
Use App\Plasmid;

// a function that cleans and print one sequence into fasta format
function Convert2Fasta ($seq)
{
  $basesPerLine = 60;
  // remove blanks in beginning or end of seq:
  $out = trim($seq);
  // remove "-" and "_" spacers
  $out = str_replace("-", "",$out);
  $out = str_replace("_", "",$out);  
  // change to upper case
  $out = strtoupper($out);
  // convert to a table and print *** bases per line:str_split()
  $out = str_split($out, $basesPerLine);
  return ($out);
}

// A function that verifies that a variable contains a DNA sequence string
function is_dna($seq){
    $valid=true;
    $msg="";
    if (!isset($seq)){
        $valid=false;
        $msg="sequence is not defined";
    };
        
    $test=preg_split("/[^acgtnACGTN]/", $seq);
    if (sizeof($test) != 1){
         $valid=false;
         $msg="Sequence contains non-DNA letters";
    }    

    return(['valid' => $valid, 'msg' => $msg]);
}

function blast2cirdna($infile, $outfile, $start, $end){
        $newtxt = "Start" . "\t" . $start . "\n" . "End" . "\t" . $end . "\n"."\n";

        if(!$output = fopen($outfile, "w")){
                echo("ERROR in blast2cirdna: Can't open file " . $outfile . " for writing");
                exit;
                }
        fwrite($output, $newtxt);
        $txt_string = file_get_contents($infile);

        if (trim($txt_string == false)){ // case where input file is empty

           $newtxt = "group" . "\n";    fwrite($output, $newtxt);
           $newtxt = "endgroup" . "\n"; fwrite($output, $newtxt);

        } else {

           $txt_string = trim($txt_string, "\n");   //remove whitespace
           $lines = explode("\n", $txt_string);
           $newtxt = "group" . "\n" . "\n"; fwrite($output, $newtxt);
   
           // For all the lines within the TXT
           foreach ($lines as $newline) {

                   $line = explode("\t", $newline);
                   $fullname = $line["1"];
                   $names = explode("_FEATCOLOR_", $fullname);
                   $featname = $names["0"];
                   $featcolor = $names["1"];
                   // set color according to feature category
                   if ($featcolor == "black"){ $color = "0";
                   } elseif ($featcolor == "red") { $color = "1";
                   } elseif ($featcolor == "yellow") { $color = "2";
                   } elseif ($featcolor == "green") { $color = "3";
                   } elseif ($featcolor == "aquamarine") { $color = "4";
                   } elseif ($featcolor == "pink") { $color = "5";
                   } elseif ($featcolor == "wheat") { $color = "6";
                   } elseif ($featcolor == "grey") { $color = "7";
                   } elseif ($featcolor == "brown") { $color = "8";
                   } elseif ($featcolor == "blue") { $color = "9";
                   } elseif ($featcolor == "blueviolet") { $color = "10";
                   } elseif ($featcolor == "cyan") { $color = "11";
                   } elseif ($featcolor == "turquoise") { $color = "12";
                   } elseif ($featcolor == "magenta") { $color = "13";
                   } elseif ($featcolor == "salmon") { $color = "14";
                   } elseif ($featcolor == "white") { $color = "15";
                   } else { $color = "15";
                   }

                   $newtxt =  "label" . "\n". "Block". "\t"; fwrite($output, $newtxt);
                   $newtxt = $line["6"]. "\t" . $line["7"] . "\t" . $color . "\n"; fwrite($output, $newtxt);
                   $newtxt = $featname . "\n"; fwrite($output, $newtxt);
                   $newtxt =  "endlabel" . "\n" ; fwrite($output, $newtxt);
           }
           $newtxt =   "\n". "endgroup"; fwrite($output, $newtxt);
           fclose($output);
        }
}

function update_blastdb_plasmids($storagedir){
          /*
          ** Create/Update the plasmids Database
          */
          $plasmids = Plasmid::all();

          // Create an updated fasta file of all plasmids
          $plasmidfasta = $storagedir . "plasmids.fasta";
          $FastaFile = fopen($plasmidfasta, 'w');
          foreach ($plasmids as $plasmid) {
              fwrite($FastaFile, ">$plasmid->name\n");
              $toprint = Convert2Fasta($plasmid->sequence);
              for ($i=0; $i< count($toprint); $i++) {
                  fwrite($FastaFile, "$toprint[$i]\n");
              }
          }
          fclose($FastaFile);

          // Build the BLAST database from the fasta file
          $plasmiddblogfile = $storagedir . "plasmidsdb.log";
          $cmd_makeblastdb = "makeblastdb -in " .$plasmidfasta . " -input_type 'fasta' -dbtype 'nucl' -title 'plasmidsdb' -out " . $storagedir . "plasmids > $plasmiddblogfile";
          system($cmd_makeblastdb, $retval1);
          if ($retval1 != 0){
               return "Error when running " . $cmd_makeblastdb;
               exit;
          }
}

function update_blastdb_oligos($storagedir){
          /*
          ** Create/Update the oligos BLAST Database
          */
          $oligos = Oligo::all();

          // Create an updated fasta file of all oligos
          $oligofasta = $storagedir . "oligos.fasta";
          $FastaFile = fopen($oligofasta, 'w');
          foreach ($oligos as $oligo) {
              fwrite($FastaFile, ">$oligo->name\n");
              $toprint = Convert2Fasta($oligo->sequence);
              for ($i=0; $i< count($toprint); $i++) {
                  fwrite($FastaFile, "$toprint[$i]\n");
              }
          }
          fclose($FastaFile);

          // Build the BLAST database from the fasta file
          $oligodblogfile = $storagedir . "oligosdb.log";
          $cmd_makeblastdb = "makeblastdb -in " .$oligofasta . " -input_type 'fasta' -dbtype 'nucl' -title 'oligosdb' -out " . $storagedir . "oligos > $oligodblogfile";
          system($cmd_makeblastdb, $retval1);
          if ($retval1 != 0){
               return "Error when running " . $cmd_makeblastdb;
               exit;
          }
}

function update_blastdb_dnafeatures($storagedir){
          /*
          ** Create/Update the dnafeatures BLAST Database
          ** (including the feature's type in its name)
          ** with fasta file in format:
          ** >featurename_FEATCOLOR_color
          */
          $dnafeatures = DnaFeature::all();
          // Create an updated fasta file of all DNA features
          $dnafeaturesfasta = $storagedir . "dnafeatures.fasta";
          $FastaFile = fopen($dnafeaturesfasta, 'w');
          foreach ($dnafeatures as $dnafeature) {
              // remove white spaces from name
              $namenospace =  str_replace(" ", "", $dnafeature->name);
              $extendedname = $namenospace . "_FEATCOLOR_" . $dnafeature->color;
              fwrite($FastaFile, ">$extendedname\n");
              $toprint = Convert2Fasta($dnafeature->sequence);
              for ($i=0; $i< count($toprint); $i++) {
                  fwrite($FastaFile, "$toprint[$i]\n");
              }
          }
          fclose($FastaFile);

          // Build the BLAST database from the fasta file
          $dnafeaturesdblogfile = $storagedir . "dnafeaturesdb.log";
          $cmd_makeblastdb = "makeblastdb -in " .$dnafeaturesfasta . " -input_type 'fasta' -dbtype 'nucl' -title 'dnafeaturesdb' -out " . $storagedir . "dnafeatures > $dnafeaturesdblogfile";
          system($cmd_makeblastdb, $retval1);
          if ($retval1 != 0){
               return "Error when running " . $cmd_makeblastdb;
               exit;
          }
}


function drawplasmidmap($seq, $id){
   // -------------------------
   // blast it on dna-features
   // -------------------------
   
   // create a tmp dir with name the plasmid id
   $tmpdir = 'storage/tmp/plasmidmaps/' . $id . '/';
   $cmd_mktmpdir = 'mkdir -p ' . $tmpdir; 
   system($cmd_mktmpdir, $retval0);
   if ($retval0 != 0){
        return "Error in drawplasmidmap() when running " . $cmd_mktmpdir;
        exit;
   }


   $blastdb = 'storage/blast/dnafeatures';
   $blastoutfile = $tmpdir . 'blastout.tab';
   // create a FASTA file containing the query sequence
   $qryfasta = $tmpdir . 'q.fasta';
   $FastaFile = fopen($qryfasta, 'w');
   fwrite($FastaFile, ">queryseq\n");
   fwrite($FastaFile, "$seq\n");
   fclose($FastaFile);
   // run blast
   $cmd_blast = "blastn -query ". $qryfasta . " -db ". $blastdb . " -out ". $blastoutfile . " -outfmt 6";
   system($cmd_blast, $retval1);
   if ($retval1 != 0){
        return "Error in drawplasmidmap() when running " . $cmd_blast;
        exit;
   }

   // -------------------------
   // convert blast output into
   // cirdna input format
   // -------------------------
   $cirdnainfile= $tmpdir . 'cirdnainfile.txt';
   $start=1;
   $end=strlen($seq);
   blast2cirdna($blastoutfile, $cirdnainfile, $start, $end);

   // -------------------------
   // run cirdna to draw image
   // -------------------------
   $cirdnaoutfile= $tmpdir . 'map';
   $cmd_cirdna = "cirdna -posblocks Out -posticks Out -blocktype Outline -blockheight 2.5 -postext 1 -textheight 1.5 -textlength 2.5 -intercolour 7 -graphout pdf -ruler Y -gtitle ' ' -infile ". $cirdnainfile . " -goutfile " . $cirdnaoutfile . " >/dev/null";
   system($cmd_cirdna, $retval2);
   if ($retval2 != 0){
        return "Error in drawplasmidmap() when running " . $cmd_cirdna;
        exit;
   }
   // convert it to svg format
   $cmd_convert2svg = "pdf2svg " . $cirdnaoutfile . ".pdf " . $cirdnaoutfile . ".svg";
   system($cmd_convert2svg, $retval3);
   if ($retval3 != 0){
        return "Error in drawplasmidmap() when running " . $cmd_convert2svg;
        exit;
   }

   // return img url
   $imgurl = getenv('APP_URL') . "/". $cirdnaoutfile . ".svg";
   return $imgurl;
}

?>
