<?php

namespace App\Listeners;

use App\Events\BoxAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Box;
use App\Boxposition;

class AddBoxpositionsOfNewBox
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Add positions of the added box
     * in the boxpositions table
     *
     * @param  BoxAdded  $event
     * @return void
     */
    public function handle(BoxAdded $event)
    {

	$box = $event->data;

        // If (old) positions already exist for this box, delete them
        // This should never happen 
        // because we have set onDelete(cascade) on the foreign key.
        // But it is safer to also write it here.
	Boxposition::where('box_id', $box->id)->delete();

        // create positions
        for ($i=1; $i <= $box->nrows; $i++)
           for ($j=1; $j <= $box->ncols; $j++){
               $boxposition = Boxposition::create([
                                'box_id' => $box->id,
                                'row' => $i,
                                'column' => $j,
                                'name' => 'box '. $box->id . ' row ' . $i .  ' col ' . $j,
                              ]);
        }
    }
}
