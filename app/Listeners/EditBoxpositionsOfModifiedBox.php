<?php

namespace App\Listeners;

use App\Events\BoxModified;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Box;
use App\Boxposition;

class EditBoxpositionsOfModifiedBox
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Edit boxpositions according to the modified box.
     *
     * @param  BoxModified  $event
     * @return void
     */
    public function handle(BoxModified $event)
    {
       	$box = $event->data;

        // If existing positions have row or column values
        // that are now beyond the dimension of this box, delete them.

	Boxposition::where('box_id', $box->id)->where('row', '>', $box->nrows)->delete();
	Boxposition::where('box_id', $box->id)->where('column', '>', $box->ncols)->delete();

        // Create positions if they do not already exist.
        // (for example if the dimension of the box has increased, or if the id of the box has changed)
        for ($i=1; $i <= $box->nrows; $i++)
           for ($j=1; $j <= $box->ncols; $j++){
               $boxposition = Boxposition::firstOrCreate([
                                'box_id' => $box->id,
                                'row' => $i,
                                'column' => $j,
                                'name' => 'box '. $box->id . ' row ' . $i .  ' col ' . $j,
                              ]);
        }
    }
}
