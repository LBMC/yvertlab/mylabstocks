<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notebook extends Model
{
    /**
     * Relationship to select the author of the notebook.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

}
