<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oligo extends Model
{
    use HasFactory;

    /**
     * Relationship to get the purif of the oligo.
     */
    public function purif()
    {
        return $this->belongsTo('App\Purif');
    }

     /**
     * Relationship to get the supplier of the oligo.
     */
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    /**
     * Relationship to select who ordered the oligo.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }


}
