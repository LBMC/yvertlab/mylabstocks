<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passage extends Model
{
     /**
     * Relationship to get the cellline that was passaged.
     */
    public function cellline()
    {
        return $this->belongsTo('App\CellLine');
    }
    /**
     * Relationship to select who did the passage.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

}
