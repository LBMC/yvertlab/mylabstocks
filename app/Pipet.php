<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pipet extends Model
{

     /**
     * Relationship to get the pipet type.
     */
    public function type()
    {
        return $this->belongsTo('App\PipetType');
    }
     /**
     * Relationship to get the brand (ie supplier).
     */
    public function brand()
    {
        return $this->belongsTo('App\Supplier');
    }

}
