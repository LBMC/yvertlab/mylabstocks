<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PipetType extends Model
{
    /**
    * Relationship to get the pipets of this type.
    */
    public function pipets()
    {
        return $this->hasMany('App\Pipet');
    }
}
