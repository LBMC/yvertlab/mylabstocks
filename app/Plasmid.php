<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plasmid extends Model
{
    use HasFactory;

    /**
    *  Allow massive assignment on sequence field
    **/
    protected $fillable = ['sequence']; 
    /**
     * Relationship to get the type of this plasmid.
     */
    public function type()
    {
        return $this->belongsTo('App\PlasmidType');
    }
     /**
     * Relationship to get the bacterial selection of this plasmid.
     */
    public function bacterialSelection()
    {
        return $this->belongsTo('App\BacterialSelection');
    }     
     /**
     * Relationship to get the species targeted by this plasmid.
     */
    public function species()
    {
        return $this->belongsTo('App\Specie');
    }

    /**
     * Relationship to select the author of the plasmid.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

    public function getMarkersAttribute()
    {
        $collection = collect();
        if ($this->marker1) {
            $collection->push($this->marker1);
        }
        if ($this->marker2) {
            $collection->push($this->marker2);
        }
        return $collection;
    }

}
