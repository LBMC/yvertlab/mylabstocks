<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlasmidType extends Model
{
    /**
    * Relationship to get the plasmids of this type.
    */
    public function plasmids()
    {
        return $this->hasMany('App\Plasmid');
    }
    
}
