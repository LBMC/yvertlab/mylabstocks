<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     /**
     * Relationship to get the supplier that provided the product.
     */
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

     /**
     * Relationship to select the lab member who ordered the product.
     */
    public function orderedby()
    {
        return $this->belongsTo('App\User');
    }


   
}
