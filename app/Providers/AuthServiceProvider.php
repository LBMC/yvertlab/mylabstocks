<?php

namespace App\Providers;

use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('browse_antibodies', function (User $user) {
            return $user->hasPermission('browse_antibodies');
        });

        Gate::define('browse_oligos', function (User $user) {
            return $user->hasPermission('browse_oligos');
        });

        Gate::define('browse_plasmids', function (User $user) {
            return $user->hasPermission('browse_plasmids');
        });

        Gate::define('browse_strains', function (User $user) {
            return $user->hasPermission('browse_strains');
        });
    }
}
