<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

/**
Event and Listeners to add/modify boxpositions when a box is added or modified.
*/
use App\Events\BoxAdded;
use App\Events\BoxModified;
use App\Listeners\AddBoxpositionsOfNewBox;
use App\Listeners\EditBoxpositionsOfModifiedBox;

class EventServiceProvider extends ServiceProvider
{
     /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        BoxAdded::class => [
            AddBoxpositionsOfNewBox::class,
        ],
        BoxModified::class => [
            EditBoxpositionsOfModifiedBox::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
