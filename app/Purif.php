<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purif extends Model
{
    /**
    * Relationship to get the oligos associated with the purif.
    */
    public function oligos()
    {
        return $this->hasMany('App\Oligo');
    }
    
}
