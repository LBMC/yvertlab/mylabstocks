<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specie extends Model
{
     /**
     * Relationship to get the plasmids targeting this spcecies.
     */
    public function plasmids()
    {
        return $this->hasMany('App\Plasmid');
    }
    
}
