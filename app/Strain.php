<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Strain extends Model
{
    use HasFactory;

    /**
     * Relationship to select the author of this strain.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relationship to get the species of this strain.
     */
    public function species()
    {
        return $this->belongsTo('App\Specie');
    }

    /**
     * Relationship to get the general background of this strain.
     */
    public function generalBackground()
    {
        return $this->belongsTo('App\Background');
    }

    /**
    * Relationship to get the mating type of this strain.
    */
    public function matingType()
    {
        return $this->belongsTo('App\Yeast\StMat');
    }

    /**
    * Relationships to get the auxotrophies of this strain.
    */
    public function ade2(){return $this->belongsTo('App\Yeast\StAde2');}
    public function his3(){return $this->belongsTo('App\Yeast\StHis3');}
    public function leu2(){return $this->belongsTo('App\Yeast\StLeu2');}
    public function lys2(){return $this->belongsTo('App\Yeast\StLys2');}
    public function met15(){return $this->belongsTo('App\Yeast\StMet15');}
    public function trp1(){return $this->belongsTo('App\Yeast\StTrp1');}
    public function ura3(){return $this->belongsTo('App\Yeast\StUra3');}

    public function getGenotypeAttribute()
    {
        $gen = collect([
            $this->mating_type,
            $this->ade2,
            $this->his3,
            $this->leu2,
            $this->lys2,
            $this->met15,
            $this->trp1,
            $this->ura3,
            $this->ho,
            $this->locus1,
            $this->locus2,
            $this->locus3,
            $this->locus4,
            $this->locus5
        ]);

        return sprintf('%s [ %s ] ( %s )', $gen->implode(' '), $this->cytoplasmic_character, $this->extrachromosomal_plasmid);
    }

}
