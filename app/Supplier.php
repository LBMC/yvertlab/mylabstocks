<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /**
    * Relationship to get the antibodies bought from the supplier.
    */
    public function antibodies()
    {
        return $this->hasMany('App\Antibody');
    }
    /**
    * Relationship to get the oligos bought from the supplier.
    */
    public function oligos()
    {
        return $this->hasMany('App\Oligo');
    }

}
