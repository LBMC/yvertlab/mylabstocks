<?php

namespace App\Traits\Frontend;

use Illuminate\Database\Eloquent\Builder;

trait SearchBuilder
{
    private function buildRecursiveQuery(Builder $query, array $criteria, string $logic): Builder
    {
        foreach ($criteria as $item) {
            if (isset($item['criteria']) && isset($item['logic'])) {
                // Recursive
                $subCriteria = $item['criteria'];
                $subLogic = $item['logic'];
                if ($logic == 'OR') {
                    $query->orWhere(function ($query) use ($subCriteria, $subLogic) {
                        $this->buildRecursiveQuery($query, $subCriteria,  $subLogic);
                    });
                } else {
                    $query->where(function ($query) use ($subCriteria, $subLogic) {
                        $this->buildRecursiveQuery($query, $subCriteria,  $subLogic);
                    });
                }
            } else {
                // Apply criteria

                // Contains
                if (isset($item['condition']) && $item['condition'] == 'contains' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], 'like', sprintf('%%%s%%', $item['value1']));
                    } else {
                        $query->where($item['origData'], 'like', sprintf('%%%s%%', $item['value1']));
                    }
                }

                // Is Equal
                if (isset($item['condition']) && $item['condition'] == '=' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '=', $item['value1']);
                    } else {
                        $query->where($item['origData'], '=', $item['value1']);
                    }
                }

                // Is Not Equal
                if (isset($item['condition']) && $item['condition'] == '!=' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '!=', $item['value1']);
                    } else {
                        $query->where($item['origData'], '!=', $item['value1']);
                    }
                }

                // Starts with
                if (isset($item['condition']) && $item['condition'] == 'starts' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], 'like', sprintf('%s%%', $item['value1']));
                    } else {
                        $query->where($item['origData'], 'like', sprintf('%s%%', $item['value1']));
                    }
                }

                // Ends with
                if (isset($item['condition']) && $item['condition'] == 'ends' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], 'like', sprintf('%%%s', $item['value1']));
                    } else {
                        $query->where($item['origData'], 'like', sprintf('%%%s', $item['value1']));
                    }
                }

                // Is Null
                if (isset($item['condition']) && $item['condition'] == 'null' && isset($item['origData'])) {
                    if ($logic == 'OR') {
                        $query->orWhereNull($item['origData']);
                    } else {
                        $query->whereNull($item['origData']);
                    }
                }

                // Is Not Null
                if (isset($item['condition']) && $item['condition'] == '!null' && isset($item['origData'])) {
                    if ($logic == 'OR') {
                        $query->orWhereNotNull($item['origData']);
                    } else {
                        $query->whereNotNull($item['origData']);
                    }
                }

                // Is Greater Than
                if (isset($item['condition']) && $item['condition'] == '>' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '>', $item['value1']);
                    } else {
                        $query->where($item['origData'], '>', $item['value1']);
                    }
                }

                // Is Greater Than Or Equal
                if (isset($item['condition']) && $item['condition'] == '>=' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '>=', $item['value1']);
                    } else {
                        $query->where($item['origData'], '>=', $item['value1']);
                    }
                }

                // Is Lower Than
                if (isset($item['condition']) && $item['condition'] == '<' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '<', $item['value1']);
                    } else {
                        $query->where($item['origData'], '<', $item['value1']);
                    }
                }

                // Is Lower Than Or Equal
                if (isset($item['condition']) && $item['condition'] == '<=' && isset($item['origData']) && isset($item['value1'])) {
                    if ($logic == 'OR') {
                        $query->orWhere($item['origData'], '<=', $item['value1']);
                    } else {
                        $query->where($item['origData'], '<=', $item['value1']);
                    }
                }

                // Is Between
                if (isset($item['condition']) && $item['condition'] == 'between' && isset($item['origData']) && isset($item['value1']) && isset($item['value2'])) {
                    if ($logic == 'OR') {
                        $query->orWhereBetween($item['origData'], [$item['value1'], $item['value2']]);
                    } else {
                        $query->whereBetween($item['origData'], [$item['value1'], $item['value2']]);
                    }
                }

                // Is Not Between
                if (isset($item['condition']) && $item['condition'] == '!between' && isset($item['origData']) && isset($item['value1']) && isset($item['value2'])) {
                    if ($logic == 'OR') {
                        $query->orWhereNotBetween($item['origData'], [$item['value1'], $item['value2']]);
                    } else {
                        $query->whereNotBetween($item['origData'], [$item['value1'], $item['value2']]);
                    }
                }

            }
        }
        return $query;
    }

}