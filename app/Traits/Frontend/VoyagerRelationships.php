<?php

namespace App\Traits\Frontend;

use App\DataType;
use Illuminate\Support\Str;

trait VoyagerRelationships
{
    public function resolveRelations(DataType $dataType)
    {
        $relationships = $dataType->dataRows->where('type', '=', 'relationship');

        $model = app($dataType->model_name);

        // Recreate model relationships on the fly based on Voyager
        foreach ($relationships as $relationship) {
            $details = $relationship->details;

            if ($details['type'] == 'belongsToMany') {
                $model::resolveRelationUsing($details['table'], function ($model) use ($details) {
                    return $model->belongsToMany($details['model'], $details['pivot_table']);
                });
            } else if ($details['type'] == 'belongsTo') {
                $model::resolveRelationUsing(Str::singular($details['table']), function ($model) use ($details) {
                    return $model->belongsTo($details['model'], $details['column']);
                });
            } else if ($details['type'] == 'hasMany') {
                $model::resolveRelationUsing($details['table'], function ($model) use ($details) {
                    return $model->hasMany($details['model']);
                });
            } else if ($details['type'] == 'hasOne') {
                $model::resolveRelationUsing(Str::singular($details['table']), function ($model) use ($details) {
                    return $model->hasOne($details['model']);
                });
            }
        }
    }
}