<?php

namespace App\View\Components\Frontend;

use Illuminate\View\Component;

class CardWithImage extends Component
{
    public $image;
    public $title;
    public $link;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($image = false, $title = false, $link = false)
    {
        $this->image = $image;
        $this->title = $title;
        $this->link = $link;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.frontend.card-with-image');
    }
}
