<?php

namespace App\View\Components\Frontend;

use Illuminate\View\Component;

class Data extends Component
{
    public $type;
    public $value;
    public $files;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($value, $type = 'default')
    {
        $this->value = $value;
        $this->type = $type;

        if (in_array($this->type, ['file', 'media_picker']) && $this->value) {
            $this->files = json_decode($this->value, true);
        } else {
            $this->files = [];
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        if ($this->type == 'file') {
            return view('components.frontend.data.file');
        } else if ($this->type == 'media_picker') {
            return view('components.frontend.data.media_picker');
        } else if ($this->type == 'rich_text_box') {
            return view('components.frontend.data.rich_text_box');
        } else {
            return view('components.frontend.data.default');
        }
    }
}
