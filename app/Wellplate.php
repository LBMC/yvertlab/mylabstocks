<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wellplate extends Model
{
    use HasFactory;

    /**
     * Relationship to select the user who stored the plate.
     */
    public function author()
    {
        return $this->belongsTo('App\User');
    }

}
