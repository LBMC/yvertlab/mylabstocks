<?php

namespace Database\Factories;

use App\Oligo;
use Illuminate\Database\Eloquent\Factories\Factory;

class OligoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Oligo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => strtoupper($this->faker->bothify('#?##')),
            'sequence' => $this->faker->randomElement([
                'GATCCCCGGGAATTGCCATGCTGCTCCTTCTTGCATCTTC',
                'GATCCCCGGGAATTGCCATGCTCCCCTCGACGTATCTTC',
                'GATCGCTGACCTGCATACCATGGCCCTCGACGTATCTTC',
            ]),
            'description' => $this->faker->paragraph,
            'author' => $this->faker->name(),
            'supplier' => $this->faker->company,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
