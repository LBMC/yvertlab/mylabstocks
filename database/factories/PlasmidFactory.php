<?php

namespace Database\Factories;

use App\Plasmid;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class PlasmidFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Plasmid::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => Str::random(6),
            'sequence' => $this->faker->randomElement([
                'atgaccatgattacgccactagtccgaggcctcgagatccgatatcgccgtggcggccgccagctgaagcttaattatcctgggcacgagtgaaacaaagctaaaacctttatttagcatggccattgaatgtaacaattatatatatcgcaagcacaaaaaatcaaggagagagaactaccactttgttcatgtgtacaatgttcattatctccataagcaaaaaaaaaaaatagaaaacatatgctataaggttgatattctcacgagtaagcggcacttgctacttattgacattgcagatttttggctacagaaatagtatattagagattataattgctaatcaaatcaaaatataaaattagtaaaccaaaccatttatacccttccttagtagttatggattgttttttaatgatatttctgcaaaccaaagaaagattgttatccagatagaatttagttttgatattcatttttttgttgaagattgaacgccatatctgggcctcataattcaaaagacggtgccattatcggtagcgtttcgcattgtactggatttcagaaatttcacagttgatgaatcgaaaagaatggtctcattgcaacacgtaaggttaagatgtccctttttaccattataggcaataaatgaatcataaaacgaccgtatactggtgaaatagtagggagaacgagtacctgtagtaaaaagtataaatcatagttaatcgggcaatgtccctcgatcaaggagtattgtgtcatgttcgagacaaacgccaacatttttgtttcttttggacaaatgttgtttgcatttatgatccgttatattttgatctaatgtagagttgcacgtagttcttactggcaaagaaatcgatgcataccaaaaaagaataaaggtgatatttgatctttaccgtttagttccaacgtaaaattgtgcctttggacttaaaatggcgtcgtacgctgcaggtcgacGGATCCGACATGGAGGCCCAGAATACCCTCCTTGACAGTCTTGACGTGCGCAGCTCAGGGGCATGATGTGACTGTCGCCCGTACATTTAGCCCATACATCCCCATGTATAATCATTTGCATCCATACATTTTGATGGCCGCACGGCGCGAAGCAAAAATTACGGCTCCTCGCTGCAGACCTGCGAGCAGGGAAACGCTCCCCTCACAGACGCGTTGAATTGTCCCCACGCCGCGCCCCTGTAGAGAAATATAAAAGGTTAGGATTTGCCACTGAGGTTCTTCTTTCATATACTTCCTTTTAAAATCTTGCTAGGATACAGTTCTCACATCACATCCGAACATAAACAACCGTTAACATAACTTCGTATAGCATACATTATACGAAGTTATCCATGTCTAAGAATATCGTTGTCCTACCGGGTGATCACGTCGGTAAAGAAGTTACTGACGAAGCTATTAAGGTCTTGAATGCCATTGCTGAAGTCCGTCCAGAAATTAAGTTCAATTTCCAACATCACTTGATCGGGGGTGCTGCCATCGATGCCACTGGCACTCCTTTACCAGATGAAGCTCTAGAAGCCTCTAAGAAAGCCGATGCTGTCTTACTAGGTGCTGTTGGTGGTCCAAAATGGGGTACGGGCGCAGTTAGACCAGAACAAGGTCTATTGAAGATCAGAAAGGAATTGGGTCTATACGCCAACTTGAGACCATGTAACTTTGCTTCTGATTCTTTACTAGATCTTTCTCCTTTGAAGCCTGAATATGCAAAGGGTACCGATTTCGTCGTCGTTAGAGAATTGGTTGGTGGTATCTACTTTGGTGAAAGAAAAGAAGATGAAGGTGACGGAGTTGCTTGGGACTCTGAGAAATACAGTGTTCCTGAAGTTCAAAGAATTACAAGAATGGCTGCTTTCTTGGCATTGCAACAAAACCCACCATTACCAATCTGGTCTCTTGACAAGGCTAACGTGCTTGCCTCTTCCAGATTGTGGAGAAAGACTGTTGAAGAAACCATCAAGACTGAGTTCCCACAATTAACTGTTCAGCACCAATTGATCGACTCTGCTGCTATGATTTTGGTTAAATCACCAACTAAGCTAAACGGTGTTGTTATTACCAACAACATGTTTGGTGATATTATCTCCGATGAAGCCTCTGTTATTCCAGGTTCTTTGGGTTTATTACCTTCTGCATCTCTAGCTTCCCTACCTGACACTAACAAGGCATTCGGTTTGTACGAACCATGTCATGGTTCTGCCCCAGATTTACCAGCAAACAAGGTTAATCCAATTGCTACCATCTTATCTGCAGCTATGATGTTGAAGTTATCCTTGGATTTGGTTGAAGAAGGTAGGGCTCTTGAAGAAGCTGTTAGAAATGTCTTGGATGCAGGTGTCAGAACCGGTGACCTTGGTGGTTCTAACTCTACCACTGAGGTTGGCGATGCTATCGCCAAGGCTGTCAAGGAAATCTTGGCTTAACGCGCCACTTCTAAATAAGCGAATTTCTTATGATTTATGATTTTTATTATTAAATAAGTTATAAAAAAAATAAGTGTATACAAATTTTAAAGTGACTCTTAGGTTTTAAAACGAAAATTCTTGTTCTTGAGTAACTCTTTCCTGTAGGTCAGGTTGCTTTCTCAGGTATAGCATGAGGTCGCTCTTATTGACCACACCTCTACCGGCAGATCGCTAGCATAACTTCGTATAGCATACATTATACGAAGTTATCCATGGTGTCAAAGGGGGAGGAAGATAATATGGCGATAATTAAAGAGTTCATGAGGTTTAAAGTCCACATGGAGGGTTCAGTCAACGGTCATGAGTTCGAGATCGAAGGTGAGGGTGAAGGCAGACCGTATGAAGGTACTCAAACTGCCAAATTGAAGGTGACCAAAGGCGGCCCACTTCCGTTTGCGTGGGACATTCTTTCACCTCAATTCATGTACGGTTCGAAAGCTTATGTTAAACATCCAGCAGATATTCCCGATTATCTAAAGTTGTCTTTCCCTGAAGGTTTTAAATGGGAAAGGGTCATGAATTTTGAAGACGGTGGGGTTGTAACGGTAACACAGGATTCCTCATTACAAGATGGCGAATTTATCTATAAAGTCAAGTTGCGTGGCACTAATTTTCCTTCTGATGGTCCTGTCATGCAGAAGAAAACAATGGGCTGGGAAGCTTCAAGCGAAAGGATGTACCCAGAAGATGGTGCTTTAAAGGGTGAGATCAAACAAAGATTAAAGTTAAAGGACGGCGGGCATTACGATGCTGAAGTTAAAACGACTTATAAAGCTAAGAAACCTGTTCAGCTGCCAGGTGCATACAACGTGAATATAAAGCTTGACATAACATCACATAACGAGGACTATACAATTGTTGAACAGTATGAAAGAGCCGAAGGTCGTCACAGTACTGGAGGGATGGATGAACTATACAAATAACCTAGGCTGGTCGAGTCATGTAATTAGTTATGTCACGCTTACATTCACGCCCTCCCCCCACATCCGCTCTAACCGAAAAGGAAGGAGTTAGACAACCTGAAGTCTAGGTCCCTATTTATTTTTTTATAGTTATGTTAGTATTAAGAACGTTATTTATATTTCAAATTTTTCTTTTTTTTCTGTACAGACGCGTGTACGCATGTAACATTATACTGAAAACCTTGCTTGAGAAGGTTTTGGGACGCTCGAAGGCTTTAATTTGCGGCCGGTACgagctcgaattcctgggggaacaacttcacagaatgttttgtcatattgtcgaagtggtcacaaaacaagagaagttccgccaattataaaaagggaacccgtatatttcagcttcacggatgatttccagggtgagagtactgtatatgggcttacgatagaaggccataaaaatttcttgcttggcaacaaaatagaagtgaaatcatgtcgaggctgctgtgtgggagaacagcttaaaatatcacaaaaaaagaatctaaaacactgtgttgcttgtcccagaaagggaatcaagtatttttataaagattggagtggtaaaaatcgagtatgtgctagatgctatggaagatacaaattcagcggtcatcactgtataaattgcaagtatgtaccagaagcacgtgaagtgaaaaaggcaaaagacaaaggcgaaaaattgggcattacgcccgaaggtttgccagttaaaggaccagagtgtataaaatgtggcggaatcttacagtggcctatgcggccgctctagaactagtggatcgatccccaattcgccctatagtgagtcgtattacaattcactggccgtcgttttacaacgtcgtgactgggaaaaccctggcgttacccaacttaatcgccttgcagcacatccccctttcgccagctggcgtaatagcgaagaggcccgcaccgatcgcccttcccaacagttgcgcagcctgaatggcgaatggcgcctgatgcggtattttctccttacgcatctgtgcggtatttcacaccgcatacgtcaaagcaaccatagtacgcgccctgtagcggcgcattaagcgcggcgggtgtggtggttacgcgcagcgtgaccgctacacttgccagcgccctagcgcccgctcctttcgctttcttcccttcctttctcgccacgttcgccggctttccccgtcaagctctaaatcggggtgggccatcgccctgatagacggtttttcgccctttgacgttggagtccacgttctttaatagtggactcttgttccaaactggaacaacactcaaccctatctcgggctattcttttgatttataagggattttgccgatttcggcctattggttaaaaaatgagctgatttaacaaaaatttaacgcgaattttaacaaaatattaacgtttacaattttatggtgcactctcagtacaatctgctctgatgccgcatagttaagccagccccgacacccgccaacacccgctgacgcgccctgacgggcttgtctgctcccggcatccgcttacagacaagctgtgaccgtctccgggagctgcatgtgtcagaggttttcaccgtcatcaccgaaacgcgcgagacgaaagggcctcgtgatacgcctatttttataggttaatgtcatgataataatggtttcttagacgtcaggtggcacttttcggggaaatgtgcgcggaacccctatttgtttatttttctaaatacattcaaatatgtatccgctcatgagacaataaccctgataaatgcttcaataatattgaaaaaggaagagtatgagtattcaacatttccgtgtcgcccttattcccttttttgcggcattttgccttcctgtttttgctcacccagaaacgctggtgaaagtaaaagatgctgaagatcagttgggtgcacgagtgggttacatcgaactggatctcaacagcggtaagatccttgagagttttcgccccgaagaacgttttccaatgatgagcacttttaaagttctgctatgtggcgcggtattatcccgtattgacgccgggcaagagcaactcggtcgccgcatacactattctcagaatgacttggttgagtactcaccagtcacagaaaagcatcttacggatggcatgacagtaagagaattatgcagtgctgccataaccatgagtgataacactgcggccaacttacttctgacaacgatcggaggaccgaaggagctaaccgcttttttgcacaacatgggggatcatgtaactcgccttgatcgttgggaaccggagctgaatgaagccataccaaacgacgagcgtgacaccacgatgcctgtagcaatggcaacaacgttgcgcaaactattaactggcgaactacttactctagcttcccggcaacaattaatagactggatggaggcggataaagttgcaggaccacttctgcgctcggcccttccggctggctggtttattgctgataaatctggagccggtgagcgtgggtctcgcggtatcattgcagcactggggccagatggtaagccctcccgtatcgtagttatctacacgacggggagtcaggcaactatggatgaacgaaatagacagatcgctgagataggtgcctcactgattaagcattggtaactgtcagaccaagtttactcatatatactttagattgatttaaaacttcatttttaatttaaaaggatctaggtgaagatcctttttgataatctcatgaccaaaatcccttaacgtgagttttcgttccactgagcgtcagaccccgtagaaaagatcaaaggatcttcttgagatcctttttttctgcgcgtaatctgctgcttgcaaacaaaaaaaccaccgctaccagcggtggtttgtttgccggatcaagagctaccaactctttttccgaaggtaactggcttcagcagagcgcagataccaaatactgtccttctagtgtagccgtagttaggccaccacttcaagaactctgtagcaccgcctacatacctcgctctgctaatcctgttaccagtggctgctgccagtggcgataagtcgtgtcttaccgggttggactcaagacgatagttaccggataaggcgcagcggtcgggctgaacggggggttcgtgcacacagcccagcttggagcgaacgacctacaccgaactgagatacctacagcgtgagcattgagaaagcgccacgcttcccgaagggagaaaggcggacaggtatccggtaagcggcagggtcggaacaggagagcgcacgagggagcttccagggggaaacgcctggtatctttatagtcctgtcgggtttcgccacctctgacttgagcgtcgatttttgtgatgctcgtcaggggggcggagcctatggaaaaacgccagcaacgcggcctttttacggttcctggccttttgctggccttttgctcacatgttctttcctgcgttatcccctgattctgtggataaccgtattaccgcctttgagtgagctgataccgctcgccgcagccgaacgaccgagcgcagcgagtcagtgagcgaggaagcggaagagcgcccaatacgcaaaccgcctctccccgcgcgttggccgattcattaatgcagctggcacgacaggtttcccgactggaaagcgggcagtgagcgcaacgcaattaatgtgagttagctcactcattaggcaccccaggctttacactttatgcttccgcggctcgtatgttgtgtggaattgtgagcggataacaatttcacacaggaaacagct',
            ]),
            'description' => $this->faker->paragraph,
            'author' => $this->faker->name(),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
    }

    public function csv()
    {
        return $this->state(function (array $attributes) {
            return [
                'other_names' => $this->faker->words(3, true),
                'seqfile' => null,
                'type' => Str::random(25),
                'bacterial_selection' => Str::random(25),
                'parent_vector' => $this->faker->randomNumber(6),
                'species' => Str::random(150),
                'obtention_date' => now()->toDateString(),
                'checkings' => Str::random(200),
                'related_oligos' => Str::random(15),
                'related_strains' => $this->faker->randomNumber(6),
                'files' => '["plasmids/iconfinder_batman_hero_avatar_comics_4043232.png","plasmids/harlock.jpg"]',
                'marker1' => Str::random(50),
                'marker2' => Str::random(50),
                'parent_vector_name' => Str::random(50),
                'insert' => Str::random(50),
                'inserttype' => Str::random(25),
                'reference' => $this->faker->words(2, true),
            ];
        });
    }
}
