<?php

namespace Database\Factories;

use App\Strain;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class StrainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Strain::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->bothify('##?###'),
            'species' => $this->faker->randomElement(['cat', 'dog', 'snake', 'eagle']),
            'general_background' => Str::random(25),
            'comments' => $this->faker->paragraph,
            'mating_type' => $this->faker->word(),
            'ade2' => Str::random(40),
            'his3' => Str::random(40),
            'leu2' => Str::random(40),
            'lys2' => Str::random(40),
            'met15' => Str::random(40),
            'trp1' => Str::random(40),
            'ura3' => Str::random(40),
            'ho' => Str::random(50),
            'locus1' => Str::random(75),
            'locus2' => Str::random(75),
            'locus3' => Str::random(75),
            'locus4' => Str::random(75),
            'locus5' => Str::random(75),
            'parental_strain' => Str::random(25),
            'obtained_by' => $this->faker->word(),
            'checkings' => $this->faker->word(),
            'extrachromosomal_plasmid' => $this->faker->word(),
            'cytoplasmic_character' => $this->faker->word(),
            'other_names' => $this->faker->word(),
            'reference' => $this->faker->word(),
            'author' => $this->faker->name(),
            'created_at' => now(),
            'updated_at' => now(),
            'warning' => false,
        ];
    }
}
