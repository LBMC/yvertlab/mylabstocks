<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAntibodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antibodies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('antigen', 150)->nullable();
            $table->string('host', 40)->nullable();
            $table->string('type', 40)->nullable();
            $table->string('supplier', 100)->nullable();
            $table->string('productid', 50)->nullable();
            $table->string('batchid', 50)->nullable();
            $table->string('instock', 25)->nullable();
            $table->string('orderedby', 255)->nullable();
            $table->longText('comments')->nullable();

            // path to media files that we may want to upload
            $table->longtext('files')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antibodies');
    }
}
