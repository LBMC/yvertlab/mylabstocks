<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOligosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oligos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 15)->unique();
            $table->string('sequence', 200)->nullable($value = false);
            $table->longtext('description')->nullable($value = true);
            $table->longtext('pcr_conditions_predicted')->nullable($value = true);
            $table->string('author', 200)->comment('the lab member who ordered it');
            $table->string('purif', 40)->nullable($value = true);
	    $table->string('supplier', 40)->nullable($value=true)->default($value = 'Eurogentec');
	    $table->date('obtention_date')->nullable($value=true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oligos');
    }
}
