<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCelltypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cell_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable($value = false);
            $table->string('species', 200)->nullable($value = true);
            $table->longtext('description')->nullable($value = true);
	    $table->longtext('files')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cell_types');
    }
}
