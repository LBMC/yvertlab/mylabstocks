<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePipetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pipets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serialnumber', 20);
            $table->string('brand', 100);
            $table->string('type', 50);
            $table->string('history', 20)->nullable($value=true);
	    $table->longtext('files')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pipets');
    }
}
