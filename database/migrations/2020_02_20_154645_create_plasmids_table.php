<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlasmidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plasmids', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->nullable($value = true);
            $table->string('other_names', 50)->nullable($value = true);
            $table->longtext('seqfile')->nullable($value = true);
            $table->string('type', 25)->nullable($value = true);
            $table->string('bacterial_selection', 25)->nullable($value = true);
            $table->integer('parent_vector')->nullable($value = true);
            $table->string('species', 150)->nullable($value = true);
            $table->longtext('sequence')->nullable($value = true);
            $table->longtext('description')->nullable($value = true);
            $table->string('author', 200)->comment('lab member who made/obtained it');
            $table->date('obtention_date')->nullable($value = true);
            $table->string('checkings', 200)->nullable($value = true);
            $table->string('related_oligos', 15)->nullable($value = true);
            $table->bigInteger('related_strains')->nullable($value = true);

            /* path to media files (other than the se file)
             * that we may want to upload
            */
            $table->longtext('files')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plasmids');
    }
}
