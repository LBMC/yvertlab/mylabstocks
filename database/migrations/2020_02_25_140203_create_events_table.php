<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');

            // whether the event occurred on a pipet, a notebook, a plasmid...
            $table->string('datatype');
            // when it occurred
            $table->date('eventdate');
            // what type of event it was
            $table->string('eventtype', 100)->nullable($value=true);
            // descriptioon of the event
            $table->longtext('description')->nullable($value=true);
            // path to media files that we may want to upload
            $table->longtext('files')->nullable($value = true);
	   
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
