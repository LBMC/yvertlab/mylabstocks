<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDnafeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dna_features', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longtext('sequence')->nullable($value = false);
            $table->longtext('description')->nullable($value = true);
            $table->string('category', 50)->nullable($value=false);
	    $table->longtext('comments')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dna_features');
    }
}
