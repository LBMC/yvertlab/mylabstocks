<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strains', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 100)->nullable($value = false);
            $table->string('species', 100)->nullable($value = false);
            $table->date('obtention_date')->nullable($value = true);
            $table->string('general_background', 25)->nullable($value = false);
            $table->longtext('comments')->nullable($value = true);

            $table->string('mating_type', 25)->nullable($value = true);
            $table->string('ade2', 40)->nullable($value = true);
            $table->string('his3', 40)->nullable($value = true);
            $table->string('leu2', 40)->nullable($value = true);
            $table->string('lys2', 40)->nullable($value = true);
            $table->string('met15', 40)->nullable($value = true);
            $table->string('trp1', 40)->nullable($value = true);
            $table->string('ura3', 40)->nullable($value = true);

            $table->string('ho', 50)->nullable($value = true);
            $table->string('locus1', 75)->nullable($value = true);
            $table->string('locus2', 75)->nullable($value = true);
            $table->string('locus3', 75)->nullable($value = true);
            $table->string('locus4', 75)->nullable($value = true);
            $table->string('locus5', 75)->nullable($value = true);

            $table->string('parental_strain', 25)->nullable($value = true);
            $table->string('obtained_by', 25)->nullable($value = true);
            $table->string('checkings', 225)->nullable($value = true);

            $table->string('extrachromosomal_plasmid', 50)->nullable($value = true);
            $table->string('cytoplasmic_character', 25)->nullable($value = true);

            $table->string('other_names', 25)->nullable($value = true);
            $table->string('reference', 125)->nullable($value = true);
            $table->string('author', 200)->nullable($value = true);
            $table->longtext('relevant_plasmids')->nullable($value = true);
            $table->longtext('relevant_oligos')->nullable($value = true);
            $table->longtext('files')->nullable($value = true);

	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strains');
    }
}
