<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSttrp1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('st_trp1s', function (Blueprint $table) {
            $table->bigIncrements('id');
	    $table->string('alleles', 40)->nullable($value = false);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('st_trp1s');
    }
}
