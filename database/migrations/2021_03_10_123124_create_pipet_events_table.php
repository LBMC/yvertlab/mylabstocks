<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePipetEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pipet_events', function (Blueprint $table) {
		$table->bigIncrements('id');
		$table->string('serialnumber',100)->nullable($value = false);
		$table->date('date')->nullable($value = false);
		$table->string('ownerfromnowon', 100)->nullable($value = true);
		$table->string('usagefromnowon', 100)->nullable($value = true);
		$table->string('eventtype', 100)->nullable($value = true);
		$table->longtext('comments');
		
		$table->timestamp('created_at')->useCurrent();
		$table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pipet_events');
    }
}
