<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsPlasmidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plasmids', function (Blueprint $table) {
		$table->string('marker1',50)->nullable($value=true);
		$table->string('marker2',50)->nullable($value=true);
		$table->string('parent_vector_name',50)->nullable($value=true);
		$table->string('insert',50)->nullable($value=true);
		$table->string('inserttype',25)->nullable($value=true);
		$table->string('reference',200)->nullable($value=true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plasmids', function (Blueprint $table) {
		$table->dropColumn('reference');
		$table->dropColumn('inserttype');
		$table->dropColumn('insert');
		$table->dropColumn('parent_vector_name');
		$table->dropColumn('marker2');
		$table->dropColumn('marker1');
        });
    }
}
