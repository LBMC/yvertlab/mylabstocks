<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorColumnToDnafeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dna_features', function (Blueprint $table) {
            $table->string('color',30)->nullable($value = true)->default("black");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dna_features', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
