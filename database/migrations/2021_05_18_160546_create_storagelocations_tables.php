<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoragelocationsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // create table for storing rooms
       Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 100)->nullable($value = false)->unique();
            $table->string('building', 100)->nullable($value = true);
            $table->string('floor', 100)->nullable($value = true);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        // create table for storing freezers
        Schema::create('freezers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('type', 100)->nullable($value = false)->default('-80degC');
            $table->string('name', 100)->nullable($value = false)->unique();

            $table->bigInteger('room_id')->unsigned()->nullable($value = true);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

        // create table for storing boxes
        Schema::create('boxes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 100)->nullable($value = true);

            $table->bigInteger('freezer_id')->unsigned()->nullable($value = true);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
        Schema::dropIfExists('freezers');
        Schema::dropIfExists('rooms');
    }
}
