<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWellplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table for storing 96-well plates
        Schema::create('wellplates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('format')->nullable($value = false);
            $table->string('name', 100)->nullable($value = true);
            $table->string('author', 100)->nullable($value = true);
            $table->string('contenttype', 100)->nullable($value = true);
	    $table->integer('thawings')->default(0)->nullable($value = false);
	    $table->date('freezedate')->nullable($value=false);
            $table->longtext('files')->nullable($value = true);
            $table->bigInteger('freezer_id')->unsigned()->nullable($value = true);
            $table->longtext('comments')->nullable($value = true);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wellplates');
    }
}
