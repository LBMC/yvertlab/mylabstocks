<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRackinfoAndFormatToBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boxes', function (Blueprint $table) {
            $table->integer('nrows')->nullable($value = false);
            $table->integer('ncols')->nullable($value = false);
            $table->integer('racknumber')->nullable($value = true);
            $table->integer('positioninrack')->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boxes', function (Blueprint $table) {
            $table->dropColumn('positioninrack');
            $table->dropColumn('racknumber');
            $table->dropColumn('ncols');
            $table->dropColumn('nrows');
        });
    }
}
