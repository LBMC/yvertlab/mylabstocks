<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxpositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxpositions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('box_id')->nullable($value = false);
            $table->integer('row')->nullable($value = false);
            $table->integer('column')->nullable($value = false);
            $table->string('name', 30)->nullable($value = false)->unique();

            $table->timestamps();

            $table->foreign('box_id')->references('id')->on('boxes')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxpositions');
    }
}
