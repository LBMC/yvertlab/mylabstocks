<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectStrainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_strains', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('strain_id')->change();
        });

        Schema::table('project_strains', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('strain_id')->references('id')->on('strains')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_strains', function (Blueprint $table) {
            $table->dropForeign(['strain_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_strains', function (Blueprint $table) {
            $table->dropIndex('project_strains_strain_id_foreign');
            $table->dropIndex('project_strains_project_id_foreign');
        });

       Schema::table('project_strains', function (Blueprint $table) {
            $table->bigInteger('strain_id')->change();
	    $table->bigInteger('project_id')->change();
        });

    }
}
