<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectAntibodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_antibodies', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('antibody_id')->change();
        });

        Schema::table('project_antibodies', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('antibody_id')->references('id')->on('antibodies')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_antibodies', function (Blueprint $table) {
            $table->dropForeign(['antibody_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_antibodies', function (Blueprint $table) {
            $table->dropIndex('project_antibodies_antibody_id_foreign');
            $table->dropIndex('project_antibodies_project_id_foreign');
        });

       Schema::table('project_antibodies', function (Blueprint $table) {
            $table->bigInteger('antibody_id')->change();
	    $table->bigInteger('project_id')->change();
        });


    }
}
