<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectCelllinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_celllines', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('cellline_id')->change();
        });

        Schema::table('project_celllines', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('cellline_id')->references('id')->on('cell_lines')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_celllines', function (Blueprint $table) {
            $table->dropForeign(['cellline_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_celllines', function (Blueprint $table) {
            $table->dropIndex('project_celllines_cellline_id_foreign');
            $table->dropIndex('project_celllines_project_id_foreign');
        });

       Schema::table('project_celllines', function (Blueprint $table) {
            $table->bigInteger('cellline_id')->change();
	    $table->bigInteger('project_id')->change();
        });


    }
}


