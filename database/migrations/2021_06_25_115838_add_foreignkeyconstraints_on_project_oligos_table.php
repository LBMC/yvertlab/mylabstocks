<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectOligosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_oligos', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('oligo_id')->change();
        });

        Schema::table('project_oligos', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('oligo_id')->references('id')->on('oligos')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_oligos', function (Blueprint $table) {
            $table->dropForeign(['oligo_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_oligos', function (Blueprint $table) {
            $table->dropIndex('project_oligos_oligo_id_foreign');
            $table->dropIndex('project_oligos_project_id_foreign');
        });

       Schema::table('project_oligos', function (Blueprint $table) {
            $table->bigInteger('oligo_id')->change();
	    $table->bigInteger('project_id')->change();
        });


    }
}
