<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectPlasmidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_plasmids', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('plasmid_id')->change();
        });

        Schema::table('project_plasmids', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('plasmid_id')->references('id')->on('plasmids')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_plasmids', function (Blueprint $table) {
            $table->dropForeign(['plasmid_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_plasmids', function (Blueprint $table) {
            $table->dropIndex('project_plasmids_plasmid_id_foreign');
            $table->dropIndex('project_plasmids_project_id_foreign');
        });

       Schema::table('project_plasmids', function (Blueprint $table) {
            $table->bigInteger('plasmid_id')->change();
	    $table->bigInteger('project_id')->change();
        });


    }
}
