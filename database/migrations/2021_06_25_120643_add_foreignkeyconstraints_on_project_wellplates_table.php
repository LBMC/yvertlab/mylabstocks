<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignkeyconstraintsOnProjectWellplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_wellplates', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->change();
	    $table->unsignedBigInteger('wellplate_id')->change();
        });

        Schema::table('project_wellplates', function (Blueprint $table) {
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('wellplate_id')->references('id')->on('wellplates')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_wellplates', function (Blueprint $table) {
            $table->dropForeign(['wellplate_id']);
            $table->dropForeign(['project_id']);
        });

        Schema::table('project_wellplates', function (Blueprint $table) {
            $table->dropIndex('project_wellplates_wellplate_id_foreign');
            $table->dropIndex('project_wellplates_project_id_foreign');
        });

       Schema::table('project_wellplates', function (Blueprint $table) {
            $table->bigInteger('wellplate_id')->change();
	    $table->bigInteger('project_id')->change();
        });


    }
}
