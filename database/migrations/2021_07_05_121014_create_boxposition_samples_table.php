<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxpositionSamplesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxposition_samples', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('boxposition_id')->unique();
            $table->unsignedBigInteger('passage_id');
            $table->timestamps();

            $table->foreign('boxposition_id')->references('id')->on('boxpositions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('passage_id')->references('id')->on('passages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxposition_samples');
    }
}
