<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPipetUsageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('pipet_usage');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { 
	Schema::create('pipet_usage', function (Blueprint $table) {
        	$table->bigIncrements('id');
		$table->string('name', 100)->nullable($value = false);

		$table->timestamp('created_at')->useCurrent();
		$table->timestamp('updated_at')->useCurrent();
        });
    }
}
