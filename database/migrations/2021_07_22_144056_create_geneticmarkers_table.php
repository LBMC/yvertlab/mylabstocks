<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneticmarkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geneticmarkers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('allele', 100)->nullable($value = false)->unique();
            $table->bigInteger('species_id')->unsigned()->nullable();
            $table->string('gene', 100)->nullable();
            $table->longtext('comments')->nullable();
            $table->boolean('episomal')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::table('geneticmarkers', function (Blueprint $table) {
            $table->foreign('species_id')->references('id')->on('species')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geneticmarkers');
    }
}
