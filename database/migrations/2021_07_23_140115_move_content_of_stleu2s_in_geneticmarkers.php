<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Geneticmarker;
use App\Yeast\StLeu2;
use App\Species;

class MoveContentOfStleu2sInGeneticmarkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // Inspect if any alleles to move
       // and skip any action if not
       if ( StLeu2::count() > 0 ){

          // gene-specific variables
          $alleles = StLeu2::all();
          $gene = 'LEU2';
          $msg = "Imported from older table st_leu2s";

          // get the id of S. cerevisiae species if it exists, create it if not.
          $speciesname = "S. cerevisiae";
          if (Species::where('name', $speciesname)->doesntExist()){
             $speciesid = Species::insertGetId(
                  ['name' => $speciesname ]
             );
          }  else {
             $spe = Species::where('name', $speciesname)->first();
             $speciesid = $spe->id;
          }

          // create records
          foreach ($alleles as $allele){
               if (!is_null($allele->alleles) AND $allele->alleles != "unknown" AND $allele->alleles != "")
                  $geneticmarker = Geneticmarker::create([
                                        'allele' => $allele->alleles,
                                        'species_id' => $speciesid,
                                        'gene' => $gene,
                                        'comments' => $msg,
                                        'episomal' => false,
                                   ]);
          }
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $msg = "Imported from older table st_leu2s";

        Geneticmarker::where('comments', $msg)->delete(); 
    }
}
