<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Geneticmarker;
use App\Yeast\StMat;
use App\Species;

class MoveContentOfStmatsInGeneticmarkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       // Inspect if any alleles to move
       // and skip any action if not
       if ( StMat::count() > 0 ){

          // gene-specific variables
          $alleles = StMat::all();
          $gene = 'MAT';
          $msg = "Imported from older table st_mats";

          // get the id of S. cerevisiae species if it exists, create it if not.
          $speciesname = "S. cerevisiae";
          if (Species::where('name', $speciesname)->doesntExist()){
             $speciesid = Species::insertGetId(
                  ['name' => $speciesname ]
             );
          }  else {
             $spe = Species::where('name', $speciesname)->first();
             $speciesid = $spe->id;
          }
 
          // create records
          foreach ($alleles as $allele){
               if (!is_null($allele->alleles) AND $allele->alleles != "unknown" AND $allele->alleles != "")
                  $geneticmarker = Geneticmarker::create([
                                        'allele' => $allele->alleles,
                                        'species_id' => $speciesid,
                                        'gene' => $gene,
                                        'comments' => $msg,
                                        'episomal' => false,
                                   ]);
          }
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        $msg = "Imported from older table st_mats";

        Geneticmarker::where('comments', $msg)->delete(); 
    }
}
