<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigincrements('id');

            $table->string('name', 100)->nullable($value = false);
            $table->string('type', 100)->nullable($value = true);
            $table->string('supplier', 100)->nullable($value = false);
            $table->string('supplierproductid', 50)->nullable($value = false);
            $table->string('producer', 100)->nullable($value = true);
            $table->string('producerproductid', 50)->nullable($value = true);
	    $table->string('weblink', 200)->nullable($value = true);
	    $table->float('price', '2')->nullable($value = true);
	    $table->date('lastorderdate')->nullable($value=false);
            $table->string('orderedby', 255)->nullable($value = true);
            $table->string('contactforquote', 100)->nullable($value = true);
            $table->string('codenacre', 15)->nullable($value = true);
            $table->longtext('comments')->nullable($value = true);

            // path to media files that we may want to upload
	    $table->longtext('files')->nullable($value = true);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
