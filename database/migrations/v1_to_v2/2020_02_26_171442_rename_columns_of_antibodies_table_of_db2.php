<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsOfAntibodiesTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('antibodies', function (Blueprint $table) {
           $table->renameColumn('Antigen', 'antigen');
           $table->renameColumn('Host', 'host');
           $table->renameColumn('Type', 'type');
           $table->renameColumn('Supplier', 'supplier');
           $table->renameColumn('ProductID', 'productid');
           $table->renameColumn('Batch_Reference', 'batchid');
           $table->renameColumn('InStock', 'instock');
           $table->renameColumn('Ordered_By', 'orderedby');
           $table->renameColumn('Comments', 'comments');

// next migration:
// remove deprecated columns
// add collumn files
// add columns timestamps

// modify columns attributes
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('antibodies', function (Blueprint $table) {
           $table->renameColumn('antigen', 'Antigen');
           $table->renameColumn('host', 'Host');
           $table->renameColumn('type', 'Type');
           $table->renameColumn('supplier', 'Supplier');
           $table->renameColumn('productid', 'ProductID');
           $table->renameColumn('batchid','Batch_Reference');
           $table->renameColumn('instock', 'InStock');
           $table->renameColumn('orderedby', 'Ordered_By');
           $table->renameColumn('comments', 'Comments');
           //             
        });
    }
}
