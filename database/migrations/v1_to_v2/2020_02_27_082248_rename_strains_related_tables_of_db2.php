<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameStrainsRelatedTablesOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->rename('st_ADE2', 'st_ade2');
        Schema::connection('mysql2')->rename('st_HIS3', 'st_his3');
        Schema::connection('mysql2')->rename('st_LEU2', 'st_leu2');
        Schema::connection('mysql2')->rename('st_LYS2', 'st_lys2');
        Schema::connection('mysql2')->rename('st_MAT', 'st_mat');
        Schema::connection('mysql2')->rename('st_MET15', 'st_met15');
        Schema::connection('mysql2')->rename('st_TRP1', 'st_trp1');
        Schema::connection('mysql2')->rename('st_URA3', 'st_ura3');
        Schema::connection('mysql2')->rename('st_general_backgrounds', 'backgrounds');
 }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->rename('st_ade2', 'st_ADE2');
        Schema::connection('mysql2')->rename('st_his3', 'st_HIS3');
        Schema::connection('mysql2')->rename('st_leu2', 'st_LEU2');
        Schema::connection('mysql2')->rename('st_lys2', 'st_LYS2');
        Schema::connection('mysql2')->rename('st_mat', 'st_MAT');
        Schema::connection('mysql2')->rename('st_met15', 'st_MET15');
        Schema::connection('mysql2')->rename('st_trp1', 'st_TRP1');
        Schema::connection('mysql2')->rename('st_ura3', 'st_URA3');
        Schema::connection('mysql2')->rename('backgrounds', 'st_general_backgrounds');
    }
}
