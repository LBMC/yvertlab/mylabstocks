<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveObsoleteColumnsOfAntibodiesTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('antibodies', function (Blueprint $table) {
		$table->dropColumn('Location');
		$table->dropColumn('Dilution-WB');
		$table->dropColumn('Volume-ChIP');
		$table->dropColumn('storage_minus20freezers');
                $table->dropColumn('storage_minus80freezers');
		$table->dropColumn('storage_fridges');
		$table->dropColumn('storage_rooms');
		
		$table->renameColumn('Date', 'obtention_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('antibodies', function (Blueprint $table) {
		$table->char('Location',40);
		$table->text('Dilution-WB');
		$table->text('Volume-ChIP');
		$table->char('storage_minus20freezers',100);
		$table->char('storage_minus80freezers',100);
		$table->char('storage_fridges',100);
		$table->char('storage_rooms',100);

		$table->renameColumn('obtention_date', 'Date');
        });
    }
}
