<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameCellLinesRelatedTablesOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::connection('mysql2')->rename('cl_name', 'cell_lines');
	    Schema::connection('mysql2')->rename('cl_passages', 'passages');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::connection('mysql2')->rename('passages', 'cl_passages');
	    Schema::connection('mysql2')->rename('cell_lines', 'cl_name');
    }
}
