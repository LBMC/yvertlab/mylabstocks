<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsOfCellLinesTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('cell_lines', function (Blueprint $table) {
		$table->renameColumn('Author', 'author');
                $table->renameColumn('type', 'celltype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('cell_lines', function (Blueprint $table) {
	       $table->renameColumn('author', 'Author');
               $table->renameColumn('celltype', 'type');
        });
    }
}
