<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsOfPassagesTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('passages', function (Blueprint $table) {
		$table->renameColumn('ID', 'id');
		$table->renameColumn('date_of_freezing', 'freezing_date');
		$table->renameColumn('name', 'cellline');
		$table->renameColumn('passage', 'name');
		$table->renameColumn('Author', 'author');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('passages', function (Blueprint $table) {
		$table->renameColumn('author', 'Author');
		$table->renameColumn('name', 'passage');
		$table->renameColumn('cellline', 'name');
		$table->renameColumn('freezing_date', 'date_of_freezing');
		$table->renameColumn('id', 'ID');
        });
    }
}
