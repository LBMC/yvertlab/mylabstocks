<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsOfDnaFeaturesTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('dna_features', function (Blueprint $table) {
		$table->renameColumn('Sequence', 'sequence');
		$table->renameColumn('Date_', 'created_at');
		$table->renameColumn('Description', 'name');
		$table->renameColumn('Comments', 'comments');
		$table->renameColumn('Category', 'category');
		$table->renameColumn('Author', 'author');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('dna_features', function (Blueprint $table) {
		$table->renameColumn('comments', 'Comments');
		$table->renameColumn('name', 'Description');
		$table->renameColumn('created_at', 'Date_');
		$table->renameColumn('sequence', 'Sequence');
		$table->renameColumn('category', 'Category');
		$table->renameColumn('author', 'Author');
        });
    }
}
