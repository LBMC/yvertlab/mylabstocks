<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNotebooksTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('notebooks', function (Blueprint $table) {
		$table->renameColumn('ID', 'id');
		$table->renameColumn('Begin_Date', 'begindate');
		$table->renameColumn('End_Date', 'enddate');
		$table->renameColumn('Author', 'author');
		$table->renameColumn('Serial_Number', 'serialnumber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('notebooks', function (Blueprint $table) {
		$table->renameColumn('serialnumber', 'Serial_Number');
		$table->renameColumn('author', 'Author');
		$table->renameColumn('enddate', 'End_Date');
		$table->renameColumn('begindate', 'Begin_Date');
		$table->renameColumn('id', 'ID');
        });
    }
}
