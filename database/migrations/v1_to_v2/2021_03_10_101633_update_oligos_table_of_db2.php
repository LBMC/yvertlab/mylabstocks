<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOligosTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('oligos', function (Blueprint $table) {
		$table->renameColumn('Author', 'author');
		$table->renameColumn('Date_', 'obtention_date');
		$table->renameColumn('Sequence', 'sequence');
		$table->renameColumn('id', 'name');
		$table->renameColumn('Description', 'description');
		$table->renameColumn('PCR_conditions_predicted', 'pcr_conditions_predicted');
		$table->renameColumn('Purif', 'purif');

		$table->dropColumn('storage_minus20freezers');
                $table->dropColumn('storage_minus80freezers');
		$table->dropColumn('storage_fridges');
		$table->dropColumn('storage_rooms');
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::connection('mysql2')->table('oligos', function (Blueprint $table) {

		$table->char('storage_minus20freezers',100);
		$table->char('storage_minus80freezers',100);
		$table->char('storage_fridges',100);
		$table->char('storage_rooms',100);

		$table->renameColumn('purif', 'Purif');
		$table->renameColumn('pcr_conditions_predicted', 'PCR_conditions_predicted');
		$table->renameColumn('description', 'Description');
		$table->renameColumn('name', 'id');
		$table->renameColumn('sequence', 'Sequence');
		$table->renameColumn('obtention_date', 'Date_');
		$table->renameColumn('author', 'Author');
            
        });
    }
}
