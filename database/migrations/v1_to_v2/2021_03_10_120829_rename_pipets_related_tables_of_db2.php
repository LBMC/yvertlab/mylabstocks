<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePipetsRelatedTablesOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->rename('pip_stock', 'pipets');
        Schema::connection('mysql2')->rename('pip_history', 'pipet_events');
        Schema::connection('mysql2')->rename('pip_usage', 'pipet_usage');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->rename('pipet_usage', 'pip_usage');
        Schema::connection('mysql2')->rename('pipet_events', 'pip_history');
        Schema::connection('mysql2')->rename('pipets', 'pip_stock');
    }
}
