<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePipetsTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('pipets', function (Blueprint $table) {
		$table->renameColumn('ID', 'id');
		$table->renameColumn('Serial_Number', 'serialnumber');
		$table->renameColumn('Marque', 'brand');
		$table->renameColumn('Type', 'type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('pipets', function (Blueprint $table) {
		$table->renameColumn('type', 'Type');
		$table->renameColumn('brand', 'Marque');
		$table->renameColumn('serialnumber', 'Serial_Number');
            	$table->renameColumn('id', 'ID');
        });
    }
}
