<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePipetEventsTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('pipet_events', function (Blueprint $table) {
            	$table->renameColumn('ID', 'id');
		$table->renameColumn('Serial_Number', 'serialnumber');
		$table->renameColumn('Date', 'date');
		$table->renameColumn('Owner_fromNowOn', 'ownerfromnowon');
		$table->renameColumn('Usage_fromNowOn', 'usagefromnowon');
		$table->renameColumn('Event_Type', 'eventtype');
		$table->renameColumn('Comments', 'comments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('pipet_events', function (Blueprint $table) {
		$table->renameColumn('comments', 'Comments');
		$table->renameColumn('eventtype', 'Event_Type');
		$table->renameColumn('usagefromnowon', 'Usage_fromNowOn');
		$table->renameColumn('ownerfromnowon', 'Owner_fromNowOn');
		$table->renameColumn('date', 'Date');
            	$table->renameColumn('serialnumber', 'Serial_Number');
            	$table->renameColumn('id', 'ID');
        });
    }
}
