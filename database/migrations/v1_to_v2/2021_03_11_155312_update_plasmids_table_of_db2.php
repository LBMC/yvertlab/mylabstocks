<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePlasmidsTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('plasmids', function (Blueprint $table) {
		
	    $table->dropColumn('storage_minus20freezers');
	    $table->dropColumn('storage_minus80freezers');
	    $table->dropColumn('storage_fridges');
	    $table->dropColumn('storage_rooms');

	    $table->dropColumn('image_file');
            $table->dropColumn('Promoter');
            $table->dropColumn('Reporter');
            $table->dropColumn('Tags');
	    
	    $table->renameColumn('Link_to_File', 'seqfilenamev1');
            $table->renameColumn('Author', 'author');
            $table->renameColumn('Reference_', 'reference');
            $table->renameColumn('Construction_Description', 'description');
            $table->renameColumn('Insert_Type', 'inserttype');
            $table->renameColumn('Insert_', 'insert');
            $table->renameColumn('parent_vector', 'parent_vector_name');
            $table->renameColumn('Bacterial_selection', 'bacterial_selection');
            $table->renameColumn('Marker_2', 'marker2');
            $table->renameColumn('Marker_1', 'marker1');
            $table->renameColumn('Type_', 'type');
            $table->renameColumn('Checkings', 'checkings');
            $table->renameColumn('date_', 'obtention_date');
            $table->renameColumn('Other_names', 'other_names');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	Schema::connection('mysql2')->table('plasmids', function (Blueprint $table) { 
	    $table->renameColumn('other_names', 'Other_names');
            $table->renameColumn('obtention_date', 'date_');
            $table->renameColumn('checkings', 'Checkings');
            $table->renameColumn('type', 'Type_');
            $table->renameColumn('marker1', 'Marker_1');
            $table->renameColumn('marker2', 'Marker_2');
            $table->renameColumn('bacterial_selection', 'Bacterial_selection');
            $table->renameColumn('parent_vector_name', 'parent_vector');
            $table->renameColumn('insert', 'Insert_');
            $table->renameColumn('inserttype', 'Insert_Type');
            $table->renameColumn('description', 'Construction_Description');
            $table->renameColumn('reference', 'Reference_');
            $table->renameColumn('author', 'Author');
	    $table->renameColumn('seqfilenamev1', 'Link_to_File');

	    $table->char('Tags',25);
	    $table->char('Reporter',25);
	    $table->char('Promoter',25);
	    $table->char('image_file',100);

	    $table->char('storage_minus20freezers',100);
	    $table->char('storage_minus80freezers',100);
	    $table->char('storage_fridges',100);
	    $table->char('storage_rooms',100);

        });
    }
}
