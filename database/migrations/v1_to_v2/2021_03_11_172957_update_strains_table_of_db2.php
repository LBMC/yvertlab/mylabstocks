<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStrainsTableOfDb2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql2')->table('strains', function (Blueprint $table) {
            $table->dropColumn('storage_minus20freezers');
	    $table->dropColumn('storage_minus80freezers');
	    $table->dropColumn('storage_fridges');
	    $table->dropColumn('storage_rooms');
	    $table->dropColumn('Last_modified');

	    $table->renameColumn('Name_', 'name');
	    $table->renameColumn('Date_', 'obtention_date');
	    $table->renameColumn('General_Background', 'general_background');
	    $table->renameColumn('Mating_Type', 'mating_type');
	    $table->renameColumn('ADE2', 'ade2');
	    $table->renameColumn('HIS3', 'his3');
	    $table->renameColumn('LEU2', 'leu2');
	    $table->renameColumn('LYS2', 'lys2');
	    $table->renameColumn('MET15', 'met15');
	    $table->renameColumn('TRP1', 'trp1');
	    $table->renameColumn('URA3', 'ura3');
	    $table->renameColumn('HO_', 'ho');
	    $table->renameColumn('Parental_strain', 'parental_strain');
	    $table->renameColumn('Obtained_by', 'obtained_by');
	    $table->renameColumn('Other_names', 'other_names');
	    $table->renameColumn('Reference_', 'reference');
            $table->renameColumn('Checkings', 'checkings');
	    $table->renameColumn('Author', 'author');
	    $table->renameColumn('Comments', 'comments');
	    $table->renameColumn('Cytoplasmic_Character', 'cytoplasmic_character');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql2')->table('strains', function (Blueprint $table) {
	    $table->renameColumn('cytoplasmic_character', 'Cytoplasmic_Character');
	    $table->renameColumn('comments', 'Comments');
	    $table->renameColumn('author', 'Author');
	    $table->renameColumn('checkings', 'Checkings');
	    $table->renameColumn('reference', 'Reference_');
	    $table->renameColumn('other_names', 'Other_names');
	    $table->renameColumn('obtained_by', 'Obtained_by');
	    $table->renameColumn('parental_strain', 'Parental_strain');
	    $table->renameColumn('ho', 'HO_');
	    $table->renameColumn('ura3', 'URA3');
	    $table->renameColumn('trp1', 'TRP1');
	    $table->renameColumn('met15', 'MET15');
	    $table->renameColumn('lys2', 'LYS2');
	    $table->renameColumn('leu2', 'LEU2');
	    $table->renameColumn('his3', 'HIS3');
	    $table->renameColumn('ade2', 'ADE2');
	    $table->renameColumn('mating_type', 'Mating_Type');
	    $table->renameColumn('general_background', 'General_Background');
	    $table->renameColumn('obtention_date', 'Date_');
            $table->renameColumn('name', 'Name_');

            $table->date('Last_modified')->default(now());
	    $table->char('storage_minus20freezers',100);
	    $table->char('storage_minus80freezers',100);
	    $table->char('storage_fridges',100);
	    $table->char('storage_rooms',100);

        });
    }
}
