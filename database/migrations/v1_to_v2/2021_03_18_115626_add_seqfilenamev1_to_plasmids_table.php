<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeqfilenamev1ToPlasmidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plasmids', function (Blueprint $table) {
            $table->string('seqfilenamev1',100)->nullable($value=true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plasmids', function (Blueprint $table) {
	     $table->dropColumn('seqfilenamev1');
        });
    }
}
