<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AntibodiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('antibodies')->insert([
            'antigen' => 'Actin C4',
            'instock' => 'available',
            'productid' => '69100',
            'host' => 'mouse',
            'batchid' => '9045J',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('antibodies')->insert([
            'antigen' => 'Myc',
            'instock' => 'available',
            'productid' => '06-340',
            'host' => 'rabbit',
            'batchid' => 'DAM1437269',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);


    }
}
