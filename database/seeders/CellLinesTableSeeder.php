<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CellLinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cell_lines')->insert([
            'name' => 'GM18487',
            'celltype' => 'LCL',
            'description' => 'Lymphoblastoid cell line. EBV-transformed B-cells from the HapMap/1000genomes collection',
            'origin' => 'Coriell Institute for Medical Research',
            'author' => 'Gerard.Triqueneaux',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        
    }
}
