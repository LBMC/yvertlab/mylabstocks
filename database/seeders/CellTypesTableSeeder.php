<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CellTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cell_types')->insert([
            'name' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('cell_types')->insert([
            'name' => 'LCL',
            'species' => 'Human',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('cell_types')->insert([
            'name' => 'HEK293',
            'species' => 'Human',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('cell_types')->insert([
            'name' => 'HeLa',
            'species' => 'Human',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('cell_types')->insert([
            'name' => 'MEF',
            'species' => 'Mus. musculus',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('cell_types')->insert([
            'name' => 'CHO',
            'species' => 'Hamster',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);


    }
}
