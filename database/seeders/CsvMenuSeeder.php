<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CsvMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_items')->insert([
            'menu_id' => 1,
            'title' => 'Csv',
            'target' => '_self',
            'icon_class' => 'voyager-upload',
            'color' => '#CC0000',
            'parent_id' => null,
            'order' => null,
            'created_at' => now(),
            'updated_at' => now(),
            'route' => 'csv.upload.create',
            'parameters' => null,
        ]);
    }
}
