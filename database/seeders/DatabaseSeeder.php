<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AntibodiesTableSeeder::class);
        $this->call(AvailabilitiesTableSeeder::class);
        $this->call(BackgroundsTableSeeder::class);
        $this->call(BacterialSelectionsTableSeeder::class);
        $this->call(CellLinesTableSeeder::class);
        $this->call(CellTypesTableSeeder::class);
        $this->call(DnaFeaturesTableSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(HostsTableSeeder::class);
        $this->call(OligosTableSeeder::class);
        $this->call(PipetTypesTableSeeder::class);
        $this->call(PlasmidsTableSeeder::class);
        $this->call(PlasmidTypesTableSeeder::class);
        $this->call(PurifsTableSeeder::class);
        $this->call(SpeciesTableSeeder::class);
        $this->call(SuppliersTableSeeder::class);
        $this->call(StAuxotrophiesTablesSeeder::class);
    }
}
