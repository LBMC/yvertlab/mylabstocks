<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DnaFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dna_features')->insert([
            'sequence' => 'tcatgtaattagttatgtcacgcttacattcacgccctccccccacatccgctctaaccgaaaaggaaggagttagacaacctgaagtctaggtccctatttatttttttatagttatgttagtattaagaacgttatttatatttcaaatttttcttttttttctgtacagacgcgtgtacgcatgtaacattatactgaaaaccttgcttgagaaggttttgggacgctcgaaggctttaatttgcggccg',
            'name' => 'CYC1-terminator',
            'comments' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('dna_features')->insert([
            'sequence' => 'ATAACTTCGTATAATGTATGCTATACGAAGTTAT',
            'name' => 'LoxP',
            'comments' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
