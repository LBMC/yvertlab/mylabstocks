<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class FrontendMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::firstOrCreate([
            'name' => 'frontend',
        ]);
        MenuItem::firstOrCreate([
            'menu_id' => $menu->id,
            'title' => 'Antibodies',
            'url' => '',
            'target' => '_self',
            'route' => 'frontend.antibodies.index',
            'order' => 1,
        ]);
        MenuItem::firstOrCreate([
            'menu_id' => $menu->id,
            'title' => 'Oligos',
            'url' => '',
            'target' => '_self',
            'route' => 'frontend.oligos.index',
            'order' => 2,
        ]);
        MenuItem::firstOrCreate([
            'menu_id' => $menu->id,
            'title' => 'Plasmids',
            'url' => '',
            'target' => '_self',
            'route' => 'frontend.plasmids.index',
            'order' => 3,
        ]);
        MenuItem::firstOrCreate([
            'menu_id' => $menu->id,
            'title' => 'Strains',
            'url' => '',
            'target' => '_self',
            'route' => 'frontend.strains.index',
            'order' => 4,
        ]);
    }
}
