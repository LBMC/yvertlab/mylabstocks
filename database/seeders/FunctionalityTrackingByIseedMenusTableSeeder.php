<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FunctionalityTrackingByIseedMenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'frontend',
                'created_at' => '2020-02-04 16:27:18',
                'updated_at' => '2021-12-14 10:43:38',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'primary',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'social',
                'created_at' => '2020-06-26 16:02:35',
                'updated_at' => '2020-06-26 16:02:35',
            ),
        ));
        
        
    }
}