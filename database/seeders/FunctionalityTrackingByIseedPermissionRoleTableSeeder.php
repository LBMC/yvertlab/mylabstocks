<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FunctionalityTrackingByIseedPermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'permission_id' => 1,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 1,
                'role_id' => 13,
            ),
            2 => 
            array (
                'permission_id' => 1,
                'role_id' => 15,
            ),
            3 => 
            array (
                'permission_id' => 1,
                'role_id' => 16,
            ),
            4 => 
            array (
                'permission_id' => 1,
                'role_id' => 20,
            ),
            5 => 
            array (
                'permission_id' => 1,
                'role_id' => 21,
            ),
            6 => 
            array (
                'permission_id' => 1,
                'role_id' => 24,
            ),
            7 => 
            array (
                'permission_id' => 1,
                'role_id' => 25,
            ),
            8 => 
            array (
                'permission_id' => 1,
                'role_id' => 27,
            ),
            9 => 
            array (
                'permission_id' => 1,
                'role_id' => 28,
            ),
            10 => 
            array (
                'permission_id' => 1,
                'role_id' => 29,
            ),
            11 => 
            array (
                'permission_id' => 1,
                'role_id' => 31,
            ),
            12 => 
            array (
                'permission_id' => 1,
                'role_id' => 32,
            ),
            13 => 
            array (
                'permission_id' => 1,
                'role_id' => 33,
            ),
            14 => 
            array (
                'permission_id' => 1,
                'role_id' => 34,
            ),
            15 => 
            array (
                'permission_id' => 1,
                'role_id' => 35,
            ),
            16 => 
            array (
                'permission_id' => 1,
                'role_id' => 37,
            ),
            17 => 
            array (
                'permission_id' => 1,
                'role_id' => 38,
            ),
            18 => 
            array (
                'permission_id' => 1,
                'role_id' => 39,
            ),
            19 => 
            array (
                'permission_id' => 1,
                'role_id' => 40,
            ),
            20 => 
            array (
                'permission_id' => 1,
                'role_id' => 41,
            ),
            21 => 
            array (
                'permission_id' => 1,
                'role_id' => 42,
            ),
            22 => 
            array (
                'permission_id' => 1,
                'role_id' => 43,
            ),
            23 => 
            array (
                'permission_id' => 1,
                'role_id' => 44,
            ),
            24 => 
            array (
                'permission_id' => 1,
                'role_id' => 45,
            ),
            25 => 
            array (
                'permission_id' => 1,
                'role_id' => 46,
            ),
            26 => 
            array (
                'permission_id' => 1,
                'role_id' => 47,
            ),
            27 => 
            array (
                'permission_id' => 1,
                'role_id' => 48,
            ),
            28 => 
            array (
                'permission_id' => 2,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 2,
                'role_id' => 39,
            ),
            30 => 
            array (
                'permission_id' => 3,
                'role_id' => 1,
            ),
            31 => 
            array (
                'permission_id' => 4,
                'role_id' => 1,
            ),
            32 => 
            array (
                'permission_id' => 4,
                'role_id' => 28,
            ),
            33 => 
            array (
                'permission_id' => 4,
                'role_id' => 29,
            ),
            34 => 
            array (
                'permission_id' => 4,
                'role_id' => 31,
            ),
            35 => 
            array (
                'permission_id' => 4,
                'role_id' => 32,
            ),
            36 => 
            array (
                'permission_id' => 4,
                'role_id' => 33,
            ),
            37 => 
            array (
                'permission_id' => 4,
                'role_id' => 34,
            ),
            38 => 
            array (
                'permission_id' => 4,
                'role_id' => 35,
            ),
            39 => 
            array (
                'permission_id' => 4,
                'role_id' => 37,
            ),
            40 => 
            array (
                'permission_id' => 4,
                'role_id' => 39,
            ),
            41 => 
            array (
                'permission_id' => 4,
                'role_id' => 40,
            ),
            42 => 
            array (
                'permission_id' => 4,
                'role_id' => 41,
            ),
            43 => 
            array (
                'permission_id' => 5,
                'role_id' => 1,
            ),
            44 => 
            array (
                'permission_id' => 5,
                'role_id' => 39,
            ),
            45 => 
            array (
                'permission_id' => 5,
                'role_id' => 40,
            ),
            46 => 
            array (
                'permission_id' => 5,
                'role_id' => 41,
            ),
            47 => 
            array (
                'permission_id' => 6,
                'role_id' => 1,
            ),
            48 => 
            array (
                'permission_id' => 6,
                'role_id' => 39,
            ),
            49 => 
            array (
                'permission_id' => 6,
                'role_id' => 40,
            ),
            50 => 
            array (
                'permission_id' => 6,
                'role_id' => 41,
            ),
            51 => 
            array (
                'permission_id' => 6,
                'role_id' => 48,
            ),
            52 => 
            array (
                'permission_id' => 7,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 7,
                'role_id' => 39,
            ),
            54 => 
            array (
                'permission_id' => 7,
                'role_id' => 40,
            ),
            55 => 
            array (
                'permission_id' => 7,
                'role_id' => 41,
            ),
            56 => 
            array (
                'permission_id' => 8,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 8,
                'role_id' => 39,
            ),
            58 => 
            array (
                'permission_id' => 8,
                'role_id' => 40,
            ),
            59 => 
            array (
                'permission_id' => 9,
                'role_id' => 1,
            ),
            60 => 
            array (
                'permission_id' => 9,
                'role_id' => 39,
            ),
            61 => 
            array (
                'permission_id' => 9,
                'role_id' => 40,
            ),
            62 => 
            array (
                'permission_id' => 10,
                'role_id' => 1,
            ),
            63 => 
            array (
                'permission_id' => 10,
                'role_id' => 39,
            ),
            64 => 
            array (
                'permission_id' => 10,
                'role_id' => 40,
            ),
            65 => 
            array (
                'permission_id' => 11,
                'role_id' => 1,
            ),
            66 => 
            array (
                'permission_id' => 11,
                'role_id' => 39,
            ),
            67 => 
            array (
                'permission_id' => 11,
                'role_id' => 40,
            ),
            68 => 
            array (
                'permission_id' => 11,
                'role_id' => 41,
            ),
            69 => 
            array (
                'permission_id' => 12,
                'role_id' => 1,
            ),
            70 => 
            array (
                'permission_id' => 12,
                'role_id' => 39,
            ),
            71 => 
            array (
                'permission_id' => 12,
                'role_id' => 40,
            ),
            72 => 
            array (
                'permission_id' => 12,
                'role_id' => 41,
            ),
            73 => 
            array (
                'permission_id' => 13,
                'role_id' => 1,
            ),
            74 => 
            array (
                'permission_id' => 13,
                'role_id' => 39,
            ),
            75 => 
            array (
                'permission_id' => 13,
                'role_id' => 40,
            ),
            76 => 
            array (
                'permission_id' => 13,
                'role_id' => 41,
            ),
            77 => 
            array (
                'permission_id' => 14,
                'role_id' => 1,
            ),
            78 => 
            array (
                'permission_id' => 14,
                'role_id' => 39,
            ),
            79 => 
            array (
                'permission_id' => 14,
                'role_id' => 40,
            ),
            80 => 
            array (
                'permission_id' => 15,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 15,
                'role_id' => 39,
            ),
            82 => 
            array (
                'permission_id' => 15,
                'role_id' => 40,
            ),
            83 => 
            array (
                'permission_id' => 16,
                'role_id' => 1,
            ),
            84 => 
            array (
                'permission_id' => 16,
                'role_id' => 27,
            ),
            85 => 
            array (
                'permission_id' => 16,
                'role_id' => 39,
            ),
            86 => 
            array (
                'permission_id' => 16,
                'role_id' => 40,
            ),
            87 => 
            array (
                'permission_id' => 16,
                'role_id' => 41,
            ),
            88 => 
            array (
                'permission_id' => 17,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 17,
                'role_id' => 39,
            ),
            90 => 
            array (
                'permission_id' => 17,
                'role_id' => 40,
            ),
            91 => 
            array (
                'permission_id' => 17,
                'role_id' => 41,
            ),
            92 => 
            array (
                'permission_id' => 18,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 18,
                'role_id' => 39,
            ),
            94 => 
            array (
                'permission_id' => 18,
                'role_id' => 40,
            ),
            95 => 
            array (
                'permission_id' => 18,
                'role_id' => 41,
            ),
            96 => 
            array (
                'permission_id' => 19,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 19,
                'role_id' => 39,
            ),
            98 => 
            array (
                'permission_id' => 19,
                'role_id' => 40,
            ),
            99 => 
            array (
                'permission_id' => 19,
                'role_id' => 41,
            ),
            100 => 
            array (
                'permission_id' => 20,
                'role_id' => 1,
            ),
            101 => 
            array (
                'permission_id' => 20,
                'role_id' => 39,
            ),
            102 => 
            array (
                'permission_id' => 20,
                'role_id' => 40,
            ),
            103 => 
            array (
                'permission_id' => 20,
                'role_id' => 41,
            ),
            104 => 
            array (
                'permission_id' => 21,
                'role_id' => 1,
            ),
            105 => 
            array (
                'permission_id' => 21,
                'role_id' => 39,
            ),
            106 => 
            array (
                'permission_id' => 21,
                'role_id' => 40,
            ),
            107 => 
            array (
                'permission_id' => 22,
                'role_id' => 1,
            ),
            108 => 
            array (
                'permission_id' => 22,
                'role_id' => 39,
            ),
            109 => 
            array (
                'permission_id' => 22,
                'role_id' => 40,
            ),
            110 => 
            array (
                'permission_id' => 23,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 23,
                'role_id' => 39,
            ),
            112 => 
            array (
                'permission_id' => 23,
                'role_id' => 40,
            ),
            113 => 
            array (
                'permission_id' => 24,
                'role_id' => 1,
            ),
            114 => 
            array (
                'permission_id' => 24,
                'role_id' => 39,
            ),
            115 => 
            array (
                'permission_id' => 24,
                'role_id' => 40,
            ),
            116 => 
            array (
                'permission_id' => 25,
                'role_id' => 1,
            ),
            117 => 
            array (
                'permission_id' => 25,
                'role_id' => 39,
            ),
            118 => 
            array (
                'permission_id' => 25,
                'role_id' => 40,
            ),
            119 => 
            array (
                'permission_id' => 26,
                'role_id' => 1,
            ),
            120 => 
            array (
                'permission_id' => 26,
                'role_id' => 39,
            ),
            121 => 
            array (
                'permission_id' => 26,
                'role_id' => 40,
            ),
            122 => 
            array (
                'permission_id' => 26,
                'role_id' => 41,
            ),
            123 => 
            array (
                'permission_id' => 37,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 37,
                'role_id' => 27,
            ),
            125 => 
            array (
                'permission_id' => 37,
                'role_id' => 39,
            ),
            126 => 
            array (
                'permission_id' => 37,
                'role_id' => 40,
            ),
            127 => 
            array (
                'permission_id' => 37,
                'role_id' => 41,
            ),
            128 => 
            array (
                'permission_id' => 38,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 38,
                'role_id' => 27,
            ),
            130 => 
            array (
                'permission_id' => 38,
                'role_id' => 39,
            ),
            131 => 
            array (
                'permission_id' => 38,
                'role_id' => 40,
            ),
            132 => 
            array (
                'permission_id' => 38,
                'role_id' => 41,
            ),
            133 => 
            array (
                'permission_id' => 39,
                'role_id' => 1,
            ),
            134 => 
            array (
                'permission_id' => 39,
                'role_id' => 39,
            ),
            135 => 
            array (
                'permission_id' => 39,
                'role_id' => 40,
            ),
            136 => 
            array (
                'permission_id' => 39,
                'role_id' => 41,
            ),
            137 => 
            array (
                'permission_id' => 40,
                'role_id' => 1,
            ),
            138 => 
            array (
                'permission_id' => 40,
                'role_id' => 39,
            ),
            139 => 
            array (
                'permission_id' => 40,
                'role_id' => 40,
            ),
            140 => 
            array (
                'permission_id' => 40,
                'role_id' => 41,
            ),
            141 => 
            array (
                'permission_id' => 41,
                'role_id' => 1,
            ),
            142 => 
            array (
                'permission_id' => 41,
                'role_id' => 39,
            ),
            143 => 
            array (
                'permission_id' => 41,
                'role_id' => 40,
            ),
            144 => 
            array (
                'permission_id' => 41,
                'role_id' => 41,
            ),
            145 => 
            array (
                'permission_id' => 42,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 42,
                'role_id' => 21,
            ),
            147 => 
            array (
                'permission_id' => 42,
                'role_id' => 27,
            ),
            148 => 
            array (
                'permission_id' => 42,
                'role_id' => 32,
            ),
            149 => 
            array (
                'permission_id' => 42,
                'role_id' => 39,
            ),
            150 => 
            array (
                'permission_id' => 42,
                'role_id' => 40,
            ),
            151 => 
            array (
                'permission_id' => 42,
                'role_id' => 41,
            ),
            152 => 
            array (
                'permission_id' => 42,
                'role_id' => 48,
            ),
            153 => 
            array (
                'permission_id' => 43,
                'role_id' => 1,
            ),
            154 => 
            array (
                'permission_id' => 43,
                'role_id' => 21,
            ),
            155 => 
            array (
                'permission_id' => 43,
                'role_id' => 27,
            ),
            156 => 
            array (
                'permission_id' => 43,
                'role_id' => 32,
            ),
            157 => 
            array (
                'permission_id' => 43,
                'role_id' => 39,
            ),
            158 => 
            array (
                'permission_id' => 43,
                'role_id' => 40,
            ),
            159 => 
            array (
                'permission_id' => 43,
                'role_id' => 41,
            ),
            160 => 
            array (
                'permission_id' => 44,
                'role_id' => 1,
            ),
            161 => 
            array (
                'permission_id' => 44,
                'role_id' => 32,
            ),
            162 => 
            array (
                'permission_id' => 44,
                'role_id' => 39,
            ),
            163 => 
            array (
                'permission_id' => 44,
                'role_id' => 40,
            ),
            164 => 
            array (
                'permission_id' => 44,
                'role_id' => 41,
            ),
            165 => 
            array (
                'permission_id' => 45,
                'role_id' => 1,
            ),
            166 => 
            array (
                'permission_id' => 45,
                'role_id' => 21,
            ),
            167 => 
            array (
                'permission_id' => 45,
                'role_id' => 32,
            ),
            168 => 
            array (
                'permission_id' => 45,
                'role_id' => 39,
            ),
            169 => 
            array (
                'permission_id' => 45,
                'role_id' => 40,
            ),
            170 => 
            array (
                'permission_id' => 45,
                'role_id' => 41,
            ),
            171 => 
            array (
                'permission_id' => 46,
                'role_id' => 1,
            ),
            172 => 
            array (
                'permission_id' => 46,
                'role_id' => 32,
            ),
            173 => 
            array (
                'permission_id' => 46,
                'role_id' => 39,
            ),
            174 => 
            array (
                'permission_id' => 46,
                'role_id' => 40,
            ),
            175 => 
            array (
                'permission_id' => 46,
                'role_id' => 41,
            ),
            176 => 
            array (
                'permission_id' => 47,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 47,
                'role_id' => 27,
            ),
            178 => 
            array (
                'permission_id' => 47,
                'role_id' => 39,
            ),
            179 => 
            array (
                'permission_id' => 47,
                'role_id' => 40,
            ),
            180 => 
            array (
                'permission_id' => 47,
                'role_id' => 41,
            ),
            181 => 
            array (
                'permission_id' => 47,
                'role_id' => 48,
            ),
            182 => 
            array (
                'permission_id' => 48,
                'role_id' => 1,
            ),
            183 => 
            array (
                'permission_id' => 48,
                'role_id' => 27,
            ),
            184 => 
            array (
                'permission_id' => 48,
                'role_id' => 39,
            ),
            185 => 
            array (
                'permission_id' => 48,
                'role_id' => 40,
            ),
            186 => 
            array (
                'permission_id' => 48,
                'role_id' => 41,
            ),
            187 => 
            array (
                'permission_id' => 49,
                'role_id' => 1,
            ),
            188 => 
            array (
                'permission_id' => 49,
                'role_id' => 39,
            ),
            189 => 
            array (
                'permission_id' => 49,
                'role_id' => 40,
            ),
            190 => 
            array (
                'permission_id' => 49,
                'role_id' => 41,
            ),
            191 => 
            array (
                'permission_id' => 50,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 50,
                'role_id' => 39,
            ),
            193 => 
            array (
                'permission_id' => 50,
                'role_id' => 40,
            ),
            194 => 
            array (
                'permission_id' => 50,
                'role_id' => 41,
            ),
            195 => 
            array (
                'permission_id' => 51,
                'role_id' => 1,
            ),
            196 => 
            array (
                'permission_id' => 51,
                'role_id' => 39,
            ),
            197 => 
            array (
                'permission_id' => 51,
                'role_id' => 40,
            ),
            198 => 
            array (
                'permission_id' => 51,
                'role_id' => 41,
            ),
            199 => 
            array (
                'permission_id' => 52,
                'role_id' => 1,
            ),
            200 => 
            array (
                'permission_id' => 52,
                'role_id' => 16,
            ),
            201 => 
            array (
                'permission_id' => 52,
                'role_id' => 27,
            ),
            202 => 
            array (
                'permission_id' => 52,
                'role_id' => 35,
            ),
            203 => 
            array (
                'permission_id' => 52,
                'role_id' => 39,
            ),
            204 => 
            array (
                'permission_id' => 52,
                'role_id' => 40,
            ),
            205 => 
            array (
                'permission_id' => 52,
                'role_id' => 41,
            ),
            206 => 
            array (
                'permission_id' => 53,
                'role_id' => 1,
            ),
            207 => 
            array (
                'permission_id' => 53,
                'role_id' => 16,
            ),
            208 => 
            array (
                'permission_id' => 53,
                'role_id' => 27,
            ),
            209 => 
            array (
                'permission_id' => 53,
                'role_id' => 35,
            ),
            210 => 
            array (
                'permission_id' => 53,
                'role_id' => 39,
            ),
            211 => 
            array (
                'permission_id' => 53,
                'role_id' => 40,
            ),
            212 => 
            array (
                'permission_id' => 53,
                'role_id' => 41,
            ),
            213 => 
            array (
                'permission_id' => 54,
                'role_id' => 1,
            ),
            214 => 
            array (
                'permission_id' => 54,
                'role_id' => 35,
            ),
            215 => 
            array (
                'permission_id' => 54,
                'role_id' => 39,
            ),
            216 => 
            array (
                'permission_id' => 54,
                'role_id' => 40,
            ),
            217 => 
            array (
                'permission_id' => 54,
                'role_id' => 41,
            ),
            218 => 
            array (
                'permission_id' => 55,
                'role_id' => 1,
            ),
            219 => 
            array (
                'permission_id' => 55,
                'role_id' => 16,
            ),
            220 => 
            array (
                'permission_id' => 55,
                'role_id' => 35,
            ),
            221 => 
            array (
                'permission_id' => 55,
                'role_id' => 39,
            ),
            222 => 
            array (
                'permission_id' => 55,
                'role_id' => 40,
            ),
            223 => 
            array (
                'permission_id' => 55,
                'role_id' => 41,
            ),
            224 => 
            array (
                'permission_id' => 56,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 56,
                'role_id' => 35,
            ),
            226 => 
            array (
                'permission_id' => 56,
                'role_id' => 39,
            ),
            227 => 
            array (
                'permission_id' => 56,
                'role_id' => 40,
            ),
            228 => 
            array (
                'permission_id' => 56,
                'role_id' => 41,
            ),
            229 => 
            array (
                'permission_id' => 72,
                'role_id' => 1,
            ),
            230 => 
            array (
                'permission_id' => 72,
                'role_id' => 13,
            ),
            231 => 
            array (
                'permission_id' => 72,
                'role_id' => 27,
            ),
            232 => 
            array (
                'permission_id' => 72,
                'role_id' => 28,
            ),
            233 => 
            array (
                'permission_id' => 72,
                'role_id' => 39,
            ),
            234 => 
            array (
                'permission_id' => 72,
                'role_id' => 40,
            ),
            235 => 
            array (
                'permission_id' => 72,
                'role_id' => 41,
            ),
            236 => 
            array (
                'permission_id' => 73,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 73,
                'role_id' => 13,
            ),
            238 => 
            array (
                'permission_id' => 73,
                'role_id' => 27,
            ),
            239 => 
            array (
                'permission_id' => 73,
                'role_id' => 28,
            ),
            240 => 
            array (
                'permission_id' => 73,
                'role_id' => 39,
            ),
            241 => 
            array (
                'permission_id' => 73,
                'role_id' => 40,
            ),
            242 => 
            array (
                'permission_id' => 73,
                'role_id' => 41,
            ),
            243 => 
            array (
                'permission_id' => 74,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 74,
                'role_id' => 28,
            ),
            245 => 
            array (
                'permission_id' => 74,
                'role_id' => 39,
            ),
            246 => 
            array (
                'permission_id' => 74,
                'role_id' => 40,
            ),
            247 => 
            array (
                'permission_id' => 74,
                'role_id' => 41,
            ),
            248 => 
            array (
                'permission_id' => 75,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 75,
                'role_id' => 13,
            ),
            250 => 
            array (
                'permission_id' => 75,
                'role_id' => 28,
            ),
            251 => 
            array (
                'permission_id' => 75,
                'role_id' => 39,
            ),
            252 => 
            array (
                'permission_id' => 75,
                'role_id' => 40,
            ),
            253 => 
            array (
                'permission_id' => 75,
                'role_id' => 41,
            ),
            254 => 
            array (
                'permission_id' => 76,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 76,
                'role_id' => 28,
            ),
            256 => 
            array (
                'permission_id' => 76,
                'role_id' => 39,
            ),
            257 => 
            array (
                'permission_id' => 76,
                'role_id' => 40,
            ),
            258 => 
            array (
                'permission_id' => 76,
                'role_id' => 41,
            ),
            259 => 
            array (
                'permission_id' => 82,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 82,
                'role_id' => 27,
            ),
            261 => 
            array (
                'permission_id' => 82,
                'role_id' => 39,
            ),
            262 => 
            array (
                'permission_id' => 82,
                'role_id' => 40,
            ),
            263 => 
            array (
                'permission_id' => 82,
                'role_id' => 41,
            ),
            264 => 
            array (
                'permission_id' => 83,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 83,
                'role_id' => 27,
            ),
            266 => 
            array (
                'permission_id' => 83,
                'role_id' => 39,
            ),
            267 => 
            array (
                'permission_id' => 83,
                'role_id' => 40,
            ),
            268 => 
            array (
                'permission_id' => 83,
                'role_id' => 41,
            ),
            269 => 
            array (
                'permission_id' => 84,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 84,
                'role_id' => 39,
            ),
            271 => 
            array (
                'permission_id' => 84,
                'role_id' => 40,
            ),
            272 => 
            array (
                'permission_id' => 84,
                'role_id' => 41,
            ),
            273 => 
            array (
                'permission_id' => 85,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 85,
                'role_id' => 39,
            ),
            275 => 
            array (
                'permission_id' => 85,
                'role_id' => 40,
            ),
            276 => 
            array (
                'permission_id' => 85,
                'role_id' => 41,
            ),
            277 => 
            array (
                'permission_id' => 86,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 86,
                'role_id' => 39,
            ),
            279 => 
            array (
                'permission_id' => 86,
                'role_id' => 40,
            ),
            280 => 
            array (
                'permission_id' => 86,
                'role_id' => 41,
            ),
            281 => 
            array (
                'permission_id' => 87,
                'role_id' => 1,
            ),
            282 => 
            array (
                'permission_id' => 87,
                'role_id' => 20,
            ),
            283 => 
            array (
                'permission_id' => 87,
                'role_id' => 27,
            ),
            284 => 
            array (
                'permission_id' => 87,
                'role_id' => 33,
            ),
            285 => 
            array (
                'permission_id' => 87,
                'role_id' => 39,
            ),
            286 => 
            array (
                'permission_id' => 87,
                'role_id' => 40,
            ),
            287 => 
            array (
                'permission_id' => 87,
                'role_id' => 41,
            ),
            288 => 
            array (
                'permission_id' => 88,
                'role_id' => 1,
            ),
            289 => 
            array (
                'permission_id' => 88,
                'role_id' => 20,
            ),
            290 => 
            array (
                'permission_id' => 88,
                'role_id' => 27,
            ),
            291 => 
            array (
                'permission_id' => 88,
                'role_id' => 33,
            ),
            292 => 
            array (
                'permission_id' => 88,
                'role_id' => 39,
            ),
            293 => 
            array (
                'permission_id' => 88,
                'role_id' => 40,
            ),
            294 => 
            array (
                'permission_id' => 88,
                'role_id' => 41,
            ),
            295 => 
            array (
                'permission_id' => 89,
                'role_id' => 1,
            ),
            296 => 
            array (
                'permission_id' => 89,
                'role_id' => 33,
            ),
            297 => 
            array (
                'permission_id' => 89,
                'role_id' => 39,
            ),
            298 => 
            array (
                'permission_id' => 89,
                'role_id' => 40,
            ),
            299 => 
            array (
                'permission_id' => 89,
                'role_id' => 41,
            ),
            300 => 
            array (
                'permission_id' => 90,
                'role_id' => 1,
            ),
            301 => 
            array (
                'permission_id' => 90,
                'role_id' => 20,
            ),
            302 => 
            array (
                'permission_id' => 90,
                'role_id' => 33,
            ),
            303 => 
            array (
                'permission_id' => 90,
                'role_id' => 39,
            ),
            304 => 
            array (
                'permission_id' => 90,
                'role_id' => 40,
            ),
            305 => 
            array (
                'permission_id' => 90,
                'role_id' => 41,
            ),
            306 => 
            array (
                'permission_id' => 91,
                'role_id' => 1,
            ),
            307 => 
            array (
                'permission_id' => 91,
                'role_id' => 33,
            ),
            308 => 
            array (
                'permission_id' => 91,
                'role_id' => 39,
            ),
            309 => 
            array (
                'permission_id' => 91,
                'role_id' => 40,
            ),
            310 => 
            array (
                'permission_id' => 91,
                'role_id' => 41,
            ),
            311 => 
            array (
                'permission_id' => 92,
                'role_id' => 1,
            ),
            312 => 
            array (
                'permission_id' => 92,
                'role_id' => 25,
            ),
            313 => 
            array (
                'permission_id' => 92,
                'role_id' => 27,
            ),
            314 => 
            array (
                'permission_id' => 92,
                'role_id' => 29,
            ),
            315 => 
            array (
                'permission_id' => 92,
                'role_id' => 39,
            ),
            316 => 
            array (
                'permission_id' => 92,
                'role_id' => 40,
            ),
            317 => 
            array (
                'permission_id' => 92,
                'role_id' => 41,
            ),
            318 => 
            array (
                'permission_id' => 93,
                'role_id' => 1,
            ),
            319 => 
            array (
                'permission_id' => 93,
                'role_id' => 25,
            ),
            320 => 
            array (
                'permission_id' => 93,
                'role_id' => 27,
            ),
            321 => 
            array (
                'permission_id' => 93,
                'role_id' => 29,
            ),
            322 => 
            array (
                'permission_id' => 93,
                'role_id' => 39,
            ),
            323 => 
            array (
                'permission_id' => 93,
                'role_id' => 40,
            ),
            324 => 
            array (
                'permission_id' => 93,
                'role_id' => 41,
            ),
            325 => 
            array (
                'permission_id' => 94,
                'role_id' => 1,
            ),
            326 => 
            array (
                'permission_id' => 94,
                'role_id' => 29,
            ),
            327 => 
            array (
                'permission_id' => 94,
                'role_id' => 39,
            ),
            328 => 
            array (
                'permission_id' => 94,
                'role_id' => 40,
            ),
            329 => 
            array (
                'permission_id' => 94,
                'role_id' => 41,
            ),
            330 => 
            array (
                'permission_id' => 95,
                'role_id' => 1,
            ),
            331 => 
            array (
                'permission_id' => 95,
                'role_id' => 25,
            ),
            332 => 
            array (
                'permission_id' => 95,
                'role_id' => 29,
            ),
            333 => 
            array (
                'permission_id' => 95,
                'role_id' => 39,
            ),
            334 => 
            array (
                'permission_id' => 95,
                'role_id' => 40,
            ),
            335 => 
            array (
                'permission_id' => 95,
                'role_id' => 41,
            ),
            336 => 
            array (
                'permission_id' => 96,
                'role_id' => 1,
            ),
            337 => 
            array (
                'permission_id' => 96,
                'role_id' => 29,
            ),
            338 => 
            array (
                'permission_id' => 96,
                'role_id' => 39,
            ),
            339 => 
            array (
                'permission_id' => 96,
                'role_id' => 40,
            ),
            340 => 
            array (
                'permission_id' => 96,
                'role_id' => 41,
            ),
            341 => 
            array (
                'permission_id' => 97,
                'role_id' => 1,
            ),
            342 => 
            array (
                'permission_id' => 97,
                'role_id' => 27,
            ),
            343 => 
            array (
                'permission_id' => 97,
                'role_id' => 39,
            ),
            344 => 
            array (
                'permission_id' => 97,
                'role_id' => 40,
            ),
            345 => 
            array (
                'permission_id' => 97,
                'role_id' => 41,
            ),
            346 => 
            array (
                'permission_id' => 97,
                'role_id' => 46,
            ),
            347 => 
            array (
                'permission_id' => 97,
                'role_id' => 47,
            ),
            348 => 
            array (
                'permission_id' => 98,
                'role_id' => 1,
            ),
            349 => 
            array (
                'permission_id' => 98,
                'role_id' => 27,
            ),
            350 => 
            array (
                'permission_id' => 98,
                'role_id' => 39,
            ),
            351 => 
            array (
                'permission_id' => 98,
                'role_id' => 40,
            ),
            352 => 
            array (
                'permission_id' => 98,
                'role_id' => 41,
            ),
            353 => 
            array (
                'permission_id' => 98,
                'role_id' => 46,
            ),
            354 => 
            array (
                'permission_id' => 98,
                'role_id' => 47,
            ),
            355 => 
            array (
                'permission_id' => 99,
                'role_id' => 1,
            ),
            356 => 
            array (
                'permission_id' => 99,
                'role_id' => 39,
            ),
            357 => 
            array (
                'permission_id' => 99,
                'role_id' => 40,
            ),
            358 => 
            array (
                'permission_id' => 99,
                'role_id' => 41,
            ),
            359 => 
            array (
                'permission_id' => 99,
                'role_id' => 47,
            ),
            360 => 
            array (
                'permission_id' => 100,
                'role_id' => 1,
            ),
            361 => 
            array (
                'permission_id' => 100,
                'role_id' => 39,
            ),
            362 => 
            array (
                'permission_id' => 100,
                'role_id' => 40,
            ),
            363 => 
            array (
                'permission_id' => 100,
                'role_id' => 41,
            ),
            364 => 
            array (
                'permission_id' => 100,
                'role_id' => 46,
            ),
            365 => 
            array (
                'permission_id' => 100,
                'role_id' => 47,
            ),
            366 => 
            array (
                'permission_id' => 101,
                'role_id' => 1,
            ),
            367 => 
            array (
                'permission_id' => 101,
                'role_id' => 39,
            ),
            368 => 
            array (
                'permission_id' => 101,
                'role_id' => 40,
            ),
            369 => 
            array (
                'permission_id' => 101,
                'role_id' => 41,
            ),
            370 => 
            array (
                'permission_id' => 101,
                'role_id' => 47,
            ),
            371 => 
            array (
                'permission_id' => 102,
                'role_id' => 1,
            ),
            372 => 
            array (
                'permission_id' => 102,
                'role_id' => 27,
            ),
            373 => 
            array (
                'permission_id' => 102,
                'role_id' => 39,
            ),
            374 => 
            array (
                'permission_id' => 102,
                'role_id' => 40,
            ),
            375 => 
            array (
                'permission_id' => 102,
                'role_id' => 41,
            ),
            376 => 
            array (
                'permission_id' => 102,
                'role_id' => 44,
            ),
            377 => 
            array (
                'permission_id' => 102,
                'role_id' => 45,
            ),
            378 => 
            array (
                'permission_id' => 103,
                'role_id' => 1,
            ),
            379 => 
            array (
                'permission_id' => 103,
                'role_id' => 27,
            ),
            380 => 
            array (
                'permission_id' => 103,
                'role_id' => 39,
            ),
            381 => 
            array (
                'permission_id' => 103,
                'role_id' => 40,
            ),
            382 => 
            array (
                'permission_id' => 103,
                'role_id' => 41,
            ),
            383 => 
            array (
                'permission_id' => 103,
                'role_id' => 44,
            ),
            384 => 
            array (
                'permission_id' => 103,
                'role_id' => 45,
            ),
            385 => 
            array (
                'permission_id' => 104,
                'role_id' => 1,
            ),
            386 => 
            array (
                'permission_id' => 104,
                'role_id' => 39,
            ),
            387 => 
            array (
                'permission_id' => 104,
                'role_id' => 40,
            ),
            388 => 
            array (
                'permission_id' => 104,
                'role_id' => 41,
            ),
            389 => 
            array (
                'permission_id' => 104,
                'role_id' => 45,
            ),
            390 => 
            array (
                'permission_id' => 105,
                'role_id' => 1,
            ),
            391 => 
            array (
                'permission_id' => 105,
                'role_id' => 39,
            ),
            392 => 
            array (
                'permission_id' => 105,
                'role_id' => 40,
            ),
            393 => 
            array (
                'permission_id' => 105,
                'role_id' => 41,
            ),
            394 => 
            array (
                'permission_id' => 105,
                'role_id' => 44,
            ),
            395 => 
            array (
                'permission_id' => 105,
                'role_id' => 45,
            ),
            396 => 
            array (
                'permission_id' => 106,
                'role_id' => 1,
            ),
            397 => 
            array (
                'permission_id' => 106,
                'role_id' => 39,
            ),
            398 => 
            array (
                'permission_id' => 106,
                'role_id' => 40,
            ),
            399 => 
            array (
                'permission_id' => 106,
                'role_id' => 41,
            ),
            400 => 
            array (
                'permission_id' => 106,
                'role_id' => 45,
            ),
            401 => 
            array (
                'permission_id' => 107,
                'role_id' => 1,
            ),
            402 => 
            array (
                'permission_id' => 107,
                'role_id' => 15,
            ),
            403 => 
            array (
                'permission_id' => 107,
                'role_id' => 27,
            ),
            404 => 
            array (
                'permission_id' => 107,
                'role_id' => 34,
            ),
            405 => 
            array (
                'permission_id' => 107,
                'role_id' => 39,
            ),
            406 => 
            array (
                'permission_id' => 107,
                'role_id' => 40,
            ),
            407 => 
            array (
                'permission_id' => 107,
                'role_id' => 41,
            ),
            408 => 
            array (
                'permission_id' => 108,
                'role_id' => 1,
            ),
            409 => 
            array (
                'permission_id' => 108,
                'role_id' => 15,
            ),
            410 => 
            array (
                'permission_id' => 108,
                'role_id' => 27,
            ),
            411 => 
            array (
                'permission_id' => 108,
                'role_id' => 34,
            ),
            412 => 
            array (
                'permission_id' => 108,
                'role_id' => 39,
            ),
            413 => 
            array (
                'permission_id' => 108,
                'role_id' => 40,
            ),
            414 => 
            array (
                'permission_id' => 108,
                'role_id' => 41,
            ),
            415 => 
            array (
                'permission_id' => 109,
                'role_id' => 1,
            ),
            416 => 
            array (
                'permission_id' => 109,
                'role_id' => 34,
            ),
            417 => 
            array (
                'permission_id' => 109,
                'role_id' => 39,
            ),
            418 => 
            array (
                'permission_id' => 109,
                'role_id' => 40,
            ),
            419 => 
            array (
                'permission_id' => 109,
                'role_id' => 41,
            ),
            420 => 
            array (
                'permission_id' => 110,
                'role_id' => 1,
            ),
            421 => 
            array (
                'permission_id' => 110,
                'role_id' => 15,
            ),
            422 => 
            array (
                'permission_id' => 110,
                'role_id' => 34,
            ),
            423 => 
            array (
                'permission_id' => 110,
                'role_id' => 39,
            ),
            424 => 
            array (
                'permission_id' => 110,
                'role_id' => 40,
            ),
            425 => 
            array (
                'permission_id' => 110,
                'role_id' => 41,
            ),
            426 => 
            array (
                'permission_id' => 111,
                'role_id' => 1,
            ),
            427 => 
            array (
                'permission_id' => 111,
                'role_id' => 34,
            ),
            428 => 
            array (
                'permission_id' => 111,
                'role_id' => 39,
            ),
            429 => 
            array (
                'permission_id' => 111,
                'role_id' => 40,
            ),
            430 => 
            array (
                'permission_id' => 111,
                'role_id' => 41,
            ),
            431 => 
            array (
                'permission_id' => 112,
                'role_id' => 1,
            ),
            432 => 
            array (
                'permission_id' => 112,
                'role_id' => 27,
            ),
            433 => 
            array (
                'permission_id' => 112,
                'role_id' => 33,
            ),
            434 => 
            array (
                'permission_id' => 112,
                'role_id' => 39,
            ),
            435 => 
            array (
                'permission_id' => 112,
                'role_id' => 40,
            ),
            436 => 
            array (
                'permission_id' => 112,
                'role_id' => 41,
            ),
            437 => 
            array (
                'permission_id' => 112,
                'role_id' => 42,
            ),
            438 => 
            array (
                'permission_id' => 112,
                'role_id' => 43,
            ),
            439 => 
            array (
                'permission_id' => 113,
                'role_id' => 1,
            ),
            440 => 
            array (
                'permission_id' => 113,
                'role_id' => 27,
            ),
            441 => 
            array (
                'permission_id' => 113,
                'role_id' => 33,
            ),
            442 => 
            array (
                'permission_id' => 113,
                'role_id' => 39,
            ),
            443 => 
            array (
                'permission_id' => 113,
                'role_id' => 40,
            ),
            444 => 
            array (
                'permission_id' => 113,
                'role_id' => 41,
            ),
            445 => 
            array (
                'permission_id' => 113,
                'role_id' => 42,
            ),
            446 => 
            array (
                'permission_id' => 113,
                'role_id' => 43,
            ),
            447 => 
            array (
                'permission_id' => 114,
                'role_id' => 1,
            ),
            448 => 
            array (
                'permission_id' => 114,
                'role_id' => 33,
            ),
            449 => 
            array (
                'permission_id' => 114,
                'role_id' => 39,
            ),
            450 => 
            array (
                'permission_id' => 114,
                'role_id' => 40,
            ),
            451 => 
            array (
                'permission_id' => 114,
                'role_id' => 41,
            ),
            452 => 
            array (
                'permission_id' => 114,
                'role_id' => 43,
            ),
            453 => 
            array (
                'permission_id' => 115,
                'role_id' => 1,
            ),
            454 => 
            array (
                'permission_id' => 115,
                'role_id' => 33,
            ),
            455 => 
            array (
                'permission_id' => 115,
                'role_id' => 39,
            ),
            456 => 
            array (
                'permission_id' => 115,
                'role_id' => 40,
            ),
            457 => 
            array (
                'permission_id' => 115,
                'role_id' => 41,
            ),
            458 => 
            array (
                'permission_id' => 115,
                'role_id' => 42,
            ),
            459 => 
            array (
                'permission_id' => 115,
                'role_id' => 43,
            ),
            460 => 
            array (
                'permission_id' => 116,
                'role_id' => 1,
            ),
            461 => 
            array (
                'permission_id' => 116,
                'role_id' => 33,
            ),
            462 => 
            array (
                'permission_id' => 116,
                'role_id' => 39,
            ),
            463 => 
            array (
                'permission_id' => 116,
                'role_id' => 40,
            ),
            464 => 
            array (
                'permission_id' => 116,
                'role_id' => 41,
            ),
            465 => 
            array (
                'permission_id' => 116,
                'role_id' => 43,
            ),
            466 => 
            array (
                'permission_id' => 122,
                'role_id' => 1,
            ),
            467 => 
            array (
                'permission_id' => 122,
                'role_id' => 24,
            ),
            468 => 
            array (
                'permission_id' => 122,
                'role_id' => 27,
            ),
            469 => 
            array (
                'permission_id' => 122,
                'role_id' => 31,
            ),
            470 => 
            array (
                'permission_id' => 122,
                'role_id' => 39,
            ),
            471 => 
            array (
                'permission_id' => 122,
                'role_id' => 40,
            ),
            472 => 
            array (
                'permission_id' => 122,
                'role_id' => 41,
            ),
            473 => 
            array (
                'permission_id' => 123,
                'role_id' => 1,
            ),
            474 => 
            array (
                'permission_id' => 123,
                'role_id' => 24,
            ),
            475 => 
            array (
                'permission_id' => 123,
                'role_id' => 27,
            ),
            476 => 
            array (
                'permission_id' => 123,
                'role_id' => 31,
            ),
            477 => 
            array (
                'permission_id' => 123,
                'role_id' => 39,
            ),
            478 => 
            array (
                'permission_id' => 123,
                'role_id' => 40,
            ),
            479 => 
            array (
                'permission_id' => 123,
                'role_id' => 41,
            ),
            480 => 
            array (
                'permission_id' => 124,
                'role_id' => 1,
            ),
            481 => 
            array (
                'permission_id' => 124,
                'role_id' => 31,
            ),
            482 => 
            array (
                'permission_id' => 124,
                'role_id' => 39,
            ),
            483 => 
            array (
                'permission_id' => 124,
                'role_id' => 40,
            ),
            484 => 
            array (
                'permission_id' => 124,
                'role_id' => 41,
            ),
            485 => 
            array (
                'permission_id' => 125,
                'role_id' => 1,
            ),
            486 => 
            array (
                'permission_id' => 125,
                'role_id' => 24,
            ),
            487 => 
            array (
                'permission_id' => 125,
                'role_id' => 31,
            ),
            488 => 
            array (
                'permission_id' => 125,
                'role_id' => 39,
            ),
            489 => 
            array (
                'permission_id' => 125,
                'role_id' => 40,
            ),
            490 => 
            array (
                'permission_id' => 125,
                'role_id' => 41,
            ),
            491 => 
            array (
                'permission_id' => 126,
                'role_id' => 1,
            ),
            492 => 
            array (
                'permission_id' => 126,
                'role_id' => 31,
            ),
            493 => 
            array (
                'permission_id' => 126,
                'role_id' => 39,
            ),
            494 => 
            array (
                'permission_id' => 126,
                'role_id' => 40,
            ),
            495 => 
            array (
                'permission_id' => 126,
                'role_id' => 41,
            ),
            496 => 
            array (
                'permission_id' => 127,
                'role_id' => 1,
            ),
            497 => 
            array (
                'permission_id' => 127,
                'role_id' => 39,
            ),
            498 => 
            array (
                'permission_id' => 127,
                'role_id' => 40,
            ),
            499 => 
            array (
                'permission_id' => 127,
                'role_id' => 41,
            ),
        ));
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'permission_id' => 128,
                'role_id' => 1,
            ),
            1 => 
            array (
                'permission_id' => 128,
                'role_id' => 39,
            ),
            2 => 
            array (
                'permission_id' => 128,
                'role_id' => 40,
            ),
            3 => 
            array (
                'permission_id' => 128,
                'role_id' => 41,
            ),
            4 => 
            array (
                'permission_id' => 129,
                'role_id' => 1,
            ),
            5 => 
            array (
                'permission_id' => 129,
                'role_id' => 39,
            ),
            6 => 
            array (
                'permission_id' => 129,
                'role_id' => 40,
            ),
            7 => 
            array (
                'permission_id' => 129,
                'role_id' => 41,
            ),
            8 => 
            array (
                'permission_id' => 130,
                'role_id' => 1,
            ),
            9 => 
            array (
                'permission_id' => 130,
                'role_id' => 39,
            ),
            10 => 
            array (
                'permission_id' => 130,
                'role_id' => 40,
            ),
            11 => 
            array (
                'permission_id' => 130,
                'role_id' => 41,
            ),
            12 => 
            array (
                'permission_id' => 131,
                'role_id' => 1,
            ),
            13 => 
            array (
                'permission_id' => 131,
                'role_id' => 39,
            ),
            14 => 
            array (
                'permission_id' => 131,
                'role_id' => 40,
            ),
            15 => 
            array (
                'permission_id' => 131,
                'role_id' => 41,
            ),
            16 => 
            array (
                'permission_id' => 132,
                'role_id' => 1,
            ),
            17 => 
            array (
                'permission_id' => 132,
                'role_id' => 39,
            ),
            18 => 
            array (
                'permission_id' => 132,
                'role_id' => 40,
            ),
            19 => 
            array (
                'permission_id' => 132,
                'role_id' => 41,
            ),
            20 => 
            array (
                'permission_id' => 133,
                'role_id' => 1,
            ),
            21 => 
            array (
                'permission_id' => 133,
                'role_id' => 39,
            ),
            22 => 
            array (
                'permission_id' => 133,
                'role_id' => 40,
            ),
            23 => 
            array (
                'permission_id' => 133,
                'role_id' => 41,
            ),
            24 => 
            array (
                'permission_id' => 134,
                'role_id' => 1,
            ),
            25 => 
            array (
                'permission_id' => 134,
                'role_id' => 39,
            ),
            26 => 
            array (
                'permission_id' => 134,
                'role_id' => 40,
            ),
            27 => 
            array (
                'permission_id' => 134,
                'role_id' => 41,
            ),
            28 => 
            array (
                'permission_id' => 135,
                'role_id' => 1,
            ),
            29 => 
            array (
                'permission_id' => 135,
                'role_id' => 39,
            ),
            30 => 
            array (
                'permission_id' => 135,
                'role_id' => 40,
            ),
            31 => 
            array (
                'permission_id' => 135,
                'role_id' => 41,
            ),
            32 => 
            array (
                'permission_id' => 136,
                'role_id' => 1,
            ),
            33 => 
            array (
                'permission_id' => 136,
                'role_id' => 39,
            ),
            34 => 
            array (
                'permission_id' => 136,
                'role_id' => 40,
            ),
            35 => 
            array (
                'permission_id' => 136,
                'role_id' => 41,
            ),
            36 => 
            array (
                'permission_id' => 137,
                'role_id' => 1,
            ),
            37 => 
            array (
                'permission_id' => 137,
                'role_id' => 39,
            ),
            38 => 
            array (
                'permission_id' => 137,
                'role_id' => 40,
            ),
            39 => 
            array (
                'permission_id' => 137,
                'role_id' => 41,
            ),
            40 => 
            array (
                'permission_id' => 138,
                'role_id' => 1,
            ),
            41 => 
            array (
                'permission_id' => 138,
                'role_id' => 39,
            ),
            42 => 
            array (
                'permission_id' => 138,
                'role_id' => 40,
            ),
            43 => 
            array (
                'permission_id' => 138,
                'role_id' => 41,
            ),
            44 => 
            array (
                'permission_id' => 139,
                'role_id' => 1,
            ),
            45 => 
            array (
                'permission_id' => 139,
                'role_id' => 39,
            ),
            46 => 
            array (
                'permission_id' => 139,
                'role_id' => 40,
            ),
            47 => 
            array (
                'permission_id' => 139,
                'role_id' => 41,
            ),
            48 => 
            array (
                'permission_id' => 140,
                'role_id' => 1,
            ),
            49 => 
            array (
                'permission_id' => 140,
                'role_id' => 39,
            ),
            50 => 
            array (
                'permission_id' => 140,
                'role_id' => 40,
            ),
            51 => 
            array (
                'permission_id' => 140,
                'role_id' => 41,
            ),
            52 => 
            array (
                'permission_id' => 141,
                'role_id' => 1,
            ),
            53 => 
            array (
                'permission_id' => 141,
                'role_id' => 39,
            ),
            54 => 
            array (
                'permission_id' => 141,
                'role_id' => 40,
            ),
            55 => 
            array (
                'permission_id' => 141,
                'role_id' => 41,
            ),
            56 => 
            array (
                'permission_id' => 142,
                'role_id' => 1,
            ),
            57 => 
            array (
                'permission_id' => 142,
                'role_id' => 39,
            ),
            58 => 
            array (
                'permission_id' => 142,
                'role_id' => 40,
            ),
            59 => 
            array (
                'permission_id' => 142,
                'role_id' => 41,
            ),
            60 => 
            array (
                'permission_id' => 143,
                'role_id' => 1,
            ),
            61 => 
            array (
                'permission_id' => 143,
                'role_id' => 39,
            ),
            62 => 
            array (
                'permission_id' => 143,
                'role_id' => 40,
            ),
            63 => 
            array (
                'permission_id' => 143,
                'role_id' => 41,
            ),
            64 => 
            array (
                'permission_id' => 144,
                'role_id' => 1,
            ),
            65 => 
            array (
                'permission_id' => 144,
                'role_id' => 39,
            ),
            66 => 
            array (
                'permission_id' => 144,
                'role_id' => 40,
            ),
            67 => 
            array (
                'permission_id' => 144,
                'role_id' => 41,
            ),
            68 => 
            array (
                'permission_id' => 145,
                'role_id' => 1,
            ),
            69 => 
            array (
                'permission_id' => 145,
                'role_id' => 39,
            ),
            70 => 
            array (
                'permission_id' => 145,
                'role_id' => 40,
            ),
            71 => 
            array (
                'permission_id' => 145,
                'role_id' => 41,
            ),
            72 => 
            array (
                'permission_id' => 146,
                'role_id' => 1,
            ),
            73 => 
            array (
                'permission_id' => 146,
                'role_id' => 39,
            ),
            74 => 
            array (
                'permission_id' => 146,
                'role_id' => 40,
            ),
            75 => 
            array (
                'permission_id' => 146,
                'role_id' => 41,
            ),
            76 => 
            array (
                'permission_id' => 147,
                'role_id' => 1,
            ),
            77 => 
            array (
                'permission_id' => 147,
                'role_id' => 39,
            ),
            78 => 
            array (
                'permission_id' => 147,
                'role_id' => 40,
            ),
            79 => 
            array (
                'permission_id' => 147,
                'role_id' => 41,
            ),
            80 => 
            array (
                'permission_id' => 148,
                'role_id' => 1,
            ),
            81 => 
            array (
                'permission_id' => 148,
                'role_id' => 39,
            ),
            82 => 
            array (
                'permission_id' => 148,
                'role_id' => 40,
            ),
            83 => 
            array (
                'permission_id' => 148,
                'role_id' => 41,
            ),
            84 => 
            array (
                'permission_id' => 149,
                'role_id' => 1,
            ),
            85 => 
            array (
                'permission_id' => 149,
                'role_id' => 39,
            ),
            86 => 
            array (
                'permission_id' => 149,
                'role_id' => 40,
            ),
            87 => 
            array (
                'permission_id' => 149,
                'role_id' => 41,
            ),
            88 => 
            array (
                'permission_id' => 150,
                'role_id' => 1,
            ),
            89 => 
            array (
                'permission_id' => 150,
                'role_id' => 39,
            ),
            90 => 
            array (
                'permission_id' => 150,
                'role_id' => 40,
            ),
            91 => 
            array (
                'permission_id' => 150,
                'role_id' => 41,
            ),
            92 => 
            array (
                'permission_id' => 151,
                'role_id' => 1,
            ),
            93 => 
            array (
                'permission_id' => 151,
                'role_id' => 39,
            ),
            94 => 
            array (
                'permission_id' => 151,
                'role_id' => 40,
            ),
            95 => 
            array (
                'permission_id' => 151,
                'role_id' => 41,
            ),
            96 => 
            array (
                'permission_id' => 157,
                'role_id' => 1,
            ),
            97 => 
            array (
                'permission_id' => 157,
                'role_id' => 27,
            ),
            98 => 
            array (
                'permission_id' => 157,
                'role_id' => 39,
            ),
            99 => 
            array (
                'permission_id' => 157,
                'role_id' => 40,
            ),
            100 => 
            array (
                'permission_id' => 157,
                'role_id' => 41,
            ),
            101 => 
            array (
                'permission_id' => 158,
                'role_id' => 1,
            ),
            102 => 
            array (
                'permission_id' => 158,
                'role_id' => 27,
            ),
            103 => 
            array (
                'permission_id' => 158,
                'role_id' => 39,
            ),
            104 => 
            array (
                'permission_id' => 158,
                'role_id' => 40,
            ),
            105 => 
            array (
                'permission_id' => 158,
                'role_id' => 41,
            ),
            106 => 
            array (
                'permission_id' => 159,
                'role_id' => 1,
            ),
            107 => 
            array (
                'permission_id' => 159,
                'role_id' => 39,
            ),
            108 => 
            array (
                'permission_id' => 159,
                'role_id' => 40,
            ),
            109 => 
            array (
                'permission_id' => 159,
                'role_id' => 41,
            ),
            110 => 
            array (
                'permission_id' => 160,
                'role_id' => 1,
            ),
            111 => 
            array (
                'permission_id' => 160,
                'role_id' => 39,
            ),
            112 => 
            array (
                'permission_id' => 160,
                'role_id' => 40,
            ),
            113 => 
            array (
                'permission_id' => 160,
                'role_id' => 41,
            ),
            114 => 
            array (
                'permission_id' => 161,
                'role_id' => 1,
            ),
            115 => 
            array (
                'permission_id' => 161,
                'role_id' => 39,
            ),
            116 => 
            array (
                'permission_id' => 161,
                'role_id' => 40,
            ),
            117 => 
            array (
                'permission_id' => 161,
                'role_id' => 41,
            ),
            118 => 
            array (
                'permission_id' => 162,
                'role_id' => 1,
            ),
            119 => 
            array (
                'permission_id' => 162,
                'role_id' => 27,
            ),
            120 => 
            array (
                'permission_id' => 162,
                'role_id' => 39,
            ),
            121 => 
            array (
                'permission_id' => 162,
                'role_id' => 40,
            ),
            122 => 
            array (
                'permission_id' => 162,
                'role_id' => 41,
            ),
            123 => 
            array (
                'permission_id' => 163,
                'role_id' => 1,
            ),
            124 => 
            array (
                'permission_id' => 163,
                'role_id' => 27,
            ),
            125 => 
            array (
                'permission_id' => 163,
                'role_id' => 39,
            ),
            126 => 
            array (
                'permission_id' => 163,
                'role_id' => 40,
            ),
            127 => 
            array (
                'permission_id' => 163,
                'role_id' => 41,
            ),
            128 => 
            array (
                'permission_id' => 164,
                'role_id' => 1,
            ),
            129 => 
            array (
                'permission_id' => 164,
                'role_id' => 39,
            ),
            130 => 
            array (
                'permission_id' => 164,
                'role_id' => 40,
            ),
            131 => 
            array (
                'permission_id' => 164,
                'role_id' => 41,
            ),
            132 => 
            array (
                'permission_id' => 165,
                'role_id' => 1,
            ),
            133 => 
            array (
                'permission_id' => 165,
                'role_id' => 39,
            ),
            134 => 
            array (
                'permission_id' => 165,
                'role_id' => 40,
            ),
            135 => 
            array (
                'permission_id' => 165,
                'role_id' => 41,
            ),
            136 => 
            array (
                'permission_id' => 166,
                'role_id' => 1,
            ),
            137 => 
            array (
                'permission_id' => 166,
                'role_id' => 39,
            ),
            138 => 
            array (
                'permission_id' => 166,
                'role_id' => 40,
            ),
            139 => 
            array (
                'permission_id' => 166,
                'role_id' => 41,
            ),
            140 => 
            array (
                'permission_id' => 167,
                'role_id' => 1,
            ),
            141 => 
            array (
                'permission_id' => 167,
                'role_id' => 27,
            ),
            142 => 
            array (
                'permission_id' => 167,
                'role_id' => 39,
            ),
            143 => 
            array (
                'permission_id' => 167,
                'role_id' => 40,
            ),
            144 => 
            array (
                'permission_id' => 167,
                'role_id' => 41,
            ),
            145 => 
            array (
                'permission_id' => 168,
                'role_id' => 1,
            ),
            146 => 
            array (
                'permission_id' => 168,
                'role_id' => 27,
            ),
            147 => 
            array (
                'permission_id' => 168,
                'role_id' => 39,
            ),
            148 => 
            array (
                'permission_id' => 168,
                'role_id' => 40,
            ),
            149 => 
            array (
                'permission_id' => 168,
                'role_id' => 41,
            ),
            150 => 
            array (
                'permission_id' => 169,
                'role_id' => 1,
            ),
            151 => 
            array (
                'permission_id' => 169,
                'role_id' => 39,
            ),
            152 => 
            array (
                'permission_id' => 169,
                'role_id' => 40,
            ),
            153 => 
            array (
                'permission_id' => 169,
                'role_id' => 41,
            ),
            154 => 
            array (
                'permission_id' => 170,
                'role_id' => 1,
            ),
            155 => 
            array (
                'permission_id' => 170,
                'role_id' => 39,
            ),
            156 => 
            array (
                'permission_id' => 170,
                'role_id' => 40,
            ),
            157 => 
            array (
                'permission_id' => 170,
                'role_id' => 41,
            ),
            158 => 
            array (
                'permission_id' => 171,
                'role_id' => 1,
            ),
            159 => 
            array (
                'permission_id' => 171,
                'role_id' => 39,
            ),
            160 => 
            array (
                'permission_id' => 171,
                'role_id' => 40,
            ),
            161 => 
            array (
                'permission_id' => 171,
                'role_id' => 41,
            ),
            162 => 
            array (
                'permission_id' => 172,
                'role_id' => 1,
            ),
            163 => 
            array (
                'permission_id' => 172,
                'role_id' => 27,
            ),
            164 => 
            array (
                'permission_id' => 172,
                'role_id' => 39,
            ),
            165 => 
            array (
                'permission_id' => 172,
                'role_id' => 40,
            ),
            166 => 
            array (
                'permission_id' => 172,
                'role_id' => 41,
            ),
            167 => 
            array (
                'permission_id' => 173,
                'role_id' => 1,
            ),
            168 => 
            array (
                'permission_id' => 173,
                'role_id' => 27,
            ),
            169 => 
            array (
                'permission_id' => 173,
                'role_id' => 39,
            ),
            170 => 
            array (
                'permission_id' => 173,
                'role_id' => 40,
            ),
            171 => 
            array (
                'permission_id' => 173,
                'role_id' => 41,
            ),
            172 => 
            array (
                'permission_id' => 174,
                'role_id' => 1,
            ),
            173 => 
            array (
                'permission_id' => 174,
                'role_id' => 39,
            ),
            174 => 
            array (
                'permission_id' => 174,
                'role_id' => 40,
            ),
            175 => 
            array (
                'permission_id' => 174,
                'role_id' => 41,
            ),
            176 => 
            array (
                'permission_id' => 175,
                'role_id' => 1,
            ),
            177 => 
            array (
                'permission_id' => 175,
                'role_id' => 39,
            ),
            178 => 
            array (
                'permission_id' => 175,
                'role_id' => 40,
            ),
            179 => 
            array (
                'permission_id' => 175,
                'role_id' => 41,
            ),
            180 => 
            array (
                'permission_id' => 176,
                'role_id' => 1,
            ),
            181 => 
            array (
                'permission_id' => 176,
                'role_id' => 39,
            ),
            182 => 
            array (
                'permission_id' => 176,
                'role_id' => 40,
            ),
            183 => 
            array (
                'permission_id' => 176,
                'role_id' => 41,
            ),
            184 => 
            array (
                'permission_id' => 182,
                'role_id' => 1,
            ),
            185 => 
            array (
                'permission_id' => 182,
                'role_id' => 27,
            ),
            186 => 
            array (
                'permission_id' => 182,
                'role_id' => 37,
            ),
            187 => 
            array (
                'permission_id' => 182,
                'role_id' => 38,
            ),
            188 => 
            array (
                'permission_id' => 182,
                'role_id' => 39,
            ),
            189 => 
            array (
                'permission_id' => 182,
                'role_id' => 40,
            ),
            190 => 
            array (
                'permission_id' => 182,
                'role_id' => 41,
            ),
            191 => 
            array (
                'permission_id' => 183,
                'role_id' => 1,
            ),
            192 => 
            array (
                'permission_id' => 183,
                'role_id' => 27,
            ),
            193 => 
            array (
                'permission_id' => 183,
                'role_id' => 37,
            ),
            194 => 
            array (
                'permission_id' => 183,
                'role_id' => 38,
            ),
            195 => 
            array (
                'permission_id' => 183,
                'role_id' => 39,
            ),
            196 => 
            array (
                'permission_id' => 183,
                'role_id' => 40,
            ),
            197 => 
            array (
                'permission_id' => 183,
                'role_id' => 41,
            ),
            198 => 
            array (
                'permission_id' => 184,
                'role_id' => 1,
            ),
            199 => 
            array (
                'permission_id' => 184,
                'role_id' => 37,
            ),
            200 => 
            array (
                'permission_id' => 184,
                'role_id' => 39,
            ),
            201 => 
            array (
                'permission_id' => 184,
                'role_id' => 40,
            ),
            202 => 
            array (
                'permission_id' => 184,
                'role_id' => 41,
            ),
            203 => 
            array (
                'permission_id' => 185,
                'role_id' => 1,
            ),
            204 => 
            array (
                'permission_id' => 185,
                'role_id' => 37,
            ),
            205 => 
            array (
                'permission_id' => 185,
                'role_id' => 38,
            ),
            206 => 
            array (
                'permission_id' => 185,
                'role_id' => 39,
            ),
            207 => 
            array (
                'permission_id' => 185,
                'role_id' => 40,
            ),
            208 => 
            array (
                'permission_id' => 185,
                'role_id' => 41,
            ),
            209 => 
            array (
                'permission_id' => 186,
                'role_id' => 1,
            ),
            210 => 
            array (
                'permission_id' => 186,
                'role_id' => 37,
            ),
            211 => 
            array (
                'permission_id' => 186,
                'role_id' => 39,
            ),
            212 => 
            array (
                'permission_id' => 186,
                'role_id' => 40,
            ),
            213 => 
            array (
                'permission_id' => 186,
                'role_id' => 41,
            ),
            214 => 
            array (
                'permission_id' => 187,
                'role_id' => 1,
            ),
            215 => 
            array (
                'permission_id' => 187,
                'role_id' => 27,
            ),
            216 => 
            array (
                'permission_id' => 187,
                'role_id' => 39,
            ),
            217 => 
            array (
                'permission_id' => 187,
                'role_id' => 40,
            ),
            218 => 
            array (
                'permission_id' => 187,
                'role_id' => 41,
            ),
            219 => 
            array (
                'permission_id' => 188,
                'role_id' => 1,
            ),
            220 => 
            array (
                'permission_id' => 188,
                'role_id' => 27,
            ),
            221 => 
            array (
                'permission_id' => 188,
                'role_id' => 39,
            ),
            222 => 
            array (
                'permission_id' => 188,
                'role_id' => 40,
            ),
            223 => 
            array (
                'permission_id' => 188,
                'role_id' => 41,
            ),
            224 => 
            array (
                'permission_id' => 189,
                'role_id' => 1,
            ),
            225 => 
            array (
                'permission_id' => 189,
                'role_id' => 39,
            ),
            226 => 
            array (
                'permission_id' => 189,
                'role_id' => 40,
            ),
            227 => 
            array (
                'permission_id' => 189,
                'role_id' => 41,
            ),
            228 => 
            array (
                'permission_id' => 190,
                'role_id' => 1,
            ),
            229 => 
            array (
                'permission_id' => 190,
                'role_id' => 39,
            ),
            230 => 
            array (
                'permission_id' => 190,
                'role_id' => 40,
            ),
            231 => 
            array (
                'permission_id' => 190,
                'role_id' => 41,
            ),
            232 => 
            array (
                'permission_id' => 191,
                'role_id' => 1,
            ),
            233 => 
            array (
                'permission_id' => 191,
                'role_id' => 39,
            ),
            234 => 
            array (
                'permission_id' => 191,
                'role_id' => 40,
            ),
            235 => 
            array (
                'permission_id' => 191,
                'role_id' => 41,
            ),
            236 => 
            array (
                'permission_id' => 192,
                'role_id' => 1,
            ),
            237 => 
            array (
                'permission_id' => 193,
                'role_id' => 1,
            ),
            238 => 
            array (
                'permission_id' => 194,
                'role_id' => 1,
            ),
            239 => 
            array (
                'permission_id' => 195,
                'role_id' => 1,
            ),
            240 => 
            array (
                'permission_id' => 196,
                'role_id' => 1,
            ),
            241 => 
            array (
                'permission_id' => 197,
                'role_id' => 1,
            ),
            242 => 
            array (
                'permission_id' => 198,
                'role_id' => 1,
            ),
            243 => 
            array (
                'permission_id' => 199,
                'role_id' => 1,
            ),
            244 => 
            array (
                'permission_id' => 200,
                'role_id' => 1,
            ),
            245 => 
            array (
                'permission_id' => 201,
                'role_id' => 1,
            ),
            246 => 
            array (
                'permission_id' => 202,
                'role_id' => 1,
            ),
            247 => 
            array (
                'permission_id' => 203,
                'role_id' => 1,
            ),
            248 => 
            array (
                'permission_id' => 204,
                'role_id' => 1,
            ),
            249 => 
            array (
                'permission_id' => 205,
                'role_id' => 1,
            ),
            250 => 
            array (
                'permission_id' => 206,
                'role_id' => 1,
            ),
            251 => 
            array (
                'permission_id' => 207,
                'role_id' => 1,
            ),
            252 => 
            array (
                'permission_id' => 208,
                'role_id' => 1,
            ),
            253 => 
            array (
                'permission_id' => 209,
                'role_id' => 1,
            ),
            254 => 
            array (
                'permission_id' => 210,
                'role_id' => 1,
            ),
            255 => 
            array (
                'permission_id' => 211,
                'role_id' => 1,
            ),
            256 => 
            array (
                'permission_id' => 212,
                'role_id' => 1,
            ),
            257 => 
            array (
                'permission_id' => 213,
                'role_id' => 1,
            ),
            258 => 
            array (
                'permission_id' => 214,
                'role_id' => 1,
            ),
            259 => 
            array (
                'permission_id' => 215,
                'role_id' => 1,
            ),
            260 => 
            array (
                'permission_id' => 216,
                'role_id' => 1,
            ),
            261 => 
            array (
                'permission_id' => 217,
                'role_id' => 1,
            ),
            262 => 
            array (
                'permission_id' => 218,
                'role_id' => 1,
            ),
            263 => 
            array (
                'permission_id' => 219,
                'role_id' => 1,
            ),
            264 => 
            array (
                'permission_id' => 220,
                'role_id' => 1,
            ),
            265 => 
            array (
                'permission_id' => 221,
                'role_id' => 1,
            ),
            266 => 
            array (
                'permission_id' => 222,
                'role_id' => 1,
            ),
            267 => 
            array (
                'permission_id' => 223,
                'role_id' => 1,
            ),
            268 => 
            array (
                'permission_id' => 224,
                'role_id' => 1,
            ),
            269 => 
            array (
                'permission_id' => 225,
                'role_id' => 1,
            ),
            270 => 
            array (
                'permission_id' => 226,
                'role_id' => 1,
            ),
            271 => 
            array (
                'permission_id' => 227,
                'role_id' => 1,
            ),
            272 => 
            array (
                'permission_id' => 228,
                'role_id' => 1,
            ),
            273 => 
            array (
                'permission_id' => 229,
                'role_id' => 1,
            ),
            274 => 
            array (
                'permission_id' => 230,
                'role_id' => 1,
            ),
            275 => 
            array (
                'permission_id' => 231,
                'role_id' => 1,
            ),
            276 => 
            array (
                'permission_id' => 232,
                'role_id' => 1,
            ),
            277 => 
            array (
                'permission_id' => 233,
                'role_id' => 1,
            ),
            278 => 
            array (
                'permission_id' => 234,
                'role_id' => 1,
            ),
            279 => 
            array (
                'permission_id' => 235,
                'role_id' => 1,
            ),
            280 => 
            array (
                'permission_id' => 236,
                'role_id' => 1,
            ),
            281 => 
            array (
                'permission_id' => 237,
                'role_id' => 1,
            ),
            282 => 
            array (
                'permission_id' => 238,
                'role_id' => 1,
            ),
            283 => 
            array (
                'permission_id' => 239,
                'role_id' => 1,
            ),
            284 => 
            array (
                'permission_id' => 240,
                'role_id' => 1,
            ),
            285 => 
            array (
                'permission_id' => 241,
                'role_id' => 1,
            ),
            286 => 
            array (
                'permission_id' => 242,
                'role_id' => 1,
            ),
            287 => 
            array (
                'permission_id' => 242,
                'role_id' => 27,
            ),
            288 => 
            array (
                'permission_id' => 242,
                'role_id' => 48,
            ),
            289 => 
            array (
                'permission_id' => 243,
                'role_id' => 1,
            ),
            290 => 
            array (
                'permission_id' => 243,
                'role_id' => 27,
            ),
            291 => 
            array (
                'permission_id' => 243,
                'role_id' => 48,
            ),
            292 => 
            array (
                'permission_id' => 244,
                'role_id' => 1,
            ),
            293 => 
            array (
                'permission_id' => 244,
                'role_id' => 48,
            ),
            294 => 
            array (
                'permission_id' => 245,
                'role_id' => 1,
            ),
            295 => 
            array (
                'permission_id' => 245,
                'role_id' => 48,
            ),
            296 => 
            array (
                'permission_id' => 246,
                'role_id' => 1,
            ),
            297 => 
            array (
                'permission_id' => 246,
                'role_id' => 48,
            ),
        ));
        
        
    }
}