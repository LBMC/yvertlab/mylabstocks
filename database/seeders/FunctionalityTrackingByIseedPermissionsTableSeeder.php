<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FunctionalityTrackingByIseedPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2020-01-30 08:14:03',
                'updated_at' => '2020-01-30 08:14:03',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2020-01-30 08:14:04',
                'updated_at' => '2020-01-30 08:14:04',
            ),
            26 => 
            array (
                'id' => 37,
                'key' => 'browse_pl_bacterial_selection',
                'table_name' => 'pl_bacterial_selection',
                'created_at' => '2020-02-12 14:21:48',
                'updated_at' => '2020-02-12 14:21:48',
            ),
            27 => 
            array (
                'id' => 38,
                'key' => 'read_pl_bacterial_selection',
                'table_name' => 'pl_bacterial_selection',
                'created_at' => '2020-02-12 14:21:48',
                'updated_at' => '2020-02-12 14:21:48',
            ),
            28 => 
            array (
                'id' => 39,
                'key' => 'edit_pl_bacterial_selection',
                'table_name' => 'pl_bacterial_selection',
                'created_at' => '2020-02-12 14:21:48',
                'updated_at' => '2020-02-12 14:21:48',
            ),
            29 => 
            array (
                'id' => 40,
                'key' => 'add_pl_bacterial_selection',
                'table_name' => 'pl_bacterial_selection',
                'created_at' => '2020-02-12 14:21:48',
                'updated_at' => '2020-02-12 14:21:48',
            ),
            30 => 
            array (
                'id' => 41,
                'key' => 'delete_pl_bacterial_selection',
                'table_name' => 'pl_bacterial_selection',
                'created_at' => '2020-02-12 14:21:48',
                'updated_at' => '2020-02-12 14:21:48',
            ),
            31 => 
            array (
                'id' => 42,
                'key' => 'browse_antibodies',
                'table_name' => 'antibodies',
                'created_at' => '2020-02-19 10:10:23',
                'updated_at' => '2020-02-19 10:10:23',
            ),
            32 => 
            array (
                'id' => 43,
                'key' => 'read_antibodies',
                'table_name' => 'antibodies',
                'created_at' => '2020-02-19 10:10:23',
                'updated_at' => '2020-02-19 10:10:23',
            ),
            33 => 
            array (
                'id' => 44,
                'key' => 'edit_antibodies',
                'table_name' => 'antibodies',
                'created_at' => '2020-02-19 10:10:23',
                'updated_at' => '2020-02-19 10:10:23',
            ),
            34 => 
            array (
                'id' => 45,
                'key' => 'add_antibodies',
                'table_name' => 'antibodies',
                'created_at' => '2020-02-19 10:10:23',
                'updated_at' => '2020-02-19 10:10:23',
            ),
            35 => 
            array (
                'id' => 46,
                'key' => 'delete_antibodies',
                'table_name' => 'antibodies',
                'created_at' => '2020-02-19 10:10:23',
                'updated_at' => '2020-02-19 10:10:23',
            ),
            36 => 
            array (
                'id' => 47,
                'key' => 'browse_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2020-02-19 10:11:54',
                'updated_at' => '2020-02-19 10:11:54',
            ),
            37 => 
            array (
                'id' => 48,
                'key' => 'read_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2020-02-19 10:11:54',
                'updated_at' => '2020-02-19 10:11:54',
            ),
            38 => 
            array (
                'id' => 49,
                'key' => 'edit_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2020-02-19 10:11:54',
                'updated_at' => '2020-02-19 10:11:54',
            ),
            39 => 
            array (
                'id' => 50,
                'key' => 'add_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2020-02-19 10:11:54',
                'updated_at' => '2020-02-19 10:11:54',
            ),
            40 => 
            array (
                'id' => 51,
                'key' => 'delete_suppliers',
                'table_name' => 'suppliers',
                'created_at' => '2020-02-19 10:11:54',
                'updated_at' => '2020-02-19 10:11:54',
            ),
            41 => 
            array (
                'id' => 52,
                'key' => 'browse_oligos',
                'table_name' => 'oligos',
                'created_at' => '2020-02-20 10:20:16',
                'updated_at' => '2020-02-20 10:20:16',
            ),
            42 => 
            array (
                'id' => 53,
                'key' => 'read_oligos',
                'table_name' => 'oligos',
                'created_at' => '2020-02-20 10:20:16',
                'updated_at' => '2020-02-20 10:20:16',
            ),
            43 => 
            array (
                'id' => 54,
                'key' => 'edit_oligos',
                'table_name' => 'oligos',
                'created_at' => '2020-02-20 10:20:16',
                'updated_at' => '2020-02-20 10:20:16',
            ),
            44 => 
            array (
                'id' => 55,
                'key' => 'add_oligos',
                'table_name' => 'oligos',
                'created_at' => '2020-02-20 10:20:16',
                'updated_at' => '2020-02-20 10:20:16',
            ),
            45 => 
            array (
                'id' => 56,
                'key' => 'delete_oligos',
                'table_name' => 'oligos',
                'created_at' => '2020-02-20 10:20:16',
                'updated_at' => '2020-02-20 10:20:16',
            ),
            46 => 
            array (
                'id' => 72,
                'key' => 'browse_plasmids',
                'table_name' => 'plasmids',
                'created_at' => '2020-02-24 14:41:53',
                'updated_at' => '2020-02-24 14:41:53',
            ),
            47 => 
            array (
                'id' => 73,
                'key' => 'read_plasmids',
                'table_name' => 'plasmids',
                'created_at' => '2020-02-24 14:41:53',
                'updated_at' => '2020-02-24 14:41:53',
            ),
            48 => 
            array (
                'id' => 74,
                'key' => 'edit_plasmids',
                'table_name' => 'plasmids',
                'created_at' => '2020-02-24 14:41:53',
                'updated_at' => '2020-02-24 14:41:53',
            ),
            49 => 
            array (
                'id' => 75,
                'key' => 'add_plasmids',
                'table_name' => 'plasmids',
                'created_at' => '2020-02-24 14:41:53',
                'updated_at' => '2020-02-24 14:41:53',
            ),
            50 => 
            array (
                'id' => 76,
                'key' => 'delete_plasmids',
                'table_name' => 'plasmids',
                'created_at' => '2020-02-24 14:41:53',
                'updated_at' => '2020-02-24 14:41:53',
            ),
            51 => 
            array (
                'id' => 82,
                'key' => 'browse_histories',
                'table_name' => 'histories',
                'created_at' => '2020-02-24 14:58:09',
                'updated_at' => '2020-02-24 14:58:09',
            ),
            52 => 
            array (
                'id' => 83,
                'key' => 'read_histories',
                'table_name' => 'histories',
                'created_at' => '2020-02-24 14:58:09',
                'updated_at' => '2020-02-24 14:58:09',
            ),
            53 => 
            array (
                'id' => 84,
                'key' => 'edit_histories',
                'table_name' => 'histories',
                'created_at' => '2020-02-24 14:58:09',
                'updated_at' => '2020-02-24 14:58:09',
            ),
            54 => 
            array (
                'id' => 85,
                'key' => 'add_histories',
                'table_name' => 'histories',
                'created_at' => '2020-02-24 14:58:09',
                'updated_at' => '2020-02-24 14:58:09',
            ),
            55 => 
            array (
                'id' => 86,
                'key' => 'delete_histories',
                'table_name' => 'histories',
                'created_at' => '2020-02-24 14:58:09',
                'updated_at' => '2020-02-24 14:58:09',
            ),
            56 => 
            array (
                'id' => 87,
                'key' => 'browse_cell_lines',
                'table_name' => 'cell_lines',
                'created_at' => '2020-02-25 12:30:54',
                'updated_at' => '2020-02-25 12:30:54',
            ),
            57 => 
            array (
                'id' => 88,
                'key' => 'read_cell_lines',
                'table_name' => 'cell_lines',
                'created_at' => '2020-02-25 12:30:54',
                'updated_at' => '2020-02-25 12:30:54',
            ),
            58 => 
            array (
                'id' => 89,
                'key' => 'edit_cell_lines',
                'table_name' => 'cell_lines',
                'created_at' => '2020-02-25 12:30:54',
                'updated_at' => '2020-02-25 12:30:54',
            ),
            59 => 
            array (
                'id' => 90,
                'key' => 'add_cell_lines',
                'table_name' => 'cell_lines',
                'created_at' => '2020-02-25 12:30:54',
                'updated_at' => '2020-02-25 12:30:54',
            ),
            60 => 
            array (
                'id' => 91,
                'key' => 'delete_cell_lines',
                'table_name' => 'cell_lines',
                'created_at' => '2020-02-25 12:30:54',
                'updated_at' => '2020-02-25 12:30:54',
            ),
            61 => 
            array (
                'id' => 92,
                'key' => 'browse_pipets',
                'table_name' => 'pipets',
                'created_at' => '2020-02-25 12:50:39',
                'updated_at' => '2020-02-25 12:50:39',
            ),
            62 => 
            array (
                'id' => 93,
                'key' => 'read_pipets',
                'table_name' => 'pipets',
                'created_at' => '2020-02-25 12:50:39',
                'updated_at' => '2020-02-25 12:50:39',
            ),
            63 => 
            array (
                'id' => 94,
                'key' => 'edit_pipets',
                'table_name' => 'pipets',
                'created_at' => '2020-02-25 12:50:39',
                'updated_at' => '2020-02-25 12:50:39',
            ),
            64 => 
            array (
                'id' => 95,
                'key' => 'add_pipets',
                'table_name' => 'pipets',
                'created_at' => '2020-02-25 12:50:39',
                'updated_at' => '2020-02-25 12:50:39',
            ),
            65 => 
            array (
                'id' => 96,
                'key' => 'delete_pipets',
                'table_name' => 'pipets',
                'created_at' => '2020-02-25 12:50:39',
                'updated_at' => '2020-02-25 12:50:39',
            ),
            66 => 
            array (
                'id' => 97,
                'key' => 'browse_events',
                'table_name' => 'events',
                'created_at' => '2020-02-25 14:09:37',
                'updated_at' => '2020-02-25 14:09:37',
            ),
            67 => 
            array (
                'id' => 98,
                'key' => 'read_events',
                'table_name' => 'events',
                'created_at' => '2020-02-25 14:09:37',
                'updated_at' => '2020-02-25 14:09:37',
            ),
            68 => 
            array (
                'id' => 99,
                'key' => 'edit_events',
                'table_name' => 'events',
                'created_at' => '2020-02-25 14:09:37',
                'updated_at' => '2020-02-25 14:09:37',
            ),
            69 => 
            array (
                'id' => 100,
                'key' => 'add_events',
                'table_name' => 'events',
                'created_at' => '2020-02-25 14:09:37',
                'updated_at' => '2020-02-25 14:09:37',
            ),
            70 => 
            array (
                'id' => 101,
                'key' => 'delete_events',
                'table_name' => 'events',
                'created_at' => '2020-02-25 14:09:37',
                'updated_at' => '2020-02-25 14:09:37',
            ),
            71 => 
            array (
                'id' => 102,
                'key' => 'browse_dna_features',
                'table_name' => 'dna_features',
                'created_at' => '2020-02-25 17:02:32',
                'updated_at' => '2020-02-25 17:02:32',
            ),
            72 => 
            array (
                'id' => 103,
                'key' => 'read_dna_features',
                'table_name' => 'dna_features',
                'created_at' => '2020-02-25 17:02:32',
                'updated_at' => '2020-02-25 17:02:32',
            ),
            73 => 
            array (
                'id' => 104,
                'key' => 'edit_dna_features',
                'table_name' => 'dna_features',
                'created_at' => '2020-02-25 17:02:32',
                'updated_at' => '2020-02-25 17:02:32',
            ),
            74 => 
            array (
                'id' => 105,
                'key' => 'add_dna_features',
                'table_name' => 'dna_features',
                'created_at' => '2020-02-25 17:02:32',
                'updated_at' => '2020-02-25 17:02:32',
            ),
            75 => 
            array (
                'id' => 106,
                'key' => 'delete_dna_features',
                'table_name' => 'dna_features',
                'created_at' => '2020-02-25 17:02:32',
                'updated_at' => '2020-02-25 17:02:32',
            ),
            76 => 
            array (
                'id' => 107,
                'key' => 'browse_strains',
                'table_name' => 'strains',
                'created_at' => '2020-02-27 09:29:49',
                'updated_at' => '2020-02-27 09:29:49',
            ),
            77 => 
            array (
                'id' => 108,
                'key' => 'read_strains',
                'table_name' => 'strains',
                'created_at' => '2020-02-27 09:29:49',
                'updated_at' => '2020-02-27 09:29:49',
            ),
            78 => 
            array (
                'id' => 109,
                'key' => 'edit_strains',
                'table_name' => 'strains',
                'created_at' => '2020-02-27 09:29:49',
                'updated_at' => '2020-02-27 09:29:49',
            ),
            79 => 
            array (
                'id' => 110,
                'key' => 'add_strains',
                'table_name' => 'strains',
                'created_at' => '2020-02-27 09:29:49',
                'updated_at' => '2020-02-27 09:29:49',
            ),
            80 => 
            array (
                'id' => 111,
                'key' => 'delete_strains',
                'table_name' => 'strains',
                'created_at' => '2020-02-27 09:29:49',
                'updated_at' => '2020-02-27 09:29:49',
            ),
            81 => 
            array (
                'id' => 112,
                'key' => 'browse_passages',
                'table_name' => 'passages',
                'created_at' => '2020-02-28 12:59:21',
                'updated_at' => '2020-02-28 12:59:21',
            ),
            82 => 
            array (
                'id' => 113,
                'key' => 'read_passages',
                'table_name' => 'passages',
                'created_at' => '2020-02-28 12:59:22',
                'updated_at' => '2020-02-28 12:59:22',
            ),
            83 => 
            array (
                'id' => 114,
                'key' => 'edit_passages',
                'table_name' => 'passages',
                'created_at' => '2020-02-28 12:59:22',
                'updated_at' => '2020-02-28 12:59:22',
            ),
            84 => 
            array (
                'id' => 115,
                'key' => 'add_passages',
                'table_name' => 'passages',
                'created_at' => '2020-02-28 12:59:22',
                'updated_at' => '2020-02-28 12:59:22',
            ),
            85 => 
            array (
                'id' => 116,
                'key' => 'delete_passages',
                'table_name' => 'passages',
                'created_at' => '2020-02-28 12:59:22',
                'updated_at' => '2020-02-28 12:59:22',
            ),
            86 => 
            array (
                'id' => 122,
                'key' => 'browse_notebooks',
                'table_name' => 'notebooks',
                'created_at' => '2020-03-05 10:28:30',
                'updated_at' => '2020-03-05 10:28:30',
            ),
            87 => 
            array (
                'id' => 123,
                'key' => 'read_notebooks',
                'table_name' => 'notebooks',
                'created_at' => '2020-03-05 10:28:30',
                'updated_at' => '2020-03-05 10:28:30',
            ),
            88 => 
            array (
                'id' => 124,
                'key' => 'edit_notebooks',
                'table_name' => 'notebooks',
                'created_at' => '2020-03-05 10:28:30',
                'updated_at' => '2020-03-05 10:28:30',
            ),
            89 => 
            array (
                'id' => 125,
                'key' => 'add_notebooks',
                'table_name' => 'notebooks',
                'created_at' => '2020-03-05 10:28:30',
                'updated_at' => '2020-03-05 10:28:30',
            ),
            90 => 
            array (
                'id' => 126,
                'key' => 'delete_notebooks',
                'table_name' => 'notebooks',
                'created_at' => '2020-03-05 10:28:30',
                'updated_at' => '2020-03-05 10:28:30',
            ),
            91 => 
            array (
                'id' => 127,
                'key' => 'browse_blog_posts',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:29',
                'updated_at' => '2020-06-26 16:02:29',
            ),
            92 => 
            array (
                'id' => 128,
                'key' => 'read_blog_posts',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:29',
                'updated_at' => '2020-06-26 16:02:29',
            ),
            93 => 
            array (
                'id' => 129,
                'key' => 'edit_blog_posts',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:29',
                'updated_at' => '2020-06-26 16:02:29',
            ),
            94 => 
            array (
                'id' => 130,
                'key' => 'add_blog_posts',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            95 => 
            array (
                'id' => 131,
                'key' => 'delete_blog_posts',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            96 => 
            array (
                'id' => 132,
                'key' => 'browse_blog_posts',
                'table_name' => 'blog_posts',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            97 => 
            array (
                'id' => 133,
                'key' => 'read_blog_posts',
                'table_name' => 'blog_posts',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            98 => 
            array (
                'id' => 134,
                'key' => 'edit_blog_posts',
                'table_name' => 'blog_posts',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            99 => 
            array (
                'id' => 135,
                'key' => 'add_blog_posts',
                'table_name' => 'blog_posts',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            100 => 
            array (
                'id' => 136,
                'key' => 'delete_blog_posts',
                'table_name' => 'blog_posts',
                'created_at' => '2020-06-26 16:02:30',
                'updated_at' => '2020-06-26 16:02:30',
            ),
            101 => 
            array (
                'id' => 137,
                'key' => 'browse_categories',
                'table_name' => 'categories',
                'created_at' => '2020-06-26 16:02:31',
                'updated_at' => '2020-06-26 16:02:31',
            ),
            102 => 
            array (
                'id' => 138,
                'key' => 'read_categories',
                'table_name' => 'categories',
                'created_at' => '2020-06-26 16:02:31',
                'updated_at' => '2020-06-26 16:02:31',
            ),
            103 => 
            array (
                'id' => 139,
                'key' => 'edit_categories',
                'table_name' => 'categories',
                'created_at' => '2020-06-26 16:02:31',
                'updated_at' => '2020-06-26 16:02:31',
            ),
            104 => 
            array (
                'id' => 140,
                'key' => 'add_categories',
                'table_name' => 'categories',
                'created_at' => '2020-06-26 16:02:31',
                'updated_at' => '2020-06-26 16:02:31',
            ),
            105 => 
            array (
                'id' => 141,
                'key' => 'delete_categories',
                'table_name' => 'categories',
                'created_at' => '2020-06-26 16:02:31',
                'updated_at' => '2020-06-26 16:02:31',
            ),
            106 => 
            array (
                'id' => 142,
                'key' => 'browse_pages',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            107 => 
            array (
                'id' => 143,
                'key' => 'read_pages',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            108 => 
            array (
                'id' => 144,
                'key' => 'edit_pages',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            109 => 
            array (
                'id' => 145,
                'key' => 'add_pages',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            110 => 
            array (
                'id' => 146,
                'key' => 'delete_pages',
                'table_name' => NULL,
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            111 => 
            array (
                'id' => 147,
                'key' => 'browse_pages',
                'table_name' => 'pages',
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            112 => 
            array (
                'id' => 148,
                'key' => 'read_pages',
                'table_name' => 'pages',
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            113 => 
            array (
                'id' => 149,
                'key' => 'edit_pages',
                'table_name' => 'pages',
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            114 => 
            array (
                'id' => 150,
                'key' => 'add_pages',
                'table_name' => 'pages',
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            115 => 
            array (
                'id' => 151,
                'key' => 'delete_pages',
                'table_name' => 'pages',
                'created_at' => '2020-06-26 16:02:34',
                'updated_at' => '2020-06-26 16:02:34',
            ),
            116 => 
            array (
                'id' => 157,
                'key' => 'browse_species',
                'table_name' => 'species',
                'created_at' => '2021-04-27 10:05:07',
                'updated_at' => '2021-04-27 10:05:07',
            ),
            117 => 
            array (
                'id' => 158,
                'key' => 'read_species',
                'table_name' => 'species',
                'created_at' => '2021-04-27 10:05:07',
                'updated_at' => '2021-04-27 10:05:07',
            ),
            118 => 
            array (
                'id' => 159,
                'key' => 'edit_species',
                'table_name' => 'species',
                'created_at' => '2021-04-27 10:05:07',
                'updated_at' => '2021-04-27 10:05:07',
            ),
            119 => 
            array (
                'id' => 160,
                'key' => 'add_species',
                'table_name' => 'species',
                'created_at' => '2021-04-27 10:05:07',
                'updated_at' => '2021-04-27 10:05:07',
            ),
            120 => 
            array (
                'id' => 161,
                'key' => 'delete_species',
                'table_name' => 'species',
                'created_at' => '2021-04-27 10:05:07',
                'updated_at' => '2021-04-27 10:05:07',
            ),
            121 => 
            array (
                'id' => 162,
                'key' => 'browse_rooms',
                'table_name' => 'rooms',
                'created_at' => '2021-05-18 16:58:39',
                'updated_at' => '2021-05-18 16:58:39',
            ),
            122 => 
            array (
                'id' => 163,
                'key' => 'read_rooms',
                'table_name' => 'rooms',
                'created_at' => '2021-05-18 16:58:39',
                'updated_at' => '2021-05-18 16:58:39',
            ),
            123 => 
            array (
                'id' => 164,
                'key' => 'edit_rooms',
                'table_name' => 'rooms',
                'created_at' => '2021-05-18 16:58:39',
                'updated_at' => '2021-05-18 16:58:39',
            ),
            124 => 
            array (
                'id' => 165,
                'key' => 'add_rooms',
                'table_name' => 'rooms',
                'created_at' => '2021-05-18 16:58:39',
                'updated_at' => '2021-05-18 16:58:39',
            ),
            125 => 
            array (
                'id' => 166,
                'key' => 'delete_rooms',
                'table_name' => 'rooms',
                'created_at' => '2021-05-18 16:58:39',
                'updated_at' => '2021-05-18 16:58:39',
            ),
            126 => 
            array (
                'id' => 167,
                'key' => 'browse_boxes',
                'table_name' => 'boxes',
                'created_at' => '2021-05-18 16:59:05',
                'updated_at' => '2021-05-18 16:59:05',
            ),
            127 => 
            array (
                'id' => 168,
                'key' => 'read_boxes',
                'table_name' => 'boxes',
                'created_at' => '2021-05-18 16:59:05',
                'updated_at' => '2021-05-18 16:59:05',
            ),
            128 => 
            array (
                'id' => 169,
                'key' => 'edit_boxes',
                'table_name' => 'boxes',
                'created_at' => '2021-05-18 16:59:05',
                'updated_at' => '2021-05-18 16:59:05',
            ),
            129 => 
            array (
                'id' => 170,
                'key' => 'add_boxes',
                'table_name' => 'boxes',
                'created_at' => '2021-05-18 16:59:05',
                'updated_at' => '2021-05-18 16:59:05',
            ),
            130 => 
            array (
                'id' => 171,
                'key' => 'delete_boxes',
                'table_name' => 'boxes',
                'created_at' => '2021-05-18 16:59:05',
                'updated_at' => '2021-05-18 16:59:05',
            ),
            131 => 
            array (
                'id' => 172,
                'key' => 'browse_freezers',
                'table_name' => 'freezers',
                'created_at' => '2021-05-18 16:59:34',
                'updated_at' => '2021-05-18 16:59:34',
            ),
            132 => 
            array (
                'id' => 173,
                'key' => 'read_freezers',
                'table_name' => 'freezers',
                'created_at' => '2021-05-18 16:59:34',
                'updated_at' => '2021-05-18 16:59:34',
            ),
            133 => 
            array (
                'id' => 174,
                'key' => 'edit_freezers',
                'table_name' => 'freezers',
                'created_at' => '2021-05-18 16:59:34',
                'updated_at' => '2021-05-18 16:59:34',
            ),
            134 => 
            array (
                'id' => 175,
                'key' => 'add_freezers',
                'table_name' => 'freezers',
                'created_at' => '2021-05-18 16:59:34',
                'updated_at' => '2021-05-18 16:59:34',
            ),
            135 => 
            array (
                'id' => 176,
                'key' => 'delete_freezers',
                'table_name' => 'freezers',
                'created_at' => '2021-05-18 16:59:34',
                'updated_at' => '2021-05-18 16:59:34',
            ),
            136 => 
            array (
                'id' => 182,
                'key' => 'browse_wellplates',
                'table_name' => 'wellplates',
                'created_at' => '2021-05-19 07:42:11',
                'updated_at' => '2021-05-19 07:42:11',
            ),
            137 => 
            array (
                'id' => 183,
                'key' => 'read_wellplates',
                'table_name' => 'wellplates',
                'created_at' => '2021-05-19 07:42:11',
                'updated_at' => '2021-05-19 07:42:11',
            ),
            138 => 
            array (
                'id' => 184,
                'key' => 'edit_wellplates',
                'table_name' => 'wellplates',
                'created_at' => '2021-05-19 07:42:11',
                'updated_at' => '2021-05-19 07:42:11',
            ),
            139 => 
            array (
                'id' => 185,
                'key' => 'add_wellplates',
                'table_name' => 'wellplates',
                'created_at' => '2021-05-19 07:42:11',
                'updated_at' => '2021-05-19 07:42:11',
            ),
            140 => 
            array (
                'id' => 186,
                'key' => 'delete_wellplates',
                'table_name' => 'wellplates',
                'created_at' => '2021-05-19 07:42:11',
                'updated_at' => '2021-05-19 07:42:11',
            ),
            141 => 
            array (
                'id' => 187,
                'key' => 'browse_projects',
                'table_name' => 'projects',
                'created_at' => '2021-06-11 11:53:14',
                'updated_at' => '2021-06-11 11:53:14',
            ),
            142 => 
            array (
                'id' => 188,
                'key' => 'read_projects',
                'table_name' => 'projects',
                'created_at' => '2021-06-11 11:53:14',
                'updated_at' => '2021-06-11 11:53:14',
            ),
            143 => 
            array (
                'id' => 189,
                'key' => 'edit_projects',
                'table_name' => 'projects',
                'created_at' => '2021-06-11 11:53:14',
                'updated_at' => '2021-06-11 11:53:14',
            ),
            144 => 
            array (
                'id' => 190,
                'key' => 'add_projects',
                'table_name' => 'projects',
                'created_at' => '2021-06-11 11:53:14',
                'updated_at' => '2021-06-11 11:53:14',
            ),
            145 => 
            array (
                'id' => 191,
                'key' => 'delete_projects',
                'table_name' => 'projects',
                'created_at' => '2021-06-11 11:53:14',
                'updated_at' => '2021-06-11 11:53:14',
            ),
            146 => 
            array (
                'id' => 192,
                'key' => 'browse_geneticmarkers',
                'table_name' => 'geneticmarkers',
                'created_at' => '2021-07-23 09:31:38',
                'updated_at' => '2021-07-23 09:31:38',
            ),
            147 => 
            array (
                'id' => 193,
                'key' => 'read_geneticmarkers',
                'table_name' => 'geneticmarkers',
                'created_at' => '2021-07-23 09:31:38',
                'updated_at' => '2021-07-23 09:31:38',
            ),
            148 => 
            array (
                'id' => 194,
                'key' => 'edit_geneticmarkers',
                'table_name' => 'geneticmarkers',
                'created_at' => '2021-07-23 09:31:38',
                'updated_at' => '2021-07-23 09:31:38',
            ),
            149 => 
            array (
                'id' => 195,
                'key' => 'add_geneticmarkers',
                'table_name' => 'geneticmarkers',
                'created_at' => '2021-07-23 09:31:38',
                'updated_at' => '2021-07-23 09:31:38',
            ),
            150 => 
            array (
                'id' => 196,
                'key' => 'delete_geneticmarkers',
                'table_name' => 'geneticmarkers',
                'created_at' => '2021-07-23 09:31:38',
                'updated_at' => '2021-07-23 09:31:38',
            ),
            151 => 
            array (
                'id' => 197,
                'key' => 'browse_st_ade2s',
                'table_name' => 'st_ade2s',
                'created_at' => '2022-06-21 07:44:53',
                'updated_at' => '2022-06-21 07:44:53',
            ),
            152 => 
            array (
                'id' => 198,
                'key' => 'read_st_ade2s',
                'table_name' => 'st_ade2s',
                'created_at' => '2022-06-21 07:44:53',
                'updated_at' => '2022-06-21 07:44:53',
            ),
            153 => 
            array (
                'id' => 199,
                'key' => 'edit_st_ade2s',
                'table_name' => 'st_ade2s',
                'created_at' => '2022-06-21 07:44:53',
                'updated_at' => '2022-06-21 07:44:53',
            ),
            154 => 
            array (
                'id' => 200,
                'key' => 'add_st_ade2s',
                'table_name' => 'st_ade2s',
                'created_at' => '2022-06-21 07:44:53',
                'updated_at' => '2022-06-21 07:44:53',
            ),
            155 => 
            array (
                'id' => 201,
                'key' => 'delete_st_ade2s',
                'table_name' => 'st_ade2s',
                'created_at' => '2022-06-21 07:44:53',
                'updated_at' => '2022-06-21 07:44:53',
            ),
            156 => 
            array (
                'id' => 202,
                'key' => 'browse_st_his3s',
                'table_name' => 'st_his3s',
                'created_at' => '2022-06-21 12:05:07',
                'updated_at' => '2022-06-21 12:05:07',
            ),
            157 => 
            array (
                'id' => 203,
                'key' => 'read_st_his3s',
                'table_name' => 'st_his3s',
                'created_at' => '2022-06-21 12:05:07',
                'updated_at' => '2022-06-21 12:05:07',
            ),
            158 => 
            array (
                'id' => 204,
                'key' => 'edit_st_his3s',
                'table_name' => 'st_his3s',
                'created_at' => '2022-06-21 12:05:07',
                'updated_at' => '2022-06-21 12:05:07',
            ),
            159 => 
            array (
                'id' => 205,
                'key' => 'add_st_his3s',
                'table_name' => 'st_his3s',
                'created_at' => '2022-06-21 12:05:07',
                'updated_at' => '2022-06-21 12:05:07',
            ),
            160 => 
            array (
                'id' => 206,
                'key' => 'delete_st_his3s',
                'table_name' => 'st_his3s',
                'created_at' => '2022-06-21 12:05:07',
                'updated_at' => '2022-06-21 12:05:07',
            ),
            161 => 
            array (
                'id' => 207,
                'key' => 'browse_st_leu2s',
                'table_name' => 'st_leu2s',
                'created_at' => '2022-06-21 12:11:35',
                'updated_at' => '2022-06-21 12:11:35',
            ),
            162 => 
            array (
                'id' => 208,
                'key' => 'read_st_leu2s',
                'table_name' => 'st_leu2s',
                'created_at' => '2022-06-21 12:11:35',
                'updated_at' => '2022-06-21 12:11:35',
            ),
            163 => 
            array (
                'id' => 209,
                'key' => 'edit_st_leu2s',
                'table_name' => 'st_leu2s',
                'created_at' => '2022-06-21 12:11:35',
                'updated_at' => '2022-06-21 12:11:35',
            ),
            164 => 
            array (
                'id' => 210,
                'key' => 'add_st_leu2s',
                'table_name' => 'st_leu2s',
                'created_at' => '2022-06-21 12:11:35',
                'updated_at' => '2022-06-21 12:11:35',
            ),
            165 => 
            array (
                'id' => 211,
                'key' => 'delete_st_leu2s',
                'table_name' => 'st_leu2s',
                'created_at' => '2022-06-21 12:11:35',
                'updated_at' => '2022-06-21 12:11:35',
            ),
            166 => 
            array (
                'id' => 212,
                'key' => 'browse_st_lys2s',
                'table_name' => 'st_lys2s',
                'created_at' => '2022-06-21 12:13:27',
                'updated_at' => '2022-06-21 12:13:27',
            ),
            167 => 
            array (
                'id' => 213,
                'key' => 'read_st_lys2s',
                'table_name' => 'st_lys2s',
                'created_at' => '2022-06-21 12:13:27',
                'updated_at' => '2022-06-21 12:13:27',
            ),
            168 => 
            array (
                'id' => 214,
                'key' => 'edit_st_lys2s',
                'table_name' => 'st_lys2s',
                'created_at' => '2022-06-21 12:13:27',
                'updated_at' => '2022-06-21 12:13:27',
            ),
            169 => 
            array (
                'id' => 215,
                'key' => 'add_st_lys2s',
                'table_name' => 'st_lys2s',
                'created_at' => '2022-06-21 12:13:27',
                'updated_at' => '2022-06-21 12:13:27',
            ),
            170 => 
            array (
                'id' => 216,
                'key' => 'delete_st_lys2s',
                'table_name' => 'st_lys2s',
                'created_at' => '2022-06-21 12:13:27',
                'updated_at' => '2022-06-21 12:13:27',
            ),
            171 => 
            array (
                'id' => 217,
                'key' => 'browse_st_mats',
                'table_name' => 'st_mats',
                'created_at' => '2022-06-21 12:15:42',
                'updated_at' => '2022-06-21 12:15:42',
            ),
            172 => 
            array (
                'id' => 218,
                'key' => 'read_st_mats',
                'table_name' => 'st_mats',
                'created_at' => '2022-06-21 12:15:42',
                'updated_at' => '2022-06-21 12:15:42',
            ),
            173 => 
            array (
                'id' => 219,
                'key' => 'edit_st_mats',
                'table_name' => 'st_mats',
                'created_at' => '2022-06-21 12:15:42',
                'updated_at' => '2022-06-21 12:15:42',
            ),
            174 => 
            array (
                'id' => 220,
                'key' => 'add_st_mats',
                'table_name' => 'st_mats',
                'created_at' => '2022-06-21 12:15:42',
                'updated_at' => '2022-06-21 12:15:42',
            ),
            175 => 
            array (
                'id' => 221,
                'key' => 'delete_st_mats',
                'table_name' => 'st_mats',
                'created_at' => '2022-06-21 12:15:42',
                'updated_at' => '2022-06-21 12:15:42',
            ),
            176 => 
            array (
                'id' => 222,
                'key' => 'browse_st_met15s',
                'table_name' => 'st_met15s',
                'created_at' => '2022-06-21 12:17:37',
                'updated_at' => '2022-06-21 12:17:37',
            ),
            177 => 
            array (
                'id' => 223,
                'key' => 'read_st_met15s',
                'table_name' => 'st_met15s',
                'created_at' => '2022-06-21 12:17:37',
                'updated_at' => '2022-06-21 12:17:37',
            ),
            178 => 
            array (
                'id' => 224,
                'key' => 'edit_st_met15s',
                'table_name' => 'st_met15s',
                'created_at' => '2022-06-21 12:17:37',
                'updated_at' => '2022-06-21 12:17:37',
            ),
            179 => 
            array (
                'id' => 225,
                'key' => 'add_st_met15s',
                'table_name' => 'st_met15s',
                'created_at' => '2022-06-21 12:17:37',
                'updated_at' => '2022-06-21 12:17:37',
            ),
            180 => 
            array (
                'id' => 226,
                'key' => 'delete_st_met15s',
                'table_name' => 'st_met15s',
                'created_at' => '2022-06-21 12:17:37',
                'updated_at' => '2022-06-21 12:17:37',
            ),
            181 => 
            array (
                'id' => 227,
                'key' => 'browse_st_trp1s',
                'table_name' => 'st_trp1s',
                'created_at' => '2022-06-21 12:19:13',
                'updated_at' => '2022-06-21 12:19:13',
            ),
            182 => 
            array (
                'id' => 228,
                'key' => 'read_st_trp1s',
                'table_name' => 'st_trp1s',
                'created_at' => '2022-06-21 12:19:13',
                'updated_at' => '2022-06-21 12:19:13',
            ),
            183 => 
            array (
                'id' => 229,
                'key' => 'edit_st_trp1s',
                'table_name' => 'st_trp1s',
                'created_at' => '2022-06-21 12:19:13',
                'updated_at' => '2022-06-21 12:19:13',
            ),
            184 => 
            array (
                'id' => 230,
                'key' => 'add_st_trp1s',
                'table_name' => 'st_trp1s',
                'created_at' => '2022-06-21 12:19:13',
                'updated_at' => '2022-06-21 12:19:13',
            ),
            185 => 
            array (
                'id' => 231,
                'key' => 'delete_st_trp1s',
                'table_name' => 'st_trp1s',
                'created_at' => '2022-06-21 12:19:13',
                'updated_at' => '2022-06-21 12:19:13',
            ),
            186 => 
            array (
                'id' => 232,
                'key' => 'browse_st_ura3s',
                'table_name' => 'st_ura3s',
                'created_at' => '2022-06-21 12:20:48',
                'updated_at' => '2022-06-21 12:20:48',
            ),
            187 => 
            array (
                'id' => 233,
                'key' => 'read_st_ura3s',
                'table_name' => 'st_ura3s',
                'created_at' => '2022-06-21 12:20:48',
                'updated_at' => '2022-06-21 12:20:48',
            ),
            188 => 
            array (
                'id' => 234,
                'key' => 'edit_st_ura3s',
                'table_name' => 'st_ura3s',
                'created_at' => '2022-06-21 12:20:48',
                'updated_at' => '2022-06-21 12:20:48',
            ),
            189 => 
            array (
                'id' => 235,
                'key' => 'add_st_ura3s',
                'table_name' => 'st_ura3s',
                'created_at' => '2022-06-21 12:20:48',
                'updated_at' => '2022-06-21 12:20:48',
            ),
            190 => 
            array (
                'id' => 236,
                'key' => 'delete_st_ura3s',
                'table_name' => 'st_ura3s',
                'created_at' => '2022-06-21 12:20:48',
                'updated_at' => '2022-06-21 12:20:48',
            ),
            191 => 
            array (
                'id' => 237,
                'key' => 'browse_purifs',
                'table_name' => 'purifs',
                'created_at' => '2024-05-29 15:41:52',
                'updated_at' => '2024-05-29 15:41:52',
            ),
            192 => 
            array (
                'id' => 238,
                'key' => 'read_purifs',
                'table_name' => 'purifs',
                'created_at' => '2024-05-29 15:41:52',
                'updated_at' => '2024-05-29 15:41:52',
            ),
            193 => 
            array (
                'id' => 239,
                'key' => 'edit_purifs',
                'table_name' => 'purifs',
                'created_at' => '2024-05-29 15:41:52',
                'updated_at' => '2024-05-29 15:41:52',
            ),
            194 => 
            array (
                'id' => 240,
                'key' => 'add_purifs',
                'table_name' => 'purifs',
                'created_at' => '2024-05-29 15:41:52',
                'updated_at' => '2024-05-29 15:41:52',
            ),
            195 => 
            array (
                'id' => 241,
                'key' => 'delete_purifs',
                'table_name' => 'purifs',
                'created_at' => '2024-05-29 15:41:52',
                'updated_at' => '2024-05-29 15:41:52',
            ),
            196 => 
            array (
                'id' => 242,
                'key' => 'browse_products',
                'table_name' => 'products',
                'created_at' => '2025-02-13 16:31:08',
                'updated_at' => '2025-02-13 16:31:08',
            ),
            197 => 
            array (
                'id' => 243,
                'key' => 'read_products',
                'table_name' => 'products',
                'created_at' => '2025-02-13 16:31:08',
                'updated_at' => '2025-02-13 16:31:08',
            ),
            198 => 
            array (
                'id' => 244,
                'key' => 'edit_products',
                'table_name' => 'products',
                'created_at' => '2025-02-13 16:31:08',
                'updated_at' => '2025-02-13 16:31:08',
            ),
            199 => 
            array (
                'id' => 245,
                'key' => 'add_products',
                'table_name' => 'products',
                'created_at' => '2025-02-13 16:31:08',
                'updated_at' => '2025-02-13 16:31:08',
            ),
            200 => 
            array (
                'id' => 246,
                'key' => 'delete_products',
                'table_name' => 'products',
                'created_at' => '2025-02-13 16:31:08',
                'updated_at' => '2025-02-13 16:31:08',
            ),
        ));
        
        
    }
}