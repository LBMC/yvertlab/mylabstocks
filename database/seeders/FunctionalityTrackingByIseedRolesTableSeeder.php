<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FunctionalityTrackingByIseedRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administrator',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-01-30 08:14:02',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'blocked',
            'display_name' => 'Inactivated (no permission)',
                'created_at' => '2020-01-30 08:14:02',
                'updated_at' => '2020-08-21 06:57:00',
            ),
            2 => 
            array (
                'id' => 13,
                'name' => 'plasmidadder',
                'display_name' => 'Plasmid Adder',
                'created_at' => '2020-08-21 09:37:11',
                'updated_at' => '2020-08-21 09:41:27',
            ),
            3 => 
            array (
                'id' => 15,
                'name' => 'strainadder',
                'display_name' => 'Strain Adder',
                'created_at' => '2020-08-21 09:51:32',
                'updated_at' => '2020-08-21 09:51:32',
            ),
            4 => 
            array (
                'id' => 16,
                'name' => 'oligoadder',
                'display_name' => 'Oligo Adder',
                'created_at' => '2020-08-21 09:52:26',
                'updated_at' => '2020-08-21 09:52:26',
            ),
            5 => 
            array (
                'id' => 20,
                'name' => 'celllineadder',
                'display_name' => 'Cell Line Adder',
                'created_at' => '2020-08-21 09:56:16',
                'updated_at' => '2020-08-21 09:56:16',
            ),
            6 => 
            array (
                'id' => 21,
                'name' => 'antibodyadder',
                'display_name' => 'Antibody Adder',
                'created_at' => '2020-08-21 09:56:46',
                'updated_at' => '2020-08-21 09:56:46',
            ),
            7 => 
            array (
                'id' => 24,
                'name' => 'notebookadder',
                'display_name' => 'Notebook Adder',
                'created_at' => '2020-08-21 09:58:17',
                'updated_at' => '2020-08-21 09:58:17',
            ),
            8 => 
            array (
                'id' => 25,
                'name' => 'pipetadder',
                'display_name' => 'Pipet Adder',
                'created_at' => '2020-08-21 09:59:17',
                'updated_at' => '2020-08-21 09:59:17',
            ),
            9 => 
            array (
                'id' => 27,
                'name' => 'viewer',
                'display_name' => 'Viewer',
                'created_at' => '2020-08-21 15:50:19',
                'updated_at' => '2020-08-21 15:50:19',
            ),
            10 => 
            array (
                'id' => 28,
                'name' => 'plasmidediter',
                'display_name' => 'Plasmid Editer',
                'created_at' => '2020-09-08 11:24:10',
                'updated_at' => '2020-09-08 11:31:05',
            ),
            11 => 
            array (
                'id' => 29,
                'name' => 'pipetediter',
                'display_name' => 'Pipet Editer',
                'created_at' => '2020-09-08 11:46:19',
                'updated_at' => '2020-09-08 11:46:19',
            ),
            12 => 
            array (
                'id' => 31,
                'name' => 'notebookediter',
                'display_name' => 'Notebook Editer',
                'created_at' => '2020-09-08 11:49:47',
                'updated_at' => '2020-09-08 11:49:47',
            ),
            13 => 
            array (
                'id' => 32,
                'name' => 'antibodyediter',
                'display_name' => 'Antibody Editer',
                'created_at' => '2020-09-08 11:50:43',
                'updated_at' => '2020-09-08 11:50:43',
            ),
            14 => 
            array (
                'id' => 33,
                'name' => 'celllineediter',
                'display_name' => 'Cell Line Editer',
                'created_at' => '2020-09-08 11:51:20',
                'updated_at' => '2020-09-08 11:51:20',
            ),
            15 => 
            array (
                'id' => 34,
                'name' => 'strainediter',
                'display_name' => 'Strain Editer',
                'created_at' => '2020-09-08 11:52:09',
                'updated_at' => '2020-09-08 11:52:09',
            ),
            16 => 
            array (
                'id' => 35,
                'name' => 'oligoediter',
                'display_name' => 'Oligo Editer',
                'created_at' => '2020-09-08 11:53:32',
                'updated_at' => '2020-10-15 18:23:12',
            ),
            17 => 
            array (
                'id' => 37,
                'name' => 'wellplateediter',
                'display_name' => 'Wellplate Editer',
                'created_at' => '2021-06-11 08:24:26',
                'updated_at' => '2021-06-11 08:24:26',
            ),
            18 => 
            array (
                'id' => 38,
                'name' => 'wellplateadder',
                'display_name' => 'Wellplate Adder',
                'created_at' => '2021-06-11 08:25:56',
                'updated_at' => '2021-06-11 08:25:56',
            ),
            19 => 
            array (
                'id' => 39,
                'name' => 'hypermanager',
                'display_name' => 'Hypermanager',
                'created_at' => '2021-07-06 09:26:03',
                'updated_at' => '2021-07-06 09:38:38',
            ),
            20 => 
            array (
                'id' => 40,
                'name' => 'supermanager',
                'display_name' => 'Supermanager',
                'created_at' => '2021-07-06 09:28:25',
                'updated_at' => '2021-07-06 09:38:47',
            ),
            21 => 
            array (
                'id' => 41,
                'name' => 'manager',
                'display_name' => 'Manager',
                'created_at' => '2021-07-06 09:31:32',
                'updated_at' => '2021-07-06 09:38:55',
            ),
            22 => 
            array (
                'id' => 42,
                'name' => 'passageadder',
                'display_name' => 'Passage Adder',
                'created_at' => '2021-07-06 09:39:54',
                'updated_at' => '2021-07-06 09:39:54',
            ),
            23 => 
            array (
                'id' => 43,
                'name' => 'passageediter',
                'display_name' => 'Passage Editer',
                'created_at' => '2021-07-06 09:41:27',
                'updated_at' => '2021-07-06 09:41:27',
            ),
            24 => 
            array (
                'id' => 44,
                'name' => 'dnafeatureadder',
                'display_name' => 'DnaFeature Adder',
                'created_at' => '2021-07-06 09:42:05',
                'updated_at' => '2021-07-06 09:42:05',
            ),
            25 => 
            array (
                'id' => 45,
                'name' => 'dnafeatureediter',
                'display_name' => 'DnaFeature Editer',
                'created_at' => '2021-07-06 09:42:36',
                'updated_at' => '2021-07-06 09:42:36',
            ),
            26 => 
            array (
                'id' => 46,
                'name' => 'eventadder',
                'display_name' => 'Event Adder',
                'created_at' => '2021-07-06 09:43:12',
                'updated_at' => '2021-07-06 09:43:12',
            ),
            27 => 
            array (
                'id' => 47,
                'name' => 'eventediter',
                'display_name' => 'Event Editer',
                'created_at' => '2021-07-06 09:43:40',
                'updated_at' => '2021-07-06 09:43:40',
            ),
            28 => 
            array (
                'id' => 48,
                'name' => 'productediter',
                'display_name' => 'Product Editer',
                'created_at' => '2025-02-14 08:24:43',
                'updated_at' => '2025-02-14 08:24:43',
            ),
        ));
        
        
    }
}