<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class OligosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oligos')->insert([
            'name' => '1Z01',
            'sequence' => 'GATCCCCGGGAATTGCCATGCTGCTCCTTCTTGCATCTTC',
            'description' => 'just a test oligo',
            'pcr_conditions_predicted' => '',
            'author' => 'Omar.Sy',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('oligos')->insert([
            'name' => '1Z02',
            'sequence' => 'GATCCCCGGGAATTGCCATGCTCCCCTCGACGTATCTTC',
            'description' => 'just another test oligo',
            'pcr_conditions_predicted' => '',
            'author' => 'Omar.Sy',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
        DB::table('oligos')->insert([
            'name' => '1Z03',
            'sequence' => 'GATCGCTGACCTGCATACCATGGCCCTCGACGTATCTTC',
            'description' => 'yet another test oligo',
            'pcr_conditions_predicted' => '',
            'author' => 'Omar.Sy',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    
    }
}
