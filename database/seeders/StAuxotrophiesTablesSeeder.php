<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StAuxotrophiesTablesSeeder extends Seeder
{

    /**
     * Seed file for S. cerevisiae strains auxotrophies
     * @return void
     */

    public function run()
    {
        /*********
        * 
        *  ADE2
        *
        **********/
        \DB::table('st_ade2s')->delete();
        
        \DB::table('st_ade2s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'ade2-1',
            ),
            2 => 
            array (
                'alleles' => 'ade2-1/ADE2',
            ),
            3 => 
            array (
                'alleles' => 'ade2-1/ade2-1',
            ),
            4 => 
            array (
                'alleles' => 'ADE2.',
            ),
            5 => 
            array (
                'alleles' => 'ade2/ade2',
            ),
            6 => 
            array (
                'alleles' => 'ADE2/ade2D::hisG',
            ),
            7 => 
            array (
                'alleles' => 'ade2D::hisG',
            ),
            8 => 
            array (
                'alleles' => 'loxP-ADE2-loxP-HphMX',
            ),
            9 => 
            array (
                'alleles' => 'unknown',
            ),
        ));

       
       /*********
        * 
        *  HIS3
        *
        **********/

        \DB::table('st_his3s')->delete();
        
        \DB::table('st_his3s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'his3',
            ),
            2 => 
            array (
                'alleles' => 'his3-11,15',
            ),
            3 => 
            array (
                'alleles' => 'his3-11,15/HIS3',
            ),
            4 => 
            array (
                'alleles' => 'his3-11,15/his3-11,15',
            ),
            5 => 
            array (
                'alleles' => 'HIS3.',
            ),
            6 => 
            array (
                'alleles' => 'his3D1',
            ),
            7 => 
            array (
                'alleles' => 'his3D1/HIS3',
            ),
            8 => 
            array (
                'alleles' => 'his3D1/his3D1',
            ),
            9 => 
            array (
                'alleles' => 'his3D200',
            ),
            10 => 
            array (
                'alleles' => 'his3D200/HIS3',
            ),
            11 => 
            array (
                'alleles' => 'his3D200/his3D200',
            ),
            12 => 
            array (
                'alleles' => 'unknown',
            ),
        ));

       /*********
        * 
        *  LEU2
        *
        **********/


        \DB::table('st_leu2s')->delete();
        
        \DB::table('st_leu2s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'leu2',
            ),
            2 => 
            array (
                'alleles' => 'leu2-3,112',
            ),
            3 => 
            array (
                'alleles' => 'leu2-3,112/LEU2',
            ),
            4 => 
            array (
                'alleles' => 'leu2-3,112/leu2-3,112',
            ),
            5 => 
            array (
                'alleles' => 'leu2-K',
            ),
            6 => 
            array (
                'alleles' => 'leu2-R',
            ),
            7 => 
            array (
                'alleles' => 'leu2-R/leu2-K',
            ),
            8 => 
            array (
                'alleles' => 'leu2-R/leu2-R',
            ),
            9 => 
            array (
                'alleles' => 'LEU2.',
            ),
            10 => 
            array (
                'alleles' => 'leu2::hisG',
            ),
            11 => 
            array (
                'alleles' => 'leu2::hisG/LEU2',
            ),
            12 => 
            array (
                'alleles' => 'leu2::hisG/leu2::hisG',
            ),
            13 => 
            array (
                'alleles' => 'leu2D0/leu2D1',
            ),
            14 => 
            array (
            'alleles' => 'leu2D(asp781-EcoRI)',
            ),
            15 => 
            array (
            'alleles' => 'leu2D(asp781-EcoRI)/LEU2',
            ),
            16 => 
            array (
            'alleles' => 'leu2D(asp781-EcoRI)/leu2D(asp781-EcoRI)',
            ),
            17 => 
            array (
                'alleles' => 'leu2D0',
            ),
            18 => 
            array (
                'alleles' => 'leu2D0/LEU2',
            ),
            19 => 
            array (
                'alleles' => 'leu2D0/leu2D0',
            ),
            20 => 
            array (
                'alleles' => 'leu2D1',
            ),
            21 => 
            array (
                'alleles' => 'leu2D1/LEU2',
            ),
            22 => 
            array (
                'alleles' => 'leu2D1/leu2D1',
            ),
            23 => 
            array (
                'alleles' => 'unknown',
            ),
        ));


       /*********
        * 
        *  LYS2
        *
        **********/
        \DB::table('st_lys2s')->delete();
        
        \DB::table('st_lys2s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'lys2',
            ),
            2 => 
            array (
                'alleles' => 'lys2-128d',
            ),
            3 => 
            array (
                'alleles' => 'LYS2.',
            ),
            4 => 
            array (
                'alleles' => 'lys2/lys2',
            ),
            5 => 
            array (
                'alleles' => 'LYS2/lys2D::loxLEUloxCrtI',
            ),
            6 => 
            array (
                'alleles' => 'LYS2/lys2D::loxLEUloxGFP',
            ),
            7 => 
            array (
                'alleles' => 'lys2D::loxLEUloxGFP',
            ),
            8 => 
            array (
                'alleles' => 'lys2D::loxLEUloxCrtI',
            ),
            9 => 
            array (
                'alleles' => 'lys2D0',
            ),
            10 => 
            array (
                'alleles' => 'lys2D0/LYS2',
            ),
            11 => 
            array (
                'alleles' => 'lys2D0/lys2D0',
            ),
            12 => 
            array (
                'alleles' => 'lys2D0::lox-ADEaru-xol',
            ),
            13 => 
            array (
                'alleles' => 'lys2D202',
            ),
            14 => 
            array (
                'alleles' => 'lys2D202/LYS2',
            ),
            15 => 
            array (
                'alleles' => 'lys2D202/lys2D202',
            ),
            16 => 
            array (
                'alleles' => 'unknown',
            ),
        ));


       /*********
        * 
        *  MAT
        *
        **********/

        \DB::table('st_mats')->delete();
        
        \DB::table('st_mats')->insert(array (
            0 => 
            array (
                'alleles' => 'MATa',
            ),
            1 => 
            array (
                'alleles' => 'MATa/MATa',
            ),
            2 => 
            array (
                'alleles' => 'MATa/MATa/MATb/MATb',
            ),
            3 => 
            array (
                'alleles' => 'MATa/MATb',
            ),
            4 => 
            array (
                'alleles' => 'MATb',
            ),
            5 => 
            array (
                'alleles' => 'MATb/MATb',
            ),
            6 => 
            array (
                'alleles' => 'MATb/MATb/MATa',
            ),
            7 => 
            array (
                'alleles' => 'unknown',
            ),
        ));


       /*********
        * 
        *  MET15
        *
        **********/

        \DB::table('st_met15s')->delete();
        
        \DB::table('st_met15s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'MET15.',
            ),
            2 => 
            array (
                'alleles' => 'MET15/met15D::loxLEUloxCrtE',
            ),
            3 => 
            array (
                'alleles' => 'met15D::loxLEUloxCrtE',
            ),
            4 => 
            array (
                'alleles' => 'met15D0',
            ),
            5 => 
            array (
                'alleles' => 'met15D0/MET15',
            ),
            6 => 
            array (
                'alleles' => 'met15D0/met15D0',
            ),
            7 => 
            array (
                'alleles' => 'unknown',
            ),
        ));
        

       /*********
        * 
        *  TRP1
        *
        **********/

        \DB::table('st_trp1s')->delete();
        
        \DB::table('st_trp1s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'trp1',
            ),
            2 => 
            array (
                'alleles' => 'trp1-1',
            ),
            3 => 
            array (
                'alleles' => 'trp1-1/TRP1',
            ),
            4 => 
            array (
                'alleles' => 'trp1-1/trp1-1',
            ),
            5 => 
            array (
                'alleles' => 'trp1-289',
            ),
            6 => 
            array (
                'alleles' => 'trp1-289/TRP1',
            ),
            7 => 
            array (
                'alleles' => 'trp1-289/trp1-289',
            ),
            8 => 
            array (
                'alleles' => 'TRP1.',
            ),
            9 => 
            array (
                'alleles' => 'trp1D::KanMX4/trp1D::63',
            ),
            10 => 
            array (
                'alleles' => 'trp1D1',
            ),
            11 => 
            array (
                'alleles' => 'trp1D1/TRP1',
            ),
            12 => 
            array (
                'alleles' => 'trp1D1/trp1D1',
            ),
            13 => 
            array (
                'alleles' => 'trp1D2',
            ),
            14 => 
            array (
                'alleles' => 'trp1D2/TRP1',
            ),
            15 => 
            array (
                'alleles' => 'trp1D2/trp1D2',
            ),
            16 => 
            array (
                'alleles' => 'trp1D63',
            ),
            17 => 
            array (
                'alleles' => 'trp1D63/TRP1',
            ),
            18 => 
            array (
                'alleles' => 'trp1D63/trp1D63',
            ),
            19 => 
            array (
                'alleles' => 'trp1D::KanMX4',
            ),
            20 => 
            array (
                'alleles' => 'trp1D::KanMX4/TRP1',
            ),
            21 => 
            array (
                'alleles' => 'trp1D::KanMX4/trp1D::KanMX4',
            ),
        ));
        

       /*********
        * 
        *  URA3
        *
        **********/

        \DB::table('st_ura3s')->delete();
        
        \DB::table('st_ura3s')->insert(array (
            0 => 
            array (
                'alleles' => '',
            ),
            1 => 
            array (
                'alleles' => 'unknown',
            ),
            2 => 
            array (
                'alleles' => 'ura3',
            ),
            3 => 
            array (
                'alleles' => 'ura3-1',
            ),
            4 => 
            array (
                'alleles' => 'ura3-52',
            ),
            5 => 
            array (
                'alleles' => 'ura3-52/URA3',
            ),
            6 => 
            array (
                'alleles' => 'ura3-52/ura3-52',
            ),
            7 => 
            array (
                'alleles' => 'URA3.',
            ),
            8 => 
            array (
                'alleles' => 'ura3/ura3',
            ),
            9 => 
            array (
                'alleles' => 'ura3::KanMX',
            ),
            10 => 
            array (
                'alleles' => 'ura3::KanMX/URA3',
            ),
            11 => 
            array (
                'alleles' => 'ura3D0',
            ),
            12 => 
            array (
                'alleles' => 'ura3D0/URA3',
            ),
            13 => 
            array (
                'alleles' => 'ura3D0/ura3D0',
            ),
            14 => 
            array (
                'alleles' => 'ura3D0::TRP1',
            ),
            15 => 
            array (
                'alleles' => 'ura3D::hphNT1',
            ),
            16 => 
            array (
                'alleles' => 'URA::CMV-tTA',
            ),
        ));
        
        
    }
}
