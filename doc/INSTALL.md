# MyLabStocks: A web-application to manage molecular biology materials.


# Installation Instructions 

This installation was tested on:
   * Ubuntu server 24.04.1 LTS
   
   
### Prerequisites

To install MyLabStocks, the following packages must be installed on your system :
- (openssh-server) if not already installed, for example if you are on Ubuntu Desktop.
- git
- apache2
- mysql-server
- php 8.3
- php-curl
- csh
- ncbi-blast+
- ncbi-blast+-legacy
- emboss
- pdf2svg

On the targeted server, you can install these packages by typing the
following command in a terminal :
```sh
   sudo apt-get install apache2 \
                        mysql-server \
                        php8.3 php8.3-xml php8.3-mysql \
                        php8.3-gd php8.3-opcache php8.3-mbstring \
                        libapache2-mod-php \
                        curl php-curl \
                        csh \
                        git \
                        ncbi-blast+ ncbi-blast+-legacy \
                        emboss \
                        pdf2svg
```

You also need to give yourself the permissions to write in the application directory without using sudo all the time. You can do this by adding yourself to the 'www-data' group, so  if your login name is 'myloginname' type in a terminal:

```sh
   sudo usermod -a -G www-data myloginname
```

### Database setup

MyLabStocks uses MySQL to store data. For this, two things must be created in your MySQL server: a database and a user with administrative priviledges on this database. You therefore need to choose a username and password for this MySQL user.

To create the database and user, type the following commands in a terminal, after changing 'admin' and 'password' by the username and password of your choice.

```sh
   sudo mysql
   CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
   CREATE DATABASE labstocks_db CHARACTER SET utf8 COLLATE utf8_bin;
   GRANT ALL PRIVILEGES ON labstocks_db.* TO 'admin'@'localhost' WITH GRANT OPTION;
   exit
```

### Optional step: install and configure phpMyAdmin

If you want to keep an eye on your database and verify if tables are correctly handled by MyLabStocks, you may want to use PhpMyAdmin. 

To install PhpMyAdmin, type the following command:

```sh
   sudo apt-get install phpmyadmin
```
You will probably be prompted twice.
- On the first prompt, select apache2
- On the [second prompt](doc/img/screenshot-phpmyadmin-prompt-for-dbconfig.png), select No.

To configure it, type the following:

```sh
   sudo vi /etc/apache2/sites-available/phpmyadmin.conf
```
And add the following lines to the configuration file:
```txt
      Alias /phpmyadmin "/usr/share/phpmyadmin/"
      <Directory "/usr/share/phpmyadmin/">
      Order allow,deny
      Allow from all
      Require all granted
      </Directory>
```
Don't forget to save the file. Quit the editor and type the following commands:
```sh
   sudo a2ensite phpmyadmin.conf
   sudo systemctl reload apache2
```

Verify phpMyAdmin installation by going to http://localhost/phpmyadmin  
(username and password are those you used in the Database setup step above)


### Install Composer.

MyLabStocks is built on [Laravel](http://www.laravel.com), which is a powerful open-source PHP framework that facilitates the development and maintenance of applications.
MyLabStocks also uses [Voyager](https://voyager.devdojo.com/), which offers an administration backend for BREAD(CRUD) operations, media management, menu builder, and much more.
 
MyLabStocks therefore uses several PHP packages that have a number of dependencies. [Composer](https://getcomposer.org/) will install these packages and all dependencies for you. It will also allow you to update these depencencies safely.

To install Composer, type the following commands.

```sh
   cd /tmp
   curl -sS https://getcomposer.org/installer | php
   sudo mv composer.phar /usr/local/bin/composer
```

### Create your MyLabStocks instance

You need to retrieve the source code of MyLabStocks and place its directory in the web server. 
To do so, type the following command in a terminal (change myloginname by your login name)
```sh
   cd /var/www/
   sudo mkdir mylabstocks
   sudo chown myloginname:www-data mylabstocks/
   sudo chmod 775 mylabstocks/
   git clone https://gitbio.ens-lyon.fr/LBMC/yvertlab/mylabstocks.git
   cd mylabstocks
   git checkout latest-release
   cd ..
   sudo chgrp -R www-data mylabstocks/
   sudo chmod -R 775 mylabstocks/
   cd mylabstocks
   cp .env.example .env
   sudo chgrp www-data .env
   sudo chmod 770 .env
```

Now edit the .env file to enable appropriate connections. Look for the following lines, and replace the 'admin' and 'password' by the values that you entered when setting up the MySQL database. Also replace 'your.server.url' by what it really is, for example its IP address.

```
   APP_URL=http://your.server.url/mylabstocks
   
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=3306
   DB_DATABASE=labstocks_db
   DB_USERNAME=admin
   DB_PASSWORD=password

```

You can now run composer to install all dependencies of MyLabStocks, and install the application itself.

```sh
   cd /var/www/mylabstocks
   composer install
   php artisan voyager:install
   sudo chmod -R 775 storage/logs/
   echo "#" > storage/logs/laravel.log
   sudo chgrp www-data storage/logs/laravel.log
   ./restorefunctionalitytables.sh
   php artisan db:seed
```

We have now to create a new admin user for MyLabStocks. To do this, type the following commands in the terminal :
   
```sh
   php artisan voyager:admin --create
```
And you will be prompted to enter the user name and password that you want to use for this admin account, as well as its contact email.


### Configure your Apache server

MyLabStocks is now installed but you must tell your web server Apache to access it. For this, type the following commands:

```sh
   echo "Alias /mylabstocks /var/www/mylabstocks/public/" > /tmp/lab.conf
   echo "<Directory /var/www/mylabstocks>" >> /tmp/lab.conf
   echo "AllowOverride All" >> /tmp/lab.conf
   echo "</Directory>" >> /tmp/lab.conf
   
   sudo cp /tmp/lab.conf  /etc/apache2/sites-available/mylabstocks.conf
   sudo a2ensite mylabstocks.conf
   sudo a2enmod rewrite
   sudo service apache2 restart
```

### Initialize plasmid maps drawings

Before visualizing any plasmid map, a BLAST database of DNA features needs to be initialized (created). For this, you simply need to:
- connect to your fresh new instance at `http://your.server.url/mylabstocks/`.
- on the left menu of the Back-End, visit the light-blue link `BLAST`.

That's it. Your DNA features database is initiated.

### You're done!

You should now have functional instance. Thank you for installing MyLabStocks, we hope you will enjoy it.

