# MyLabStocks: A web-application to manage molecular biology materials.

# Documentation for Administrators

## Table of Contents <a name="Contents"></a>

**[1. Installation](#Installation)**

**[2. Getting Started](#Gettingstarted)**
  * [2.1 Important definitions: 'admin', 'manager' etc.](#Definitions)
  * [2.2 Before anything else: create a 'manager' user](#Doitfirst)
  * [2.3 Your first steps as a manager](#Managerfirststeps)
  * [2.4 What does BREAD mean?](#Breadbasics)

**[3. Users, roles and permissions](#Usersrolespermissions)**

**[4. Front-End vs. Back-End](#Front-endvsback-end)**

**[5. Stocks tables](#Stockstables)**
  * [5.1 Plasmids](#Plasmids)
  * [5.2 Oligos](#Oligos)
  * [5.3 Strains](#Strains)
  * [5.4 Antibodies](#Antibodies)
  * [5.5 Cell lines](#Celllines)
  * [5.6 Notebooks](#Notebooks)
  * [5.7 Pipets](#Pipets)
  * [5.8 Wellplates](#Wellplates)
  * [5.9 Products](#Products)

**[6. Features tables](#Featurestables)**
  * [6.1 Dna features](#Dnafeatures)
  * [6.2 Events](#Events)
  * [6.3 Passages](#Passages)
  * [6.4 Species](#Species)
  * [6.5 Suppliers](#Suppliers)
  * [6.6 Storage locations](#Storage)
	* [6.6.1 Rooms](#Rooms)
	* [6.6.2 Freezers](#Freezers)
	* [6.6.3 Boxes](#Boxes)
	* [6.6.4 Boxpositions](#Boxpositions)
  * [6.7 Projects](#Projects)

**[7. Media files](#Mediafiles)**

**[8. Plasmid maps and sequences](#Plasmidmapsandsequences)**
  * [8.1 Plasmid maps](#Plasmidmaps)
  * [8.2 Plasmid sequences](#Plasmidsequences)

**[9. BREADs](#Breads)**

**[10. Backups](#Backups)**
  * [10.1 Performing Backups](#Performingbackups)
  * [10.2 Restoring from backups](#Restoringfrombackups)

**[11. Upgrading](#Upgrading)**
  * [11.1 What version of MyLabStocks am I using?](#Whatversion)
  * [11.2 How to upgrade](#Howtoupgrade)

**[12. For a deeper understanding](#Diggingdeeper)**

**[13. You can help](#Helpus)**

----

## Installation <a name="Installation"></a> 

Detailed instructions on how to install your MyLabStocks instance are described here:

[Installation instructions](../INSTALL.md)

*[Back to Contents](#Contents)*

## Getting Started <a name="Gettingstarted"></a> 

### Important definitions: 'admin', 'manager' etc. <a name="Definitions"></a> 

MyLabStocks allows you to define roles and assign them to users. For administrative tasks, the following roles can be used:
* role `admin`: This role provides **full access** on the application.
* role `hypermanager`: This role provides complete access to the application administration, except modifications of the structure of the database.
* role `supermanager`: same as `hypermanager` except that supermanagers can not modify [BREADs](#Breads).
* role `manager`: This is **the safest role for administrative tasks**. With this role, you can perform all common tasks such as editing tables, creating users, assigning them roles, etc. You cannot, however, add new Roles or modify BREADs, Menus or Settings and this will avoid corrupting important functionalities of the application (see below).

*[Back to Contents](#Contents)*

### Before anything else: create a 'manager' user <a name="Doitfirst"></a> 

After completing the installation steps, a single user is created and its role is `admin`. This user therefore has full priviledge on the application functionalities. Using this full access can be convenient but it presents important risks because you can easily make irreversible damage. Even worse, you may make damage that you will not notice immediately, but which will generate difficult issues during future upgrades of your instance. We therefore ***strongly recommend*** that you use another role for your administrative tasks.

For this, go to the login page located at `your.server.address/mylabstocks/admin/login/` and enter the credentials of the admin user (you chose these credentials during installation).
After login, 
- click on the `Users` icon of the main menu on your left.
- click on `Add New`
- choose a user name, email, password for this new user and assign him/her the role **`manager` as the `Default role`**. Leave the `additional roles` field empty.
- click on `Save` (bottom right of the page).

If you want, you can repeat the same procedure to create another user with default role `supermanager` and a third one with default role `hypermanager`. 

Then click in the top right corner to open your user menu and click `Logout`.

*[Back to Contents](#Contents)*

### Your first steps as a manager <a name="Managerfirststeps"></a> 

You can now safely log in to the manager account that you created. After doing so, you have access on the left of the page to the following menu:

![manager menu](/doc/img/screenshot-menu-manager.png) 

- Items in yellow (including those of the submenu "Other Stocks") correspond to different types of `Stocks`. As a manager, you can access and edit these stocks. For more information, please [see below](#Stockstables).
- Items in green (accessible via the submenu "Stock Features") correspond to different types of `Stock features` which, in MyLabStocks, are not actual stocks but information that can be linked to stocks. For example, freezers are stock features: they are not stocks themselves but locations where samples are stored. As a manager, you can access and edit these features. For more information, please [see below](#Featurestables).
- The blue item `BLAST` will direct you to a page that all users can use to search the stocks via a BLAST query sequence. You don't have any administrative tasks to worry about regarding this functionality.
- The pink items refer to Users and their Roles. This is where you can add users with specific permissions on the database. As a manager of the application, it is very important that you understand and use these functionalities. For more information, please [see below](#Usersrolespermissions).
- The grey/white items provide additional administration functionalities. The `Tools` submenu lists items that are not useful unless you want to use high-level functionalities as a `supermanager`, `hypermanager` or `admin` user. For important information on these high-level roles, please [see above](#Definitions). The last, and very useful, item of the menu is a link to the `Media manager`, which allows you to upload and organize media files (images, documents etc.) that can then be linked to items of the database. For more information, please [see below](#Mediafiles)

*[Back to Contents](#Contents)*

### What does BREAD mean? <a name="Breadbasics"></a>

The application is largely based on functionalities called `BREADs`. These letters stand for:
- ***Browse***: What users do when they scroll down tables in search of records.
- ***Read***: Viewing the details of an existing record.
- ***Edit***:  Modifying an existing record.
- ***Add***:  Adding a new record.
- ***Delete***: Deleting a record (for ever!).

The fact that these actions are distinguished allows you to assign specific permissions to users (see below). It also allows developers to build specific functionalities and modules. For example, the functionality called "Update Sequence from .gb file" is specific to the `edit` page of `plasmids`; the dynamic display of plasmid maps is specific to the `read` page of `plasmids`; and so on...

For additional information on BREADs, please [see below](#Breads).

*[Back to Contents](#Contents)*

## Users, roles and permissions <a name="Usersrolespermissions"></a> 

As the manager of your MyLabStocks instance, you can add, edit and delete user accounts. Click on the Users icon of the menu, and you will access a page looking like this:

![manager users](/doc/img/screenshot-users.png) 

Each `user` has a `default role` and `additional roles`. For simplicity, and also to avoid difficulties during upgrades, **we recommend to leave the `additional roles` field empty** for all users.

A `role` is a defined set of `permissions`. To access the list of available roles, click on Roles icon of the menu and you will obtain a page like this:

![manager roles](/doc/img/screenshot-roles.png)

To see the permissions attributed to a role, click the `Edit` action of one of them.

***DO NOT tick or untick the Permissions. Modifying roles can generate difficulties during later upgrades*** so you should only do this if you really know how you will handle your modified roles during later upgrades.

![manager roleexample](/doc/img/screenshot-roleexample-1.png) 

In this example, we see that the `antibodyediter` role does not allow to browse the database, but allows to browse the `Media`. Don't be surprised that it also allows to browse the `Admin` pages: this does not mean that the role provides admin priviledges but only that it gives access to functionalities located in the backend of the application.

If you scroll down you will see further ticks:

![manager roleexample](/doc/img/screenshot-roleexample-2.png) 

indicating that this `antibodyediter` role specifically provides full permissions on the `antibodies` table of the database.

To avoid saving any modification that you might have done, do not click Save but leave the page by clicking on any menu Item (for example the home page at the top of the menu).

You can organize your users and roles in two ways:
* ***Suggested way***:
  * Create an account for each member of the lab and assign it the `viewer` default role. In the name field, enter something explicit such a firstname.name. This has two advantages: Once logged in, the user can browse and read all contents and records, use BLAST etc., with no risk of modifying/damaging any entries. The second advantage is that the name of the user will then appear in the list of possible 'Authors' of items, which is important to track who constructed a given plasmid, who ordered a given oligo etc.
  * When a lab member leaves the laboratory, keep the account but assign it the default role `blocked`. This way, the name remains in the list of `Authors` but access will not be possible any more.
  * Create generic users that can add or edit a single specific table. For example, create a user with the default role `plasmidediter` and a functional name such as "Plasmid.Editor". This user will be able to enter a new plasmid or modify existing plasmids, but he/she will not have access to  any other items such as browsing antibodies or oligos. This will be annoying enough to force your colleagues to log out after they completed their editing, and log back in with their viewer role. This way, the window of time during which errors may be done is reduced. You will need to give the credentials of these "generic users" to your colleagues. Make sure you regularly change the passwords and inform your colleagues about it.


* Alternative way ***(not recommmended)***:
  * Create an account for each member of the lab and give him/her a default role that you consider relevant. In the name field, enter something explicit such as firstname.name. 
  * When a lab member leaves the laboratory, keep the account but assign it the default role `blocked`. This way, the name remains in the list of `Authors` but access will not be possible any more.
  * This way can potentially allow to register logs of what happened to the database (who did what and when), because every person uses a single account. However, we do not recommend it because of two disadvantages: it opens permanent time for users to make errors by deleting or modifying records, and you will probably want to define roles that do not already exist. Given that adding/modifying roles present some risks during upgrades, this may complicate your administration tasks on the long term.
 
*[Back to Contents](#Contents)*

## Front-End vs. Back-End <a name="Front-endvsback-end"></a> 

An important thing to understand when administering the application is the distinction between its Front-End and its Back-End. Nearly all web-applications offer two interfaces: the front-end is for users, the back-end for administrators. MyLabStocks also offers two interfaces but the distinction is slightly more subtle:

- The ***Front-End*** is where users can rapidly browse **some but not all**, of the stocks. The Front-End offers convenient filters to search for specific items and information. Views are organized to be synthetic and rather compact. In other words, the Front-End is for everyday usage, for everyone. The address of the Front-End interface is typically `url-of-your-server/mylabstocks/`.
- The ***Back-End*** is both for the administrator(s) and the users. The administrator can perform all admin tasks such as modifying any table, add users, assign permissions etc. The users can also visit the Back-End to have access to advanced features of MyLabStocks: in the Back-End, they can browse the entire database, provided that they have the necessary permissions. They can also add and edit records, which is not possible from the Front-End. The address of the backend is typically `url-of-your-server/mylabstocks/admin/`.

So, keep in mind that it is normal usage of the application to navigate back and forth between the Front-End and the Back-End.

*[Back to Contents](#Contents)*

## Stocks tables <a name="Stockstables"></a> 

Stocks are the items listed in yellow in the main menu:

![menu-stocks](/doc/img/screenshot-menu-stocks.png)

They correspond to different types of stocks. Each type is stored in a specific table. These tables are described below.

*[Back to Contents](#Contents)*

### Plasmids <a name="Plasmids"></a>

| Field name  | Type              | What you enter in this field                                            |
| ----------  | ----              | ----------------------------                                            |
| Id          | Integer           | A unique integer                                                        |
| Name        | Character string  | Plasmid's name (free text)                                              |
| Author      | Character string  | The lab member who obtained the plasmid. Dropdown selection made from the `name` field of the `users` table. |
| Other names | Character string  | Plasmid's alternative names (free text)                                 |
| Seqfile     | File upload       | Uploaded file containing the plasmid's sequence. It is **very important that this file is in GENBANK format**. The file name must have the `.gb` extension |
| Type        | Character string  | Selection from dropdown menu                                            |
| Bacterial Selection | Character string  | The selection marker to use when amplifying your plasmid in bacteria (selection from dropdown menu) |
| Parent vector | Character string  | The id of the parent plasmid, if any. (free text)                     |
| Species     | Character string  | The species where this plasmid was designed to be used. Dropdown selection made from the `name` field of the `species` table |
| Sequence    | Character string  | The plasmid's DNA sequence. If a seqfile is uploaded, this field can be automatically filled and updated from the sequence contained in the file (see below)|
| Description | Character string  | A description of the plasmid (free text)                                |
| Obtention Date | Date           | Date when the plasmid was obtained                                      |
| Checkings   | Character string  | Verifications that were made on this plasmid such as restriction maps, resequencing etc. (free text) |
| Related oligos  | Character string | Name or IDs of oligos related to this plasmid, if any. (free text)   |
| Related strains | Character string | Name or IDs of strains related to this plasmid, if any. (free text)  |
| Files       | Media files       | Uploaded media files of any format that you want to be associated with the plasmid |
| Created At  | Timestamp         | Timestamp of the record in the database                                 |
| Marker 1    | Character string  | Selection marker to use in the target species                           |
| Marker 2    | Character string  | Secondary selection marker to use in the target species                 |
| Parent vector name | Character string | The name of the parent plasmid, if any. (free text). For compatibility with v1. Redundant with field parent-vector and should be changed in the future. |
| Insert      | Character string  | An optional name for the insert, if any. (free text)                    |
| Insert type | Character string  | Type of insert, such as restriction fragment, PCR etc. (free text, optional) |
| Projects    | Relationship      | Links to projects related to this plasmid. Dropdown selection made from the `name` field of the `projects` table |

*[Back to Contents](#Contents)*

### Oligos <a name="Oligos"></a>

***Tip: We found it very useful to impose the following policy to all lab members: "An oligo will be ordered only if it is first entered in the database".*** 

| Field name  | Type              | What you enter in this field                                            |
| ----------  | ----              | ----------------------------                                            |
| Id          | Integer           | A unique integer                                                        |
| Name        | Character string  | Oligo's name (free text). Must be unique                              |
| Sequence    | Character string  | The DNA sequence of the oligo, always **5-prime to 3-prime** !          |
| Description | Character string  | A description of the oligo (free text)                                  |
| Author      | Character string  | The lab member who obtained the oligo. Dropdown selection made from the `name` field of the `users` table |
| Pcr conditions predicted | Character string  | Optional free text describing expected PCR conditions      |
| Supplier    | Character string  | The company or service who supplied the oligo. Dropdown selection made from the `name` field of the `suppliers` table |
| Created At  | Timestamp         | Timestamp of the record in the database                                 |
| Obtention Date | Date           | Date when the oligo was obtained                                        |
| Purif       | Character string  | The purification method applied to the oligo when ordering/obtaining it. Dropdown selection made from the `name` field of the `purifs` table |
| Files       | Media files       | Uploaded media files of any format that you want to be associated with the oligo |
| Projects    | Relationship      | Links to projects related to this oligo. Dropdown selection made from the `name` field of the `projects` table |

In the browse view of oligos, you will see a button action named `CopySeq`. It is located next to a text field containing the sequence of the oligo:

![oligo-copyseq](/doc/img/screenshot-oligo-copyseq.png)

The text field does not display the entire sequence, so don't be surprised if it looks truncated.
Clicking the `CopySeq` button copies the full oligo sequence to your clipboard. This is convenient when you need to copy/paste the sequence into other web pages or applications.

**Users can retrieve oligos using a query sequence.** For this, go to the BLAST item of the menu, select `oligos` in response to 'what do you want to search' and enter a query sequence in the text box. By default, the page expects your query to be nucleotidic and the program is `blastn`. If your query sequence is a peptide, you can select the program and task `tblastn`.

*[Back to Contents](#Contents)*

### Strains <a name="Strains"></a>


| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Name                      | Character string  | Strain's name (free text)                                               |
| Species                   | Character string  | Strain's species. Dropdown selection made from the `name` field of the `species` table.                                        |
| Other names               | Character string  | Strain's other name. Optional.                                          |
| General background        | Character string  | Genetic background of the strain. Dropdown selection made from the `name` field of the `backgrounds` table.                    |
| Comments                  | Character string  | A description of the strain (free text)                                 |
| Mating type               | Character string  | Strain's mating type. Dropdown selection made from the `alleles` field of the `st_mats` table.                                 |
| ade2                      | Character string  | Modifications at *ADE2* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_ade2s` table.    |
| his3                      | Character string  | Modifications at *HIS3* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_his3s` table.    |
| leu2                      | Character string  | Modifications at *LEU2* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_leu2s` table.    |
| met15                     | Character string  | Modifications at *MET15* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_met15s` table.  |
| trp1                      | Character string  | Modifications at *TRP1* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_trp1s` table.    |
| ura3                      | Character string  | Modifications at *URA3* locus. Leave empty if none. Dropdown selection made from the `alleles` field of the `st_ura3s` table.    |
| ho                        | Character string  | Modifications at *HO* locus. Leave empty if none. Freetext                |
| locus1                    | Character string  | Modifications at a locus not mentionned in other fields. Free text      |
| locus2                    | Character string  | Modifications at a locus not mentionned in other fields. Free text      |
| locus3                    | Character string  | Modifications at a locus not mentionned in other fields. Free text      |
| locus4                    | Character string  | Modifications at a locus not mentionned in other fields. Free text      |
| locus5                    | Character string  | Modifications at a locus not mentionned in other fields. Free text      |
| Parental strain           | Character string  | The id of the strain from which this one was obtained. Free text        |
| Obtained by               | Character string  | Method used to obtain the strain. Free text                             |
| Checkings                 | Character string  | Checkings performed on this strain such as PCR, microscopy, phenotyping...           |
| Extrachromosomal plasmid  | Character string  | Any plasmid present in this strain                                       |
| Cytoplasmic character     | Character string  | Known cytoplasmic characters of the stran, such as PSI+ prion or *rho0* mitochondrial DNA loss. Free text.                       |
| Reference                 | Character string  | Litterature reference related to the strain, free text.                            |
| Author                    | Character string  | Person (lab member) authoring this strain. Dropdown selection made from the `name` field of the `users` table.                             |
| Relevant plasmids         | Character string  | Plasmids related to the design of the strain. Free text                 |
| Relevant oligos           | Character string  | Oligos related to the design of the strain. Free text                   |
| Files                     | Media files       | Uploaded media files of any format that you want to be associated with the strain                                             |
| Created at                | Timestamp         | Timestamp of the record in the database                                 |
| Updated at                | Timestamp         | Timestamp of updates                                                    |
| Warning                   | Boolean           | Option used to signal an issue with the strain                          |


*[Back to Contents](#Contents)*

### Antibodies <a name="Antibodies"></a>


| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Antigen                   | Character string  | Antibody's antigen (free text)                                          |
| Host                      | Character string  | Host of the antibody. Dropdown selection made from the `name` field of the `hosts` table.                                    |
| Supplier                  | Character string  | The company or service who supplied the oligo. Dropdown selection made from the `name` field of the `suppliers` table        |
| Type                      | Character string  | Type of antibody. Dropdown selection (Monoclonal, Polyclonal) defined in the BREAD of this table.           |
| Productid                 | Character string  | Supplier's reference of the antibody (free text)                        |
| Batchid                   | Character string  | Production batch reference (free text)                                  |
| Instock                   | Character string  | Stock status of the antibody. Dropdown selection made from the `name` field of the `availabilities` table.                              |
| Ordered By                | Character string  | Person who ordered this antibody. Dropdown selection made from the `name` field of the `users` table.                        |
| Comments                  | Character string  | A description or comment about the antibody (free text)                 |
| Created At                | Timestamp         | Timestamp of the record in the database                                 |
| Obtention Date            | Timestamp         | Date when the antibody was obtained                                     |
| projects                  | Relationship      | Links to projects related to this antibody. Dropdown selection made from the `name` field of the `projects` table            |
| Files                     | Media files       | Uploaded media files of any format that you want to be associated with the antibody.                                         |


*[Back to Contents](#Contents)*

### Cell lines <a name="Celllines"></a>


| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Name                      | Character string  | Cell line's name (free text)                                            |
| Celltype                  | Character string  | Type of cells. Dropdown selection made from the `name` field of the `cell_types` table.                                     |
| Description               | Character string  | A description or comments about the cell line (free text)                                                                   |
| Origin                    | Character string  | Source from which the cell line was obtained (free text)                |
| Author                    | Character string  | Person authoring this cell line. Dropdown selection made from the `name` field of the `users` table.                        |
| Files                     | Media files       | Uploaded media files of any format that you want to be associated with the cell line.                                       |
| Projects                  | Relationship      | Links to projects related to this cell line. Dropdown selection made from the `name` field of the `projects` table          |
| Created at                | Timestamp         | Timestamp of the record in the database                                 |
| Updated at                | Timestamp         | Timestamp of updates                                                    |



*[Back to Contents](#Contents)*

### Notebooks <a name="Notebooks"></a>


| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Serial number             | Character string  | Reference number of the notebook. (free text)                                               |
| Author                    | Character string  | Author of the notebook. Dropdown selection made from the `name` field of the `users` table.                                 |
| Begin Date                | Date              | Date at which the notebook was started                                  |
| End Date                  | Date              | Date at which the notebook was ended                                    |
| Created at                | Timestamp         | Timestamp of the record in the database                                 |
| Updated at                | Timestamp         | Timestamp of updates                                                    |



*[Back to Contents](#Contents)*

### Pipets <a name="Pipets"></a>



| Field name                | Type              | What you enter in this field                                            |  
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Serial number             | Character string  | Reference number of the pipet. (free text)                              |
| Brand                     | Character string  | Pipet's brand. Dropdown selection made from the `name` field of the `suppliers` table.                                       |
| Type                      | Character string  | Pipet's type such as volume of number of channels. Dropdown selection made from the `name` field of the `pipet_types` table. |
| Files                     | Media Files       | Uploaded media files of any format that you want to be associated with the pipet.                                            |
| History                   | Character string  | Information on history of the pipet                                     |
| Created at                | Timestamp         | Timestamp of the record in the database                                 |
| Updated at                | Timestamp         | Timestamp of updates                                                    |




*[Back to Contents](#Contents)*

### Well plates <a name="Wellplates"></a>



| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Format                    | Character string  | Plate's format (free text)                                              |
| Name                      | Character string  | Plate's name (free text)                                                |
| Author                    | Character string  | Person authoring this wellplate. Dropdown selection made from the `name` field of the `users` table.                        |
| Content Type              | Character string  | Plate's content (free text)                                             |
| Thawings                  | Integer           | Number of times the plate was thawed                                    |
| Freeze Date               | Date              | Date at which the plate was frozen                                      |
| Files                     | Media Files       | Uploaded media files of any format that you want to be associated with the plate.                                           |
| Freezer Id                | Integer           | Freezer in which the plate is kept                                      |
| Comments                  | Character string  | Comments or description about the weelplate (free text)                 |
| Warning                   | Boolean           | Option used to signal an issue with the plate                           |
| Created at                | Timestamp         | Timestamp of the record in the database                                 |
| Updated at                | Timestamp         | Timestamp of updates                                                    |




*[Back to Contents](#Contents)*

### Products <a name="Products"></a>

In MyLabStocks, a `Product` is anything that was purchased by the lab and is not referenced in the dedicated tables strains, oligos, plasmids, antibodies, cell lines, pipets, etc. For example, a chemical reagent, a microtube or a biochemical kit are all "Products".

Note that the software is **NOT** appropriate to track your orderings. This table is provided to help you keep useful information on past orders, such as the reference of a product, or how to contact a company, and share this information among lab members.

| Field name  | Type              | What you enter in this field  |
| ----------  | ----              | ----------------------------  |
| Id          | Integer           | A unique integer |
| Name        | Character string  | Product's name (free text) |
| Type        | Character string  | The type of product (dropdown menu defined in the BREAD) |
| Supplier    | Character string  | The name of the company who sold you the product (linked to table Suppliers) |
| Supplier's reference    | Character string  | The product ID as referenced by the supplier |
| Producer    | Character string  | The name of the company who produced the product (may differ from the Supplier) (free text) |
| Producer's reference    | Character string  | The product ID as referenced by the producer |
| Web link    | Character string  | A web page describing the product |
| Price    | Character string  | The price you paid last time |
| Last order date    | Date  | Date of last ordering |
| Ordered by    | Character string  | The lab member who placed the last order |
| Contact for quote    | Character string  | Any information on the commercial contact from the supplier (name, email ...)|
| Code Nacre    | Character string  | *For French academic labs Only* The nomenclature NACRE code needed by your administration when placing the order |
| Files    | media  | Any media you may want to upload regarding the product (e.g. a quote, a manual...) |
| Comments    | Character string  | Free text giving comments on the product |
| Created At  | Timestamp         | Timestamp of the record creation in the database |
| Created At  | Timestamp         | Timestamp of the record update in the database   |

*[Back to Contents](#Contents)*

## Features tables <a name="Featurestables"></a> 

Stock features are the items listed in green in the main menu:

![menu-features](/doc/img/screenshot-menu-features.png)

They correspond to pieces of information that are linked to stocks. Each type of stock feature is stored in a specific table. These tables are described below.

*[Back to Contents](#Contents)*

### Dna features  <a name="Dnafeatures"></a> 

| Field name  | Type              | What you enter in this field  |
| ----------  | ----              | ----------------------------  |
| Id          | Integer           | A unique integer |
| Sequence    | Character string  | The nucleotide sequence of the feature |
| Name        | Character string  | Feature's name (free text) |
| Comments    | Character string  | Free text giving comments on the feature |
| Created At  | Timestamp         | Timestamp of the record creation in the database |
| Updated At  | Timestamp         | Timestamp of the record update in the database   |
| Color       | Character string  | The color with which the feature will be displayed on plasmid maps. Do **NOT** edit directly because your color name or syntax may not be recognized properly. Instead, select one of those proposed by the dropdown menu |

After adding or editing DNA features entries, your **modifications will NOT directly appear on the plasmid maps** unless you intentionally **update plasmid maps with the latest features**. Technically, this corresponds to updating the BLAST database containing the DNA features sequences. To achieve this update, go to the `browse` page of the `DNA features` and click on the blue button (top right) labeled `Update Plasmid Maps with these Features`.

Now your latest changes should appear on the [plasmid maps](#Plasmidmaps).

*[Back to Contents](#Contents)*

### Events <a name="Events"></a> 


| Field name                | Type              | What you enter in this field                             |
| ----------                | ----              | ----------------------------                             |
| Id                        | Integer           | A unique integer                                         |
| Datatype                  | Character string  | Type of data                                             |
| Event date                | Date              | Date of the event                                        |
| Event type                | Character string  | Type of event                                            |
| Description               | Character string  | A description of the event. (free text)                  |
| Files                     | Media             | Any media you may want to upload regarding the event     |
| Created At                | Timestamp         | Timestamp of the record creation in the database         |
| Updated At                | Timestamp         | Timestamp of the record update in the database           |



*[Back to Contents](#Contents)*

### Passages <a name="Passages"></a> 

| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Cell line                 | Character string  | The cell line this passage is about                                     |
| Name                      | Character string  | Passage's name (free text)                                              |
| Author                    | Character string  | Author of the passage. Dropdown selection made from the `name` field of the `users` table.                                 |
| Freezing Date             | Timestamp         | Date when the passage was frozen                                        |
| Created At                | Timestamp         | Timestamp of the record creation in the database                        |
| Updated At                | Timestamp         | Timestamp of the record update in the database                          |


*[Back to Contents](#Contents)*

### Species <a name="Species"></a> 

| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Name                      | Character string  | Specie's name (free text). Must be unique, will appear in dropdown selections.                          |
| Created At                | Timestamp         | Timestamp of the record creation in the database                        |
| Updated At                | Timestamp         | Timestamp of the record update in the database                          |


*[Back to Contents](#Contents)*

### Suppliers <a name="Suppliers"></a> 

| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Name                      | Character string  | Supplier's name (free text). Must be unique, will appear in dropdown selections.                        |
| Created At                | Timestamp         | Timestamp of the record creation in the database                        |
| Updated At                | Timestamp         | Timestamp of the record update in the database                          |

*[Back to Contents](#Contents)*

### Storage locations <a name="Storage"></a> 

The tables below are dedicated to the physical locations where your stocks are stored.

#### Rooms <a name="Rooms"></a> 

As its name indicates, the rooms where you store things (eg. where your freezers are).

| Field name | Type              | What you enter in this field                                                 |
| ---------- | ----              | ----------------------------                                                 |
| Id         | Integer           | A unique integer                                                             |
| Name       | Character string  | Room's name (free text, must be unique, will be displayed in dropdown menus) |
| Building   | Character string  | Name of the building where the room is (free text)                           |
| Floor      | Character string  | Building's floor where the room is (free text)                               |

*[Back to Contents](#Contents)*

#### Freezers <a name="Freezers"></a> 

As its name indicates, the freezers where you store things.
 
| Field name | Type              | What you enter in this field                                                    |
| ---------- | ----              | ----------------------------                                                    |
| Id         | Integer           | A unique integer                                                                |
| Name       | Character string  | Freezer's name (free text, must be unique, will be displayed in dropdown menus) |
| Type       | Character string  | Selection from dropdown menu                                                    |
| Room       | Character string  | The room where the freezer is (selection from dropdown menu)                    |

*[Back to Contents](#Contents)*

#### Boxes <a name="Boxes"></a> 

As its name indicates, the boxes where you store cryotubes.
 
| Field name       | Type              | What you enter in this field                                         |
| ----------       | ----              | ----------------------------                                         |
| Id               | Integer           | A unique integer                                                     |
| Nrows            | Integer           | The number of rows in the box                                        |
| Ncols            | Integer           | The number of columns in the box                                     |
| Name             | Character string  | An optional name for this box                                        |
| Freezer          | Character string  | The freezer where this box is located (selection from dropdown menu) |
| Rack Nb          | Integer           | An optional index indicating in which rack the box is                |
| Position in rack | Integer           | An optional index indicating the position of the box in the rack     |

*[Back to Contents](#Contents)*

#### Boxpositions <a name="Boxpositions"></a> 

Boxpositions are all physical positions where a cryotube can be located. You do not have direct access to this table because you don't need to. The boxpositions table is automatically updated when a box is added, edited or deleted.

The interest of this table is to provide dropdown menus where users can select positions for a given sample. For example, a passage can be linked to one or more boxpositions, corresponding to where all samples (cryotubes) of this passage are stored.

For your information, boxpositions are referenced by the id of the box, and by the row and column indexes in the box.

*[Back to Contents](#Contents)*

### Projects <a name="Projects"></a> 

| Field name                | Type              | What you enter in this field                                            |
| ----------                | ----              | ----------------------------                                            |
| Id                        | Integer           | A unique integer                                                        |
| Name                      | Character string  | Project's name (free text). Must be unique, will appear in dropdown selections.                              |
| Leader                    | Character string  | Leader of the project. Dropdown selection made from the `name` field of the `users` table.                   |
| Files                     | Media             | Any media you may want to upload regarding the event                    |
| Status                    | Character string  | Project's status.                                                       |
| Description               | Character string  | A description of the project. (free text)                               |
| Created At                | Timestamp         | Timestamp of the record creation in the database                        |
| Updated At                | Timestamp         | Timestamp of the record update in the database                          |


*[Back to Contents](#Contents)*

## Media files <a name="Mediafiles"></a> 

With MyLabStocks, you can store files of any format (images, articles, notes, DNA sequence files, videos, etc.) and associate them to any record of your database. For example, if you store a `.ab1` file corresponding to the a resequencing of one of your plasmids, you can upload it in your instance and link it to the plasmid. This way, any user can access this file and use it in the future. This can be very helpful on the long term.

To organize these media files, MyLabStocks comes with a `Media Manager` functionality, which is powered by the [Voyager](https://voyager-docs.devdojo.com/) package. After log in with your manager account, clicking the Media icon of the left menu will lead you to a graphic interface that allows you to manage your media files.

![media](/doc/img/screenshot-media.png) 

You can upload files, delete them (for ever, be careful!), create subfolders and move your files in them. You can also crop images (click on the icon of an image file and then click the `Crop` tool). These functionalities are straightforward to use.
***Warning: If you move a file, its previous link to any record will be broken.***
For more information on the Media Manager, please see the dedicated [Voyager's documentation](https://voyager-docs.devdojo.com/core-concepts/media-manager) or the [Voyager's video tutorial](https://voyager.devdojo.com/academy/media-manager/).

Note that users do not need administrative permissions to upload files and link them directly by editing records. For example, a user who enters a new antibody in the database can upload media files from the add/edit page of the antibody, and the files will be automatically linked to this antibody.

*[Back to Contents](#Contents)*

## Plasmid maps and sequences <a name="Plasmidmapsandsequences"></a>

### Plasmid maps <a name="Plasmidmaps"></a> 

An important feature of MyLabStocks is its dynamic display of plasmid maps. Every time a user visits the read view of a plasmid, a number of tasks are run in the background before rendering the view:
* The plasmid sequence is used as a query in a BLAST search for `Dna features`.
* The results of the BLAST search are passed to [cirdna](http://emboss.sourceforge.net/apps/cvs/emboss/apps/cirdna.html) which produces an image of the map as a pdf file.
* The pdf file is converted to svg.
* The image is then loaded in the view.

![plasmidmap](/doc/img/screenshot-plasmidmap.png) 

Given this implementation, you should keep in mind the following:

* The features displayed on the map have high homology with the corresponding regions of the plamid, but their **sequence may slightly differ from the plasmid sequence**. This is intentional: MyLabStocks helps you to see what your plasmids look like but it is not a software to analyze their sequence. There are other powerful solutions that you should use or this. For example, [UGENE](https://ugene.net) is very complete, open source, Linux-friendly and well-documented.
* The DNA features that are displayed are those present in the `Dna Features` table of your MyLabStocks instance. For information about how to update the list of these features and how to choose their color, please [see above](#Dnafeatures).
* The map is freshly-made at every visit. If two users visit the read view of the same plasmid exactly at the same time, they may experience a problem. We consider that this will almost never happen at the scale of a single laboratory, but if you consider installing a MyLabStocks instance for a very large community such as an entire institute, a consortium or a whole campus, you may consider this as a potential issue.
* Images of maps are stored in directory `storage/app/public/tmp/plasmidmaps/` of your instance. You might want to clean this directory regularly if you want to save disk space.

*[Back to Contents](#Contents)*

### Plasmid sequences <a name="Plasmidsequences"></a> 

The sequence of a plasmid is stored in the field `sequence` of the `plasmids` table. It can be entered and modified in two ways:

- By directly editing the sequence field. This is **NOT** recommended as it may generate errors.

- By automatically importing the sequence from a `.gb` file (GenBank file format). Note that this will overwrite any existing sequence previously stored in the sequence field.

In the top right corner of the edit page of plasmids, a button invites you to update the plasmid sequence using its `.gb` file:
![plasmid-updateseqfromfile](/doc/img/screenshot-plasmid-updateseqfromfile.png)

A file must be uploaded before you can use this functionality:
- Enter a `.gb` file in the `Seqfile` field (must be in GENBANK format).
- Click `Save`
- Come back to the `Edit` page of the plasmid
- Click `Update sequence from .gb file`.
- The `Sequence` field of the plasmid now contains the sequence contained in the provided `.gb` file.

**Users can retrieve plasmids using a query sequence.** For this, go to the BLAST item of the menu, select `plasmids` in response to 'what do you want to search' and enter a query sequence in the text box. By default, the page expects your query to be nucleotidic and the program is `blastn`. If your query sequence is a peptide, you can select the program and task `tblastn`.

*[Back to Contents](#Contents)*

## BREADs <a name="Breads"></a> 

As [briefly mentioned above](#Breadbasics), BREADs are the functionalities enabling users to interact with the database. The way BREADs are defined and organized is written in special tables of the database (tables `data_types`, `data_rows` and `permissions`). The application reads these tables to offer specific functionalities on specific records, and according to the specific permissions defined in the user's role.

There is one BREAD per `Stock` table or `Stock feature` table.

If you log in as a `hypermanager` or `administrator` ([see definitions above](#Definitions)), you can access and edit BREADs. However, we recommend that you ***do not modify BREADs*** unless you really know what you are doing. Modifying BREADs will have the following consequences:
* Functionality changes. This is expected because BREADs define functionalities. For example, one can modify the plasmids' BREAD so that the name of plasmids is not displayed anymore in the read view. This allows you to personalize your instance, but it also implies that you will experience undesired changes if you enter modifications by mistake.
* Your BREADs may not be compatible with future releases of the application. We (developers) create and modify BREADs together with the code of the application. During upgrades, new BREADs are loaded in the database. If you changed your instance's BREADs before running an upgrade, you may experience problems. At the best, your BREADs will be overwritten by the new ones and you will lose your modifications; but more complex difficulties may occur and functionalities may become corrupted.

*[Back to Contents](#Contents)*

## Backups <a name="Backups"></a> 

It is very important to realize that there are several types of precious things in MyLabStocks:
- The ***database***. This is handled by a database server, such as MySQL. It is made of tables in which you and your lab mates have stored data. Some tables also contain informations that are important for the functionalities of MyLabStocks, such as permissions or the content of menus. This data is specific to your lab. So, as an administrator of your lab instance of MyLabStocks it is ***VERY important*** that you backup the database regularly.
- The ***files***. Wherever you installed mylabstocks, a number of non-code files are stored. For example, users of your lab have most likely uploaded in MyLabStocks the sequence files of their plamsids, or images, or any other types of media. These files, and the path to them, are also specific to your lab. It is therefore also ***VERY important*** to back up media files.
- The ***code***. This is a series of scripts (in PHP, html, etc...) that are strictly organized in files and directories. These files are kept safe in the git repository of MyLabStocks, so you should not worry too much about backing them up because you will most likely be able to re-download them from a remote server. Address of the source repository is indicated in the `git clone` instruction of the installation procedure.

Below are some recommendations on how to backup your instance. We provide them for MySQL. If you use another database server, please adapt accordingly.

*[Back to Contents](#Contents)*

### Performing Backups <a name="Performingbackups"></a>

All the steps below should be run at the root of your installation. Unless you installed your instance at a specific location, type:
```sh
cd /var/www/mylabstocks
```

Bring down the application so that no user can make any change to the database:
```sh
php artisan down
```

In the following command, change `admin` by the administrator login you provided when constructing the database (see the `.env` file or the mysql instructions in the installation guide) and, if your database name is not `labstocks_db` then change this name too, then type it in order to backup the database (provide the password when prompted):
```sh
mysqldump --host=localhost --user=admin -p --single-transaction --no-tablespaces labstocks_db > storage/backups/labstocks_db_dump.sql
```

Backup the filesystem, which now contains the backup of the database.
```sh
cd ..
tar -cz --exclude-ignore="exclude-from-backups.list" -f /tmp/mylabstocks_backup.tgz mylabstocks/
```

Bring up the application to live mode:
```sh
cd mylabstocks
php artisan up
```

Then copy the archive file `mylabstocks_backup.tgz` to a safe place.

*[Back to Contents](#Contents)*
 
### Restoring from backups <a name="Restoringfrombackups"></a>

We assume here that you are in the following situation:
- You have a corrupted instance of MyLabStocks located on `/var/www/mylabstocks`.
- Your server has all the necessary programs installed (prerequisites, Apache configuration, composer).
- The MySQL database of your instance exists, it is named `labstocks_db` and its connection is correctly instructed in the `.env` file.

If needed, see the [installation instructions](../INSTALL.md) regarding all this.

- We also assume that you have a backup archive file named `mylabstocks_backup.tgz` that contains a previous, intact, instance of your application and that this file was produced from the backup instructions above. Place this file in the `/tmp/` directory.

Run the following steps ***CAREFULLY***:

Rename your old (corrupted) instance and database so that you keep the `.env` file and the possibility to go back to any old content in case something goes wrong:
```sh
cd /var/www/
sudo mv mylabstocks mylabstocks_old
mysqldump --host=localhost --user=admin -p  --single-transaction --no-tablespaces labstocks_db > /tmp/labstocks_old_db_dump.sql
sudo mv /tmp/labstocks_old_db_dump.sql mylabstocks_old/storage/backups/
```

Unzip the backup archive, place it where it needs to be and set appropriate permissions (change `yourlogin` by whatever your login is):
```sh
cd ~
cp /tmp/mylabstocks_backup.tgz .
tar -xzf mylabstocks_backup.tgz
sudo mv mylabstocks /var/www/
cd /var/www/
sudo chown yourlogin:www-data mylabstocks/
sudo chgrp -R www-data mylabstocks/
sudo chmod -R 775 mylabstocks/
```

Restore the database:
```sh
cd /var/www/mylabstocks
mysql --host=localhost --user=admin -p --database=labstocks_db < storage/backups/labstocks_db_dump.sql
```

Restore the `.env` file and its permissions (replace `yourlogin` by what it really is):
```sh
cd /var/www/mylabstocks
sudo cp ../mylabstocks_old/.env .
sudo chown yourlogin:www-data .env
sudo chmod 770 .env
```

Bring up the instance to live mode:
```sh
php artisan up
```

You should now have restored your instance.

*[Back to Contents](#Contents)*

## Upgrading <a name="Upgrading"></a> 

### What version of MyLabStocks am I using? <a name="Whatversion"></a>

To know which version of MyLabStocks you are currently using, go to the root of the application:
```sh
cd /var/www/mylabstocks
```
and type:
```sh
git log --decorate=short --oneline | head -1
```
This will display the commitID of your version, and any tag associated to it. For example:
```txt
44ae5e2 (HEAD -> latest-beta, tag: v2.0.4-beta) bugfix: remove reference to deprecated column in seeder for dna-features
```
In this example, version is `v2.0.4-beta` because:
- `HEAD ->` indicates that information on this line corresponds to your version
- `latest-beta` is the branch on which your local git environment currently is
- `44ae5e2` is the commitID of your version. This identifier is unique in all the history of MyLabStocks development.
- `tag:` indicates the name of the version. If this does not appear, it means that your version is not a referenced tag, which is not necessarily a problem.
- `bugfix: remove...` is some text commenting the modifications made at this commit.

*[Back to Contents](#Contents)*

### How to upgrade your instance <a name="Howtoupgrade"></a>

Detailed instructions on how to upgrade your MyLabStocks instance from one release to another are provided here: [Upgrading instructions](upgrading/README.md)


*[Back to Contents](#Contents)*

## For a deeper understanding <a name="Diggingdeeper"></a> 

If you'd like to understand in more details how MyLabStocks works you can read the [documentation for developers](../dev/README.md).

Keep in mind that:
* MyLabStocks is a [Laravel](https://laravel.com/) application.
* Most of MyLabStocks functionalities are based on [Voyager](https://voyager.devdojo.com/), which offers many tools to modify the application. If you choose to use these tools (such as changing BREADs or Menus) you should do this very carefully and only if you really know what you are doing.
* For any security issue, please contact Gael Yvert directly. You will find his address in the original open-access [MyLabStocks publication](https://doi.org/10.1002/yea.3008).

*[Back to Contents](#Contents)*

## Found the application useful? Help us and others! <a name="Helpus"></a>

We are happy to distribute this code freely and openly. If you find MyLabStocks useful, please **say it on social media** and don't forget to cite us (or acknowledge us) in your papers and communications. Please cite the original paper:

[Chuffart F. and Yvert G. MyLabStocks: a web-application to manage molecular-biology materials. Yeast (2014) 31:179-185](https://doi.org/10.1002/yea.3008).

You can also help by contributing to the code of MyLabStocks. If you're interested, feel free to contact Gael Yvert (address is in the original publication).

For any security issue, please contact Gael Yvert directly.

*[Back to Contents](#Contents)*

