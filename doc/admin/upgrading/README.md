# MyLabStocks: A web-application to manage molecular biology materials.

# Upgrading instructions<a name="Top"></a>

1. [From v2.0.1 or later](#Fromv201orlater)

2. [From v1.0.0 to v2.0.1](#Fromv100tov201)


## From v2.0.1 or later to latest-release  <a name="Fromv201orlater"></a>

***Pre-requisites***

As of version 2.1.2, MyLabStocks requires PHP 8.1.0 or greater and Composer 2.2.0 or later. To verify the versions installed on your system, you can type:

```sh
composer --version
```

***Go into application directory***

All the steps below should be run at the root of your installation. Unless you installed your instance at a specific location, type:

```sh
cd /var/www/mylabstocks
```

***Step 1: Bring down the application in maintenance mode***

We assume that you sent a warning to your colleagues (users) telling them you will upgrade the instance today and that the application will not be available during the procedure. It is time to do it, so the first thing is to block users' access to the application. Type the following command to stop the application:
```sh
php artisan down
```

***Step 2: Backup everything***

Before running any upgrade procedure, it is important to backup your entire instance in case something goes wrong. For guidelines on how to backup, please refer to the [Administrators manual](../README.md) .

***Step 3: Upgrade the application code***

See if you have any untracked changes in your code files (this can happen if you made some configurations specific to your instance)
```sh
git status
```
If yes, then commit these changes to a new branch that you can later retrieve. For example:
```sh
git checkout -b oldprod
git add *
git commit -m "add untracked files of prod instance"
```
Then pull the upgraded code:

```sh
git checkout latest-release
git pull
```
***Step 4: Update packages and dependencies***

```sh
composer install
```

***Step 5: Update database structure by running migrations***

```sh
php artisan migrate
```

***Step 6: Seed functionality tables***

Currently, Voyager has a limitation regarding deployment of BREAD functionalities. The developers are working on this and things should be easier after the v2.0 release of Voyager. Before this is implemented, you need to use the following procedure to update the functionality tables.

Make a backup of the users and roles table (these copies will not have foreign keys), and empty the actual users table. For this, type the following (change `admin` by the login you set when installing the database):
```sh
mysql -u admin -p
```
Enter the password when prompted. This will bring you in a SQL shell.
Once there type:
```sql
mysql> USE labstocks_db;
mysql> CREATE TABLE IF NOT EXISTS users_bckp SELECT * FROM users;
mysql> CREATE TABLE IF NOT EXISTS roles_old SELECT * FROM roles;
mysql> DELETE FROM users WHERE id > 0;
mysql> exit ;
```
Now update the functionality tables by running this command which will call the corresponding seeders:

```sh
./restorefunctionalitytables.sh
```

Restore the users table from backup, without their roles if the roleid changed during the update of the roles table. For this, reconnect into MySQL as above and once there, type:

```sql
mysql> USE labstocks_db;
mysql> UPDATE users_bckp SET role_id=NULL WHERE role_id NOT IN ( SELECT id FROM roles WHERE id IN (SELECT id from roles_old) ) ;
mysql> INSERT users SELECT * FROM users_bckp ;
mysql> DROP TABLE roles_old , users_bckp ;
mysql> exit ;
```

**IMPORTANT:** Some users may now have no role. You will need to attribute them a role manually from the admin panel (see Step 9 below) so that they can log in. Otherwise, login will be refused and Voyager will redirect them to the / route.

***Step 7: Clear all caches***

First these three ones:

```sh
php artisan config:clear
php artisan route:cache
php artisan view:clear
```
Then clear the whole application cache:

```sh
php artisan optimize:clear
php artisan cache:clear
```
If you get an error `Failed to clear cache. Make sure you have the appropriate permissions` then make sure that directory `storage/framework/cache/data` exists. If it does not, create it. If it does, then delete everything in it and run `php artisan cache:clear` again.

***Step 8: Bring up the application to live mode***

```sh
php artisan up
```

***Step 9 (IMPORTANT): Update roles attribution to users who lost their default role***

As mentioned above, some users may now have no role because the role they had is no longer valid after the upgrade. From the admin panel, browse the users table and look for NULL entries in the role attribute. Edit these users to assign them a role of your choice.

***Additional step if you are upgrading from v2.0.1***:

If you are upgrading from v2.0.1, you need to install additional prerequisites for plasmid map drawing (based on cirdna):

```sh
sudo apt install emboss pdf2svg
```

You also need now to re-initialize the BLAST database of `dna_features`, so that it is in the correct format for plasmid maps drawing. To do this, simply visit the BLAST query form of your instance.

This step is only needed once. However, all your past entries of dnafeatures will appear black on your maps. To customize the colors you want, simply edit the `color` field of each dnafeature and then click on the button 'Update Plasmid Maps with these Features'.

*[Back to top](#Top)*

## From v1.0.0 to v2.0.1 <a name="Fromv100tov201"></a>
 

Version v2 is **not** an upgrade of version v1 but was fully rewritten. Upgrading a v1 instance is therefore not straightfoward. However, it is totally do-able and we provide tools to help you achieve this process.

The first thing to do is to install a fresh instance of MyLabStocks that runs v2.0.1. Please see the steps above for the general guidelines. What is critical here in order to have v2.0.1 installed is to change:
```sh
git clone https://gitbio.ens-lyon.fr/LBMC/gylab/mylabstocks.git
git checkout latest-release
```
by:
```sh
git clone https://gitbio.ens-lyon.fr/LBMC/gylab/mylabstocks.git
git checkout v2.0.1
```


You will then need to perform three consecutive steps (in this order), please read the specific documentation for each one them:
- [transfer the database](v1tov2/v1tov2_transfer_database.md)
- [transfer media files](v1tov2/v1tov2_transfer_media.md)
- [transfer plasmid sequence files](v1tov2/v1tov2_transfer_plasmidseqfiles.md)


*[Back to top](#Top)*


