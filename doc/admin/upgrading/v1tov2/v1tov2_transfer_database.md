# How to import a database from a previous (outdated) version of MyLabStocks


If you previoulsy used MyLabStocks in the past, as we did in GYlab, you may have your data in a database that has a rather different format than the one used by the current version of MyLabStocks.

Here are some information on how we did to migrate our data into the updated version of the application. 

We applied this procedure to upgrade **from version 1.0.0 to version 2.0.1**. 

Note that this is not a fully-operational tool, but rather guidelines to help you do this if you need to. You will possibly have to adapt some of the tasks according to the specificities of your database.

Laravel can deal with two databases if necessary. A useful discussion about this is [here](https://stackoverflow.com/questions/31847054/how-to-use-multiple-databases-in-laravel)

Let's say that you are in the following situation:
- you have a database called `labstocks_db` that is up to date in terms of format with MyLabStocks but does not contain the data of your lab.
- you have another database, called `olddatabase`, in MySQL format, containing your lab data. This database contains tables corresponding to an old format of MyLabStocks (v1.0.0).
- `olddatabase` is housed on a server that we call here OLDSERVER
- `labstocks_db` is housed on a server that we call here PRODSERVER

## Step 1: load OLDDATABASE into MySQL of the PRODSERVER

You first need to dump `olddatabase`. We assume that you have a MySQL user called `root` that has all priviledges on `olddatabase`. To create the dump:
- Make sure nobody is active on `olddatabase`.
- Then connect to the OLDSERVER and type the following line (enter the password of the root user when prompted):
```sh
mysqldump --host=localhost --user=root -p --single-transaction olddatabase > myOldDatabaseDump.sql
```

Then copy the file `myOldDatabaseDump.sql` in the `/tmp/` directory of your PRODSERVER.
We assume that, on the PRODSERVER, you have a MySQL user called 'admin'. To create the `olddatabase` on the PRODSERVER, connect to it and type:
```sh
sudo mysql
CREATE DATABASE olddatabase CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL PRIVILEGES ON olddatabase.* TO 'admin'@'localhost' WITH GRANT OPTION;
exit
```

You can now load the data. Still connected to the PRODSERVER, type the following command and enter the password of 'admin' when prompted to. Note that this step can take some time depending on the size of the olddatabase.
```sh
mysql --host=localhost --user=admin -p --database=olddatabase < /tmp/myOldDatabaseDump.sql
```

All following steps below will be done on the PRODSERVER.

## Step 2: Configure database connections

You now need to tell MyLabStocks (well, Laravel...), to connect to two databases instead of only one.

Add the following lines in the `.env` file of mylabstocks (replacing 'admin' and 'password' by the appropriate credentials):
```
DB_CONNECTION_SECOND=mysql
DB_HOST_SECOND=127.0.0.1
DB_PORT_SECOND=3306
DB_DATABASE_SECOND=olddatabase
DB_USERNAME_SECOND=admin
DB_PASSWORD_SECOND=password
```

Edit the `config/database.php` file so that it contains:
```
'mysql' => [
    'driver'    => env('DB_CONNECTION'),
    'host'      => env('DB_HOST'),
    'port'      => env('DB_PORT'),
    'database'  => env('DB_DATABASE'),
    'username'  => env('DB_USERNAME'),
    'password'  => env('DB_PASSWORD'),
],

'mysql2' => [
    'driver'    => env('DB_CONNECTION_SECOND'),
    'host'      => env('DB_HOST_SECOND'),
    'port'      => env('DB_PORT_SECOND'),
    'database'  => env('DB_DATABASE_SECOND'),
    'username'  => env('DB_USERNAME_SECOND'),
    'password'  => env('DB_PASSWORD_SECOND'),
],
```

## Step 3: Reformat old tables using appropriate migrations

You can now modify olddatabase using migrations. We strongly recommend to test these migrations one by one, to avoid modifying your `labstocks_db` database.

A useful safeguard is to put these atypical migrations in a separate directory. For example:
```sh
mkdir database/migrations/v1_to_v2 # not needed if already exists
php artisan make:migration rename_columns_of_plasmids_table_of_db2 --table=plasmids --path=database/migrations/v1_to_v2/
```

Then edit the migration by setting the appropriate connection. **Make sure you do so in both up() and down() functions**.

We wrote several migrations that we needed in G.Yvert's lab; feel free to use them but be careful: they may not be adapted to your needs. You'll find them in directory
```
database/migrations/v1_to_v2/
```

To run the migrations, type:
```sh
php artisan migrate --path=database/migrations/v1_to_v2/
```
Then verify from within phpMyAdmin that the tables of olddatabase are changed as you want them to.

Verify that the migrations can be safely reversed, by typing:
```sh
php artisan migrate:rollback --path=database/migrations/v1_to_v2/
```
And take a look at the tables of olddatabase: they should be back to what they were.

Finally, run again the migrations forward so that the olddatabase is ready for the next steps. Type:
```sh
php artisan migrate --path=database/migrations/v1_to_v2/
```

## Step 4: Generate table seeders for the transfer of data

After you have reformatted the tables of `olddatabase` appropriately, especially regarding the number of columns to keep and their names, you can run [iseed](https://github.com/orangehill/iseed) to generate the corresponding seeders. Once the seeders are written, they can be used to populate the database `labstocks_db`.

Note that iseed writes in `database/seeders/DatabaseSeeder.php` the seeder class it produces. You probably don't want to run the seeder everytime, so we recommend that you leave your main DatabaseSeeder class unchanged.

You can do this by running the following:

```sh
# save a copy the original main seeder
cp database/seeders/DatabaseSeeder.php database/seeders/DatabaseSeeder_original.php

# run iseed on the desired tables
php artisan iseed antibodies,plasmids --database=mysql2 --classnameprefix=BigFromPreviousDatabase

# restore the main seeder
mv database/seeders/DatabaseSeeder_original.php database/seeders/DatabaseSeeder.php
```
This example will create two seeder files:
- `database/seeders/BigFromPreviousDatabasePlasmidsTableSeeder.php` containing the data of table `plasmids` of the `olddatabase`.
- `database/seeders/BigFromPreviousDatabaseAntibodiesTableSeeder.php` containing the data of table `antibodies` of the `olddatabase`.

**Important note**: Seeder files can be large. Make sure you set appropriate instructions in .gitignore to avoid loading them in your application repository.

## Step 5: Load data into `labstocks_db`

Once the seeder file is produced, simply run it. For example:
```sh
php artisan db:seed --class=BigFromPreviousDatabasePlasmidsTableSeeder
```

Now take a look with phpMyAdmin at the populated table, you should see the new content.

## Step 6: Import media files from the OLDSERVER

You probably had media files on the OLDSERVER that you want to transfer to the PRODSERVER.

To transfer these media, please refer to the dedicated [doc](v1tov2_transfer_media.md)

For example a raw file containing .ab1 Sanger sequencing of your plasmid id=13 was accessible from the page of this plasmid and you want to also have access to this file from the plasmid page of your new instance of MyLabStocks.

## Step 7: Import plasmid sequence files

To transfer plasmid sequence files from the OLDSERVER to the PRODSERVER, please refer to the dedicated [doc](v1tov2_transfer_plasmidseqfiles.md).

## Step 8: Cleanup by rolling back dedicated migrations

The specific migrations you used are in folder `database/migrations/v1_to_v2/`. You can (and should) now run them in reverse (which is called rolling back):
```sh
php artisan migrate:rollback --path=database/migrations/v1_to_v2
```
In particular, this will remove the column `seqfilenamev1` of the `plasmids` table.
