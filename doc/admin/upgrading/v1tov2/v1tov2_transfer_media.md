# How to transfer your media files from a v1.0.0 instance to a v2.x instance

If you used a previous version of MyLabStocks, you probably had media files that you want to transfer to the current installation.

For example a raw file containing .ab1 Sanger sequencing of a plasmid was accessible from the page of this plasmid and you want to also have access to this file from the plasmid page of your new instance of MyLabStocks.

This is an issue if you want to transfer from version v1.0.0 to a v2.x, because v2 is not an upgrade but a complete recoding.

We provide a tool that will help you do this transfer.

We applied this procedure to upgrade **from version 1.0.0 to version 2.0.1**.
 
This tool does not work for the oligos table, because in v1.0.0 this table does not have a numbered id column. This documentation describes the procedure on how to use it for tables antibodies, plasmids and strains.

We will assume that you are in the following situation:
- Your previous instance of MyLabStocks (v1.0.0) is on a server called OLDSERVER
- Your current version of MyLabStocks (v2.x) is on a server called NEWSERVER
- The id of your items (eg plasmids) are the same in the database of the OLDSERVER as in the database of the NEWSERVER.

If you did not import the database records from the OLDSERVER to the NEWSERVER, you must do so before transfering the media. Please follow the related [documentation](v1tov2_transfer_database.md).

On the OLDSERVER, your media files are in directories:
- `raw_dirs/plasmids/id/`
- `raw_dirs/antibodies/id/`
- `raw_dirs/strains/id/`

etc.
Where id is the id of the item related to the media files.

On the NEWSERVER, MyLabStocks stores media in directories:
- `storage/app/public/plasmids/`
- `storage/app/public/antibodies/`
- `storage/app/public/strains/`

etc.
The specific files related to a specific item's id is not in the path but is stored in the database, in the field `files` of the item's record.

# Step 1: Copy media files to your server

You first need to move already existing media of your recent MyLabStocks instance into a safe place that will be used temporarily during the transfer procedure.

On the NEWSERVER, from the root of your mylabstocks installation, type:

```sh
mkdir storage/app/tmpDuringTransfer
sudo chown -R yourlogin:www-data storage/app/public/*
mv storage/app/public/* storage/app/tmpDuringTransfer/
```

Then, copy the media from the OLDSERVER.
```sh
scp -r login@OLDSERVER:/path-to-mylabstocks/raw_dirs/plasmids storage/app/public/
scp -r login@OLDSERVER:/path-to-mylabstocks/raw_dirs/antibodies storage/app/public/
scp -r login@OLDSERVER:/path-to-mylabstocks/raw_dirs/strains storage/app/public/
scp -r login@OLDSERVER:/path-to-mylabstocks/raw_dirs/oligos storage/app/public/
```

# Step 2: Create the class to transfer the files paths

Now create a file containing the list of media that need to be referenced in the database, and run our tool on this file:
```sh
cd storage/app/public/
ls -l */* > /tmp/medialist.txt
cd ../../..
awk -f tools/v1tov2_mediapaths.awk -v outclass=PathsOfTransferedMediaBigSeeder /tmp/medialist.txt
```
This will create the following new file, which may be of big size if you are transferring a large number of media.
>database/seeders/PathsOfTransferedMediaBigSeeder.php

Note that including 'Big' in the file name tells git to ignore it, as instructed in the .gitignore of directory database/seeds/.

# Step 3: Load the media paths into the database

To load the media paths into the database, run:
```sh
php artisan db:seed --class=PathsOfTransferedMediaBigSeeder
```
This should have done it. Visit a few items to see.

# step 4: Cleanup (important)

Don't forget to restore the media files that you safely moved away before the procedure, and remove temporary files:
```
cd  storage/app/public
mv ../tmpDuringTransfer/antibodies/* antibodies/
rm -r ../tmpDuringTransfer/antibodies
mv ../tmpDuringTransfer/plasmids/* plasmids/
rm -r ../tmpDuringTransfer/plasmids
mv ../tmpDuringTransfer/oligos/* oligos/
rm -r ../tmpDuringTransfer/oligos
mv ../tmpDuringTransfer/strains/* strains/
rm -r ../tmpDuringTransfer/strains
mv ../tmpDuringTransfer/* .
rm -r ../tmpDuringTransfer
rm /tmp/medialist.txt

sudo chown -R www-data:www-data antibodies
sudo chown -R www-data:www-data plasmids
sudo chown -R www-data:www-data oligos
sudo chown -R www-data:www-data strains
```

