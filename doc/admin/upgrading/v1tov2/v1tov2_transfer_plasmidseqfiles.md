# How to transfer the sequence files of your plasmids from a v1.0.0 instance to a v2.x instance

If you used a previous version of MyLabStocks, you probably had .gb files of your plasmids that you want to transfer to the current installation.

This is not straightforward because v2 is not an upgrade of v1 but a complete recoding of MyLabStocks.

This documentation describes the procedure on how to do this. We applied this procedure to upgrade **from version 1.0.0 to version 2.0.1**.

If you did not import the database records from the OLDSERVER to the NEWSERVER, you must do so before transfering the media. Please follow the related [documentation](v1tov2_transfer_database.md).

We assume here that you are in the following situation:

- On the OLDSERVER, all your plasmid sequence files are in directory:
 `/var/www/labstocks/plasmid_files/`
- These sequence files were zipped with gzip and their filename extension is `.gz`.
- On the NEWSERVER, your MyLabStocks database is stored in MySQL under the name `labstocks_db`.


## Step 1: Copy sequence files to the NEWSERVER

On the NEWSERVER, MyLabStocks stores media in directory `storage/app/public/`. You must therefore copy the files there.

From the root of your mylabstocks installation, type the following commands (change `yourlogin` by what it is):
```sh
sudo mkdir storage/app/public/plasmids/seqfilesv1
sudo chown -R yourlogin:www-data storage/app/public/plasmids/seqfilesv1
```

Then, copy the sequence files from the OLDSERVER, unzip them and set appropriate permissions:

```sh
scp -r login@OLDSERVER:/var/www/labstocks/plasmid_files/*.gz storage/app/public/plasmids/seqfilesv1/
gunzip storage/app/public/plasmids/seqfilesv1/*
sudo chown -R www-data:www-data storage/app/public/plasmids/seqfilesv1
```

## Step 2: Update the seqfile field of the plasmids table in the database

On the NEWSERVER, type the following (change `admin` by the login you set when installing the database, see the `CREATE USER` instruction in the [main installation documentation](../README.md) ):
```sh
mysql -u admin -p
```
Enter the password when prompted. This will bring you in a SQL shell.
Once there type:
```sql
mysql> USE labstocks_db;
mysql>  UPDATE plasmids SET seqfile = CONCAT('[{"download_link":"plasmids/seqfilesv1/',LEFT(seqfilenamev1, LENGTH(seqfilenamev1)-3), '","original_name":"', LEFT(seqfilenamev1, LENGTH(seqfilenamev1)-3), '"}]');
```

Using phpMyAdmin, examine the content of the seqfile column of table plasmids. You should now see valid paths to the .gb files.

## Step 3: You're done. Don't do anything else.

In particular, do **NOT** delete the column seqfilenamev1 directly from phpMyAdmin or from with MySQL. This must be done by rolling back a Laravel migration.

