# Guidelines for Developers of MyLabStocks
 
Thank you for contributing to MyLabStocks! Please see below some useful information and guidelines.

## Table of Contents <a name="Contents"></a>

1. [Where are Classes ?](#whereclass)
2. [Database](#Database)
3. [Tracking functionality tables](#Trackingfunctionalitytables)
4. [Releasing](#release)
5. [External documentation](#externaldoc)



## Where are essential Classes ? <a name="whereclass"></a>

- *Routes* are in 'routes/web.php'
- *Models* for the tables are in 'app/'
- *Views* are in 'resources/views/'. Views that overriding the views of Voyager are in folder 'resources/views/vendor/voyager/'
- *Controllers* are in 'app/Http/Controllers/'

*[Back to Contents](#Contents)*

## Database Migrations and Seeding <a name="Database"></a>

It is important to build the database using migrations, to ensure version tracking and safe evolution of the application in the future.

Here are some memo of commands.

*[Back to Contents](#Contents)*

#### To create a table called `suppliers`:
```sh
php artisan make:migration create_suppliers_table --create=suppliers
```
Then edit the migration file that has been created in `database/migrations/`.
Then run the migration:
```sh
php artisan migrate
```
Or roll back to the latest migration done before
```sh
php artisan migrate:rollback
```

#### Don't forget to create the model

It is common to receive an error in voyager because the model is absent from the app/ directory. To create a model for table suppliers, type:
```sh
php artisan make:model Supplier
```

#### To seed this table with some initial data

Create the seeder class
```sh
php artisan make:seeder SuppliersTableSeeder
```
Then edit the seeder file that has been created in `database/seeders/`.
Then run it (the --class option tells laravel to be specific on this one only)
```sh
composer dump-autoload
php artisan db:seed --class=SuppliersTableSeeder
```

#### Relationships

We build relationships by editing the models and adding optional details in voyager's BREAD editor.

*[Back to Contents](#Contents)*
 
## IMPORTANT considerations regarding VERSIONING and Voyager

Voyager offers the possibility to create, modify and delete tables of the database. However, when doing so, there is no versioning/history records of these modifications. Therefore, **DO NOT MODIFY TABLES FROM WITHIN VOYAGER**. Please work exclusively with migrations as described above when creating/modifying tables.

*[Back to Contents](#Contents)*

## Git Functionalities Tables <a name="Trackingfunctionalitytables"></a>


When using voyager to develop MyLabStocks, a number of application functionalities are recorded in tables of the database. For example, BREADs are stored in `data_types` and `data_rows`.

An important consequence of this is that these functionalities are not tracked by git, because we git the code but we will never git the database of course. So, how are we going to keep track of the functionalities stored in the database tables?? This issue is discussed by voyager's developers themselves [here](https://github.com/the-control-group/voyager/issues/1030), where they say that this issue is solved in Voyager version 2. 

Before upgrading to Voyager v2, we propose a temporary solution to this, which is based on [iseed](https://github.com/orangehill/iseed). The idea is simple: we create seeders of the functionality tables, and we git these seeders.

We therefore ask you to follow the procedure described in this example:

- let's assume you are currently in branch master
- After you have edited some functionality in Voyager, for example BREADs, and you are happy with these edits, run
```sh 
git add *
git commit -m "I changed this and that in the code"
./gitfunctionalitytables.sh
git commit -m " I changed this and that in BREADS and MENUs..."
```
- Let's assume now that you want to start doing some tasks in a new branch called dev:
```sh
git chekout -b dev
do some work on files and/or BREADs and/or MENUs ...
git add *
./gitfunctionalitytables.sh
git commit -m "I did some dev tasks"
```
- And now you want to go back to branch master and retrieve not only the files but also the BREADs and MENUs
```sh
git checkout master
composer dumpautoload
./restorefunctionalitytables.sh
```
So, what happened?
- Your BREADs should be functional as they were prior to your edits on the dev branch.
- Your MENU tables are back to what they were, **but you don't see the changes from the Voyager Menu Builder**. To actualize your pages, click on a menu item that you know is conserved (or create a new menu item if you're not sure) and on the dialog box of this item click on **update**. This will tell Voyager to read again the menus and menuitems tables and your page should now display your actualized menus.

*[Back to Contents](#Contents)*

## Releasing <a name="release"></a>

For simplicity and clarity, the latest-release branch connects commits that are all on the master branch.

We assume here that you have a new version of MyLabStocks on a dev server and that you are happy with it and want to make a release.

Steps when making a release:
- Update seeders of functionality tables (for details and why, see above)
```sh
./gitfunctionalitytables.sh
```
- commit your changes
- merge to the master branch.
- Make a tag with a new version in format `vX.X.X`
```sh
git tag -a vX.X.X -m "version X.X.X" # change with tag name of the new version
```
- Produce the release on Gitlab server. Indicate in **comments the major changes, the issues (with their ID) that were addressed**, and link it to milestones and any relevant information.
- Update branch latest-release
```sh
git checkout latest-release
git merge vX.X.X # change with tag name of the new version
```
- Upgrade a server (a VM machine) dedicated to qualification of new releases (follow the upgrading instructions of the doc for administrators). On this upgraded instance, perform a few tests to see if all is ok.

*[Back to Contents](#Contents)*

## External documentation <a name="externaldoc"></a>

MyLabStocks is entirely based on laravel and voyager. Most fo the questions you may have will find answers in these pages:
- Laravel [documentation](https://laravel.com/docs/)
- Voyager [documentation](https://voyager-docs.devdojo.com/), and [Video tutorials](https://voyager.devdojo.com/academy/)

For questions related to PHP:
- [PHP Manual](https://www.php.net/manual/en/)

For questions related to styles:
- [Bootstrap doc](https://getbootstrap.com/docs/4.0/getting-started/introduction/). Bootstrap can be recompiled using customized variables via this [online compiler](https://getbootstrap.com/docs/3.4/customize/) and then overridden by passing the resulting css and js files to voyager as explained [here](https://voyager-docs.devdojo.com/customization/additional-css-js).
- [CSS tutorial](https://www.bitdegree.org/learn/css-tutorial)
- [HTML tutorial](https://www.bitdegree.org/learn/html)
- [Javascript tutorial](https://www.bitdegree.org/learn/javascript)

*[Back to Contents](#Contents)*
