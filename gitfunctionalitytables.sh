#! /bin/bash

##########################################
#
# This script is for developers of MyLabStocks
#
# It creates seeder files corresponding
# to your current database tables defining the BREADs and MENUs
# and it then add these seeders in the git repository.
# This ensures version tracking of BREADs and MENUs
#
# To restore BREADs and MENUs from such seeders
# see script restorefunctionalitytables.sh
#
##########################################
#
# Make a copy of DatabaseSeeder before iseed writes in it.
#
# I prefer this to using git for downstream restore, because 
# we don't know if this file is staged when this script is run.
cp database/seeders/DatabaseSeeder.php /tmp/DatabaseSeeder.php

# Inverse-seed contents of functionality tables
php artisan iseed data_types,data_rows,menus,menu_items,permissions,roles,permission_role,settings --force --classnameprefix=FunctionalityTrackingByIseed

# Restore original DatabaseSeeder
mv /tmp/DatabaseSeeder.php database/seeders/DatabaseSeeder.php

# Stage the seeders in git repository
git add database/seeders/FunctionalityTrackingByIseedDataTypesTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedDataRowsTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedMenusTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedMenuItemsTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedPermissionsTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedPermissionRoleTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedRolesTableSeeder.php
git add database/seeders/FunctionalityTrackingByIseedSettingsTableSeeder.php

echo " ########## "
echo "  Changes were likely made to seeders of tables that are "
echo "  used for application functionalities."
echo "  Don't forget to commit these changes to the git repository."
echo "  For details, please type: git status."
echo " ########## "

## Restoring instructions:
#  run:
#  ./restorefunctionalitytables.sh 



