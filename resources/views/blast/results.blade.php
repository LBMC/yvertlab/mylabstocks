@extends('voyager::master')

@section('page_title', setting('site.title') . ' BLAST')

@section('page_header')
    <h1 class="page-title">
        Results of your BLAST search using: <br>
        <?php
           echo $prog. " -task ". $task. " -db ". $blastdb;
        ?>
        <br>
        &nbsp;
    </h1>
@stop


@section('content')

   <?php
      include($outfile);
   ?>

@stop
