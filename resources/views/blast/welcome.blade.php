@extends('voyager::master')

@section('page_title', setting('site.title') . ' BLAST')

@section('page_header')
    <h1 class="page-title">
        BLAST your stocks 
         &nbsp;
    </h1>
@stop


@section('content')


{{ html()->form('POST')->route('blast.search')->open() }}
    <div class="form-group">
        {{ html()->label('Select the stock you want to search') }}
        {{ html()->select('blastdb', ['oligos' => 'oligos', 'plasmids' => 'plasmids', 'dnafeatures' => 'DNA features'], 'oligos') }}
        <br> <br>
        {{ html()->label('Select the blast program') }}
        {{ html()->select('blastprog', ['blastn' => 'blastn', 'tblastn' => 'tblastn'],  'blastn') }}
        <br> <br>
        {{ html()->label('Select the blast task') }}
        {{ html()->select('blasttask', ['blastn-short' => 'blastn-short', 'blastn' => 'blastn', 'tblastn' => 'tblastn'],  'blastn-short') }}
        <br> <br>
        {{ html()->label('Paste below your query sequence (must be at least 7bp-long)') }}
        <br> <br>
        {{ html()->textarea('queryseq', null, ['class' => 'form-control'])->rows(20)->cols(100) }}
    </div>

    {{ html()->submit('BLAST Search', array('class' => 'btn btn-primary center-block')) }}

{{ html()->form()->close() }}


@stop
