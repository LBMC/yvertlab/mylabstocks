<div class="card shadow-sm">
    @if($image)
        <img src="{{ $image }}" alt="">
    @endif
    <div class="card-body">
        @if($title)
            <h5 class="card-title">
                {{ $title }}
            </h5>
        @endif

        {{ $slot }}

        @if($link)
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{ $link }}" >
                       <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                    </a>
                </div>
            </div>
        @endif
    </div>
</div>
