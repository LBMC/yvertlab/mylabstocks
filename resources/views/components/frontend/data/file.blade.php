<ul class="list-group list-group-flush">
    @forelse($files as $file)
        @if(isset($file['download_link']) && isset($file['original_name']))
            <li class="list-group-item">
                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file['download_link']) }}"
                   target="_blank" download="{{ $file['original_name'] }}">{{ $file['original_name'] }}</a>
            </li>
        @endif
    @empty
        <li class="list-group-item">No file found.</li>
    @endforelse
</ul>
