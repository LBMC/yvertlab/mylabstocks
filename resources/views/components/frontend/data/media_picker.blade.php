<ul class="list-group list-group-flush">
    @forelse($files as $file)
        <li class="list-group-item">
            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file) }}" target="_blank">{{ $file }}</a>
        </li>
    @empty
        <li class="list-group-item">No file found.</li>
    @endforelse
</ul>
