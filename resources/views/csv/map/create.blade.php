@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-upload"></i>
        <p>CSV: map</p>
    </h1>
@stop

@section('content')
    <div class="page-content browse container-fluid">
	<h4 class="alert-warning">
		Dates in your csv file must follow the format:
		YYYY-mm-dd H:m:s
	</h4>
    </div>
    <br>

    <div id="gradient_bg"></div>

    <div class="page-content browse container-fluid">

        <form method="get" role="form" class="form-edit-add" action="{{ route('csv.map.create', ['filename' => $filename,  'dataType' => $dataType]) }}">

            <legend>Source and destination</legend>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-6 col-md-6">
                    <label for="model" class="control-label">Destination model</label>
                    <input type="text" class="form-control" id="dataType" name="dataType" value="{{ $dataType->slug }}"
                           readonly>
                </div>
                <div class="col-6 col-md-6">
                    <div class="form-group">
                        <label for="filename" class="control-label">Source filename</label>
                        <input type="text" class="form-control" id="filename" name="filename" value="{{ $filename }}"
                               readonly>
                    </div>
                </div>
            </div>

        </form>

        @if($dataType && $dataType->dataRows->count() != count($csvHeaders))
            <div class="alert alert-danger" role="alert">
                <strong>Warning</strong><br>
                {{ sprintf('Column count mismatch (csv: %d, model: %d)', count($csvHeaders), $dataType->dataRows->count()) }}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger" role="alert">
                <strong>Error</strong><br>
                {{ session('error') }}
            </div>
        @endif

        @if($dataType)
            <form method="post" role="form" class="form-edit-add" action="{{ route('csv.map.store', ['dataType' => $dataType]) }}">
                @method('POST')
                @csrf

                <legend>Map</legend>

                <input type="hidden" name="filename" value="{{ $filename }}">

                @foreach($dataType->dataRows as $dataRow)
                    <div class="row">
                        <div class="col-6 col-md-3">
                            <label for="{{ $dataRow->field }}" style="padding-top: 0.6rem;">
                                {{ $dataRow->display_name }} @if($dataRow->required) <span class="label label-danger">Required</span> @endif
                            </label>
                        </div>
                        <div class="col-6 col-md-9">
                            <select class="form-control" name="{{ $dataRow->field }}" id="{{ $dataRow->field }}">
                                <option value="">-- select csv column for {{ $dataRow->field }} --</option>
                                @foreach($csvHeaders as $csvHeader)
                                    <option value="{{ $csvHeader }}"
                                            @if(old($dataRow->field, in_array($dataRow->field, $csvHeaders) ? $dataRow->field : null) == $csvHeader) selected @endif>{{ $csvHeader }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endforeach

                <button type="submit" class="btn btn-primary">Start import</button>

                <a href="{{ route('csv.upload.create', ['dataType' => $dataType]) }}">Cancel and import another file</a>

            </form>
        @endif
    </div>
@endsection
