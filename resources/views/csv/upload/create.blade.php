@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-upload"></i>
        <p>Import from CSV to {{ $dataType->slug }}</p>
    </h1>
@stop

@section('content')
    <div class="page-content browse container-fluid">
	<h4 class="alert-warning">
		Dates in your csv file must follow the format:
		YYYY-mm-dd H:m:s
	</h4>
    </div>
    <br>
    <div class="page-content browse container-fluid">
        <form method="post" role="form" class="form-edit-add" enctype="multipart/form-data" action="{{ route('csv.upload.store', ['dataType' => $dataType]) }}">
            @csrf
            @method('post')

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group">
                <label for="source">Source file</label>
                <input name="source" type="file" id="source">
                <p class="help-block">Only .csv or .txt, maximum file size: {{ $uploadMaxFilesize }}</p>
            </div>

            <button type="submit" class="btn btn-primary">Upload</button>

        </form>
    </div>
@endsection
