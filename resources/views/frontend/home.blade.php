@extends('frontend.layout')

@section('content')

    @include('frontend.partials.header')

    <div class="album py-2 bg-light">
        <div class="container">
            <div class="row row-cols-1 row-cols-lg-2 g-2">

                <div class="col">
                    <x-frontend.card-with-image image="storage/frontend/img/shelf.png"
                                                title="Make sure you know what is where...">
                        <p class="card-text">
                           With MyLabStocks, retrieve and update information about your laboratory stocks.
                        </p>

                    </x-frontend.card-with-image>
                </div>

                <div class="col">
                    <x-frontend.card-with-image image="storage/settings/May2020/kk7wJ1UfaDU67RMVpqE9.jpg"
                                                link=""
                                                title="... even deep in your freezers!">
                        <p class="card-text">
                        </p>
                        <p class="card-text">
                        </p>
                    </x-frontend.card-with-image>
                </div>
            </div>
        </div>
    </div>
    <div class="album py-2 bg-light">
        <div class="container">
            <div class="row row-cols-1 row-cols-lg-1 g-2">

                <div class="col">
                    <x-frontend.card-with-image image=""
                                                link="https://gitbio.ens-lyon.fr/LBMC/yvertlab/mylabstocks/-/blob/master/README.md"
                                                title="Software documentation">
                        <p class="card-text">
                           Click below to access the software project and read the documentation describing its features and how to use it.
                        </p>

                    </x-frontend.card-with-image>
                </div>

                <div class="col">
                    <x-frontend.card-with-image image=""
                                                title="Please cite us and spread the word!">
                        <p class="card-text">
                            The software was proudly, happily and freely made available by <a href="http://www.ens-lyon.fr/LBMC/gisv/index.php/en/">the lab of Gael Yvert</a> at CNRS and ENS de Lyon, France. If you find it useful, please consider citing our work. You can add in the methods section of your papers a sentence like:
                        </p>
                        <p class="card-text">
                               <i>"Laboratory materials were traced using MyLabStocks [1]."<br>
                              [1] Chuffart F. and Yvert G. MyLabStocks: a web-application to manage molecular biology materials. Yeast. 2014 31:179-185.</i>
                        </p>
                        <p class="card-text">
                            Another way to help is to simply spread the word: tell your colleagues about MyLabStocks! Thank you.
                        </p>

                    </x-frontend.card-with-image>
                </div>
            </div>
        </div>
    </div>


@endsection
