@extends('frontend.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <div class="d-flex justify-content-between mb-2">
            <h2>{{ $dataType->display_name_plural }}</h2>
            <div>
                <a href="{{ route(sprintf('voyager.%s.create', $dataType->slug)) }}" target="_blank" class="btn btn-secondary">{{ __('Add') }}</a>
            </div>
        </div>

        {!! $builder->table(['class' => 'table table-striped table-bordered'], true) !!}

    </div>
@endsection

@push('scripts')
    {!! $builder->scripts() !!}
@endpush