<div class="card mt-3">
    <ul class="list-group list-group-flush overflow-auto max-33vh">
        @foreach($dataType->dataRows as $dataRow)
            @if($dataRow->type != 'relationship')
            <li class="list-group-item">
                <div class="row">
                    <div class="col-3">{{ $dataRow->display_name }}</div>
                    <div class="col-9 @if($dataRow->field == 'sequence') overflow-auto max-height-150 @endif">
                        <x-frontend.data :type="$dataRow->type" :value="$item->{$dataRow->field}"/>
                    </div>
                </div>
            </li>
            @endif
        @endforeach
    </ul>
    <div class="card-body">
        <a href="{{ route(sprintf('voyager.%s.show', $dataType->slug), ['id' => $item->id]) }}"
           target="_blank" class="card-link">View in Back-End</a>
    </div>
</div>
