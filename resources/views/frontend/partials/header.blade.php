<section class="py-5 text-center container">
    <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
            <h1 class="fw-light">Welcome to {{ env('APP_NAME') }} !</h1>
            <p class="lead text-muted"> This is the Front-End interface of the application, where you can search and browse your main stocks. More complete functionalities are offered from the Back-End interface.</p>
            <p>
                
            @guest
                <a href="{{ route('voyager.login') }}" target="_self"
                   class="btn btn-primary my-2">{{ __('Please login') }}</a>
            @else
                <a href="{{ route('frontend.plasmids.index') }}" class="btn btn-primary my-2"><span   class="icon icon-plasmid"></span>  Plasmids</a>
                <a href="{{ route('frontend.oligos.index') }}"   class="btn btn-secondary my-2"><span class="icon icon-primer"></span> Oligos</a>
                <a href="{{ route('frontend.strains.index') }}"  class="btn btn-warning my-2"><span   class="icon icon-budyeast"></span> Strains</a>
            @endguest

            </p>
        </div>
    </div>
</section>
