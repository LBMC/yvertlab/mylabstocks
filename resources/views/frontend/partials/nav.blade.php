<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand text-uppercase" href="{{ config('app.url') }}">{{ config('app.name') }}</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            @auth
                {{ menu('frontend', 'frontend.voyager.bootstrap') }}
            @endauth

            @guest
                <a href="{{ route('voyager.login') }}" target="_self"
                   class="nav-link text-secondary">{{ __('Login') }}</a>
            @else
                <a href="{{ route('voyager.dashboard') }}" target="_blank"
                   class="nav-link text-secondary">{{ __('Back-End') }}</a>
            @endguest
        </div>
    </div>
</nav>
