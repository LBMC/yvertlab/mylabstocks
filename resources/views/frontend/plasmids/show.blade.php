@extends('frontend.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <div class="d-flex justify-content-between mb-2">
            <h2>{{ $dataType->display_name_singular }} {{ $item->name }}</h2>
            <div>
                <a href="{{ route(sprintf('voyager.%s.edit', $dataType->slug), ['id' => $item->id]) }}" target="_blank"
                   class="btn btn-secondary">{{ __('Edit') }}</a>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Plasmid
                                </div>
                                <div class="col">
                                    {{ $item->id }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Name
                                </div>
                                <div class="col">
                                    {{ $item->name }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Type
                                </div>
                                <div class="col">
                                    {{ $item->type }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Selection
                                </div>
                                <div class="col">
                                    {{ $item->bacterial_selection }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Species
                                </div>
                                <div class="col">
                                    {{ $item->species }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Markers
                                </div>
                                <div class="col">
                                    {{ $item->markers->implode(', ') }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Seqfile
                                </div>
                                <div class="col">
                                    <x-frontend.data type="file" :value="$item->seqfile"/>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Author
                                </div>
                                <div class="col">
                                    {{ $item->author }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Projects
                                </div>
                                <div class="col">
                                    @if($item->projects)
                                        <ul class="list-group list-group-flush">
                                            @foreach($item->projects as $project)
                                                <li class="list-group-item">
                                                    <a href="{{ route('voyager.projects.show', ['id' => $project->id]) }}"
                                                       target="_blank">{{ $project->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="card-body">
                        <a href="{{ route(sprintf('voyager.%s.show', $dataType->slug), ['id' => $item->id]) }}"
                           target="_blank" class="card-link">View in Back-End</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-9">
                <div class="card">
                    <img src="{{ $map }}" class="card-img-top" alt="">
                </div>
            </div>
        </div>

        @if($item->description)
            <div class="mt-3">
                <div class="card">
                    <div class="card-body">
                        <legend>Description</legend>
				<x-frontend.data :type="$dataType->dataRows->where('field', 'description')->first()->type" :value="$item->description"/>
                    </div>
                </div>
            </div>
        @endif


        @if($item->files)
            <div class="mt-3">
                <div class="card">
                    <div class="card-body">
                        <legend>Files</legend>
                        <x-frontend.data type="media_picker" :value="$item->files"/>
                    </div>
                </div>
            </div>
        @endif

        @include('frontend.partials.dataType', ['dataType' => $dataType])

    </div>
@endsection
