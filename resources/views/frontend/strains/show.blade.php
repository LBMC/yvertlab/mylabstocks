@extends('frontend.layout')

@section('content')
    <div class="container mt-4 mb-4">
        <div class="d-flex justify-content-between mb-2">
            <h2>{{ $dataType->display_name_singular }} {{ $item->name }}</h2>
            <div>
                <a href="{{ route(sprintf('voyager.%s.edit', $dataType->slug), ['id' => $item->id]) }}" target="_blank" class="btn btn-secondary">{{ __('Edit') }}</a>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-2 g-3">
            <div class="col-12 col-sm-6 col-md-3">
                @if($item->warning)
                    <div class="alert alert-danger" role="alert">
                        Issue! See comments
                    </div>
                @else
                    <div class="alert alert-info" role="alert">
                        No issue detected.
                    </div>
                @endif
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Strain
                                </div>
                                <div class="col">
                                    {{ $item->id }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Name
                                </div>
                                <div class="col">
                                    {{ $item->name }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Species
                                </div>
                                <div class="col">
                                    {{ $item->species }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Background
                                </div>
                                <div class="col">
                                    {{ $item->general_background }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Author
                                </div>
                                <div class="col">
                                    {{ $item->author }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col">
                                    Projects
                                </div>
                                <div class="col">
                                    @if($item->projects)
                                        <ul class="list-group list-group-flush">
                                            @foreach($item->projects as $project)
                                                <li class="list-group-item">
                                                    <a href="{{ route('voyager.projects.show', ['id' => $project->id]) }}"
                                                       target="_blank">{{ $project->name }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="card-body">
                        <a href="{{ route(sprintf('voyager.%s.show', $dataType->slug), ['id' => $item->id]) }}"
                           target="_blank" class="card-link">View in Back-End</a>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-md-9">
                <div class="card">
                    <div class="card-body">
                        <legend>Genotype</legend>
                        {!! $item->genotype !!}
                    </div>
                </div>
            </div>
        </div>

        @if($item->files)
            <div class="mt-3">
                <div class="card">
                    <div class="card-body">
                        <legend>Files</legend>
                        <x-frontend.data type="media_picker" :value="$item->files"/>
                    </div>
                </div>
            </div>
        @endif

        @if($item->comments)
            <div class="mt-3">
                <div class="card">
                    <div class="card-body">
                        <legend>Comments</legend>
                        {!! $item->comments !!}
                    </div>
                </div>
            </div>
        @endif

        @include('frontend.partials.dataType', ['dataType' => $dataType])

    </div>
@endsection
