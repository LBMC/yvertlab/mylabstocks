<ul class="navbar-nav me-auto mb-2 mb-lg-0">
    @foreach($items as $menu_item)
        <li class="nav-item">
                <a href="{{ $menu_item->link() }}" class="nav-link">{{ $menu_item->title }}</a>
        </li>
    @endforeach
</ul>
