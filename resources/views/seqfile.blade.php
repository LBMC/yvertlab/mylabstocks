<!-- BROWSE VIEW -->
@if ($view == "browse")
	@if (isset($content))
	        @foreach(json_decode($content) as $file)
          		<div data-field-name="{{ $row->field }}">
          		  <a class="fileType" target="_blank"
           		   href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
            		  data-file-name="{{ $file->original_name }}" download="{{ $file->original_name }}" >
           		   {{ $file->original_name ?: '' }}
           		 </a>
          		</div>
                @endforeach
	@endif

<!-- EDIT VIEW -->
@elseif ($view == "edit")
	@if (isset($content) && $content !== "[]")
		@if(isset($dataTypeContent->{$row->field}))
		    @if(json_decode($dataTypeContent->{$row->field}) !== null)
	        	@foreach(json_decode($dataTypeContent->{$row->field}) as $file)
		          <div data-field-name="{{ $row->field }}">
		            <a class="fileType" target="_blank"
		              href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
		              data-file-name="{{ $file->original_name }}" data-id="{{ $dataTypeContent->getKey() }}" download="{{ $file->original_name }}">
		              {{ $file->original_name ?: '' }}
		            </a>
	        	    <a href="#" class="voyager-x remove-multi-file"></a>
		          </div>
		        @endforeach
		    @else
		      <div data-field-name="{{ $row->field }}">
		        <a class="fileType" target="_blank"
		          href="{{ Storage::disk(config('voyager.storage.disk'))->url($dataTypeContent->{$row->field}) }}"
		          data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}">>
	          Download
		        </a>
		        <a href="#" class="voyager-x remove-single-file"></a>
		      </div>
		    @endif
		@endif
	@else
    	    <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]">
	@endif

<!-- ADD VIEW -->
@elseif ($view == "add")
    	    <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]">

<!-- READ VIEW -->
@elseif ($view == "read")
	@if(isset($dataTypeContent->{$row->field}))
	    @if(json_decode($dataTypeContent->{$row->field}) !== null)
	        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
	          <div data-field-name="{{ $row->field }}">
	            <a class="fileType" target="_blank"
	              href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
	              data-file-name="{{ $file->original_name }}" data-id="{{ $dataTypeContent->getKey() }}" download="{{ $file->original_name }}">
	              {{ $file->original_name ?: '' }}
	            </a>
	          </div>
	        @endforeach
	    @else
	      <div data-field-name="{{ $row->field }}">
	        <a class="fileType" target="_blank"
	          href="{{ Storage::disk(config('voyager.storage.disk'))->url($dataTypeContent->{$row->field}) }}"
	          data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}">>
	          Download
	        </a>
	        <a href="#" class="voyager-x remove-single-file"></a>
	      </div>
	    @endif
	@endif

<!-- OTHER VIEWS -->
@else
	@if(isset($dataTypeContent->{$row->field}))
	    @if(json_decode($dataTypeContent->{$row->field}) !== null)
	        @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
	          <div data-field-name="{{ $row->field }}">
	            <a class="fileType" target="_blank"
	              href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}"
	              data-file-name="{{ $file->original_name }}" data-id="{{ $dataTypeContent->getKey() }}" download="{{ $file->original_name }}">
	              {{ $file->original_name ?: '' }}
	            </a>
        	    <a href="#" class="voyager-x remove-multi-file"></a>
	          </div>
	        @endforeach
	    @else
	      <div data-field-name="{{ $row->field }}">
	        <a class="fileType" target="_blank"
	          href="{{ Storage::disk(config('voyager.storage.disk'))->url($dataTypeContent->{$row->field}) }}"
	          data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}">>
	          Download
	        </a>
	        <a href="#" class="voyager-x remove-single-file"></a>
	      </div>
	    @endif
	@endif
	<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file" name="{{ $row->field }}[]" multiple="multiple">

@endif
