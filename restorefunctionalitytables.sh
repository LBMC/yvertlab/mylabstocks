#! /bin/bash
##########################################
#
# This script is for developers of MyLabStocks
#
# It uses seeder files produced by gitbreadmenus.sh
# to restore BREADs, MENUs, ROLEs and PERMISSIONs
#
# Note that you may have conflicts between your current USERS or USER_ROLES tables
# and the ROLES seeder, for example if the USERS table contains a role_id entry that is no longer present in the seeder. If this happens, edit your users and user_roles tables and run the seeders again.
#
##########################################

php artisan db:seed --class=FunctionalityTrackingByIseedDataTypesTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedDataRowsTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedMenusTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedMenuItemsTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedPermissionsTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedRolesTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedPermissionRoleTableSeeder
php artisan db:seed --class=FunctionalityTrackingByIseedSettingsTableSeeder
