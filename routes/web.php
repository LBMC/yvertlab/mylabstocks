<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', config('app.url') . '/home');
Route::view('/home', '/frontend/home');

/* Test Route (for dev purposes) */
//Route::get('/tests', [ 'uses' =>'PlasmidBaseController@index', 'as' => 'plasmid.map' ]);

Route::group([], __DIR__.'/web/frontend.php');

/* Voyager routes */
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::group([], __DIR__.'/web/csv.php');

    // MyLabStocks overriding routes here
    $namespacePrefix = '\\'.config('voyager.controllers.namespace').'\\';
    try {
            foreach (Voyager::model('DataType')::all() as $dataType) {
                $breadController = $dataType->controller
                                 ? Str::start($dataType->controller, '\\')
                                 : $namespacePrefix.'VoyagerBaseController';

                Route::get($dataType->slug.'/{id}/duplicate', ['uses' => $breadController.'@duplicate', 'as' => 'voyager.'.$dataType->slug.'.duplicate']);
                //Route::get('search/'.$dataType->slug   , ['uses' => $breadController.'@search', 'as' => 'voyager.search.'.$dataType->slug]);
            }
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException("Custom routes hasn't been configured because: ".$e->getMessage(), 1);
        } catch (\Exception $e) {
            // do nothing, might just be because table not yet migrated.
        }
});

/* BLAST routes */
Route::get('/blast/',  [ 'uses' => 'BlastController@welcome', 'as' => 'blast.welcome' ]);
Route::post('/blast/', [ 'uses' => 'BlastController@search' , 'as' => 'blast.search'  ]);
Route::get('/updateBlastDnaFeatures', [ 'uses' =>'BlastController@update_dna_features', 'as' => 'update_blast_dna_features' ]);

/* Route to update plasmid sequence from its .gb file */
Route::get('/sequence/updateplasmid/{id}', [ 'uses' =>'PlasmidSequenceController@update_plasmid_seq_from_file', 'as' => 'sequence.update_plasmid_seq_from_file' ]);


