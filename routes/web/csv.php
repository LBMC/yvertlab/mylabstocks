<?php

// Csv download
Route::get('csv/download/{dataType:slug}', [\App\Http\Controllers\Csv\ExportController::class, 'download'])->name('csv.download');

// Csv upload
Route::get('csv/upload/{dataType:slug}', [\App\Http\Controllers\Csv\UploadController::class, 'create'])->name('csv.upload.create');
Route::post('csv/upload/{dataType:slug}', [\App\Http\Controllers\Csv\UploadController::class, 'store'])->name('csv.upload.store');

// Csv column mapping
Route::get('csv/map/{dataType:slug}', [\App\Http\Controllers\Csv\MapController::class, 'create'])->name('csv.map.create');
Route::post('csv/map/{dataType:slug}', [\App\Http\Controllers\Csv\MapController::class, 'store'])->name('csv.map.store');
