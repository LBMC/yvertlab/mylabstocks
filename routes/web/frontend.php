<?php

/*
 * Either run:
 * php artisan db:seed --class=FrontendMenuSeeder
 * ...or create manually a new menu in Voyager UI: Tools > Menu builder > add new
 * and call it "frontend"
 */
Route::get('/plasmids/searchBuilder', [\App\Http\Controllers\Frontend\PlasmidController::class, 'searchBuilder'])->name('frontend.plasmids.searchBuilder');

Route::get('/antibodies', [\App\Http\Controllers\Frontend\AntibodyController::class, 'index'])->name('frontend.antibodies.index');
Route::get('/oligos', [\App\Http\Controllers\Frontend\OligoController::class, 'index'])->name('frontend.oligos.index');
Route::get('/oligos/{oligo}', [\App\Http\Controllers\Frontend\OligoController::class, 'show'])->name('frontend.oligos.show');
Route::get('/plasmids', [\App\Http\Controllers\Frontend\PlasmidController::class, 'index'])->name('frontend.plasmids.index');
Route::get('/plasmids/{plasmid}', [\App\Http\Controllers\Frontend\PlasmidController::class, 'show'])->name('frontend.plasmids.show');
Route::get('/strains', [\App\Http\Controllers\Frontend\StrainController::class, 'index'])->name('frontend.strains.index');
Route::get('/strains/{strain}', [\App\Http\Controllers\Frontend\StrainController::class, 'show'])->name('frontend.strains.show');

