############################################################
#
# usage:
#
# see doc/v1tov2_transfer_media.md 
#
# awk -f thisscript -v outclass=outClassName infile
#
# where:
#
#    infile: a text file produced by shell command of the type
#            ls -l storage/app/public/
#            (see documentation)
#
#    outClassName: the php Class name you want to obtain
#                  for example
#                  AddFilesPathsFromV1Seeder
#
#############################################################


BEGIN{
  outdir = "database/seeders/";
  FS = "\n";
  RS = "";
  OFS = "";
  outfile = sprintf("%s%s.php", outdir, outclass)
  print "<?php" > outfile
  print "/** "  > outfile
  print " *"    > outfile
  print " * This file was generated automatically " > outfile
  print " * by running tools/v1tov2_mediapaths.awk on file: " > outfile
  print " * ", ARGV[1] > outfile
  print " * "          > outfile
  print " */"          > outfile

  print ""   > outfile
  print "namespace Database\\Seeders;"   > outfile
  print ""   > outfile
  print "use Illuminate\\Database\\Seeder;" > outfile
  print "use DB;" > outfile
  print "" > outfile
  print "class ", outclass, " extends Seeder" > outfile
  print "{"  > outfile
  print ""   > outfile
  print "    public function run()"  > outfile
  print "    {" > outfile
  print ""   > outfile
}


{
  # get the path
  path=substr($1, 1, length($1) - 1)
  n = split(path, a, "/")
  # get the table name and record id from the path
  {
     table = a[1]
     id = a[2]
  }
  # loops on files
  f = 1
  for (i=3; i<=NF; i++){
     # get the filename
     m = split($i, b, " ");
     files[f] = b[9]
     f++;
  }
  # build the database entry 
  # in format ["path/file1", "path/file2"]
  dbentry = ""
  for (f in files){
     if (length(dbentry) == 0) 
         dbentry = sprintf("[\"%s/%s\"", path, files[f])
     else 
        dbentry = sprintf("%s,\"%s/%s\"", dbentry, path, files[f])
  }
  dbentry = sprintf("%s]", dbentry)
  ## print the seed code ##
  # special case of oligos where id is a string and corresponds to the name filed
  if (table == "oligos"){
    if (length(files) > 0)
    {
        print "      DB::table('", table, "')"                    > outfile
        print "          ->where('name', \"", id, "\")"           > outfile
        print "          ->update(['files' => '", dbentry, "']);" > outfile
        print "" > outfile
    }
  }
  else{ # for other tables, id is an integer and corresponds to the id field
    if (length(files) > 0)
    {
        print "      DB::table('", table, "')"                    > outfile
        print "          ->where('id', ", id, ")"                 > outfile
        print "          ->update(['files' => '", dbentry, "']);" > outfile
        print "" > outfile
    }
  }
  delete files;

}

END{
  print "    }"> outfile
  print "}"    > outfile
}
